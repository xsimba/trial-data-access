# Trial Data Access

This application transforms device data into archive format.

## Installation

Install npm and then:

``` npm install ```

To use AWS locally please have your credentials stored locally:

``` ~/.aws/credentials ```

Run DynamoDB locally for development: http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Tools.DynamoDBLocal.html

Run elasticsearch locally: https://www.elastic.co/guide/en/elasticsearch/guide/current/_installing_elasticsearch.html

### AWS

We use the AWS account setup with the email address Simband.ssic@gmail.com.

Currently the services we use are

* S3 bucket named 'trial-archive'
* DynamoDB - tables are setup automatically by our models (https://github.com/automategreen/dynamoose)
* Elasticsearch - search-trial-archive-azj2gsyyrvxizzea7y2y5hjtwu.us-west-2.es.amazonaws.com

All the AWS services must be accessed with the credentials of the IAM user 'trial-access' - http://docs.aws.amazon.com/IAM/latest/UserGuide/introduction.html

## Save an archive from a session

To create an archive from SAMI data or a trial session post the SAMIsession JSON found in the clipboard in the data visualization app and the trial admin app:

``` http://localhost:3000/import ```

You will need to present a valid SAMI authorization token in the header, for example:

``` Authorization: Bearer d4261fb6d52f40b784e993d036542051 ```

Example JSON from the data vis tool might be:

```
{
 "startDate": 1440941185000,
 "endDate": 1441114012428,
 "sdids": "2e71bd49506a411aa70fec0efb00b17f",
 "uids": "b379322f43e641bd9ad6ee7787043fad",
 "trialId": "b936c867117a4ae9a40bfb482682c1a8"
}
```

Example JSON from trials admin might be:

```
{
 "sessionId": "041550a47c66460f99e9f226b0b538fc",
 "exportId": "8bda3ef4cc0643918cabd4d2434e8921",
 "startDate": 1440941185000,
 "endDate": 1441114012428,
 "sdids": "2e71bd49506a411aa70fec0efb00b17f",
 "uids": "b379322f43e641bd9ad6ee7787043fad",
 "trialId": "b936c867117a4ae9a40bfb482682c1a8"
}
```

By default the archive created will be in JSON format but you can request CSV like this:

``` http://localhost:3000/import?archive=csv ```

The archive will be uploaded to S3 and you will receive a response with a link to the archive. This archive will expire
after the number of days defined in the config.

## Transform service

The transform service (lib/services/transform.js) takes SAMI data and transforms it into the archive format on a per device basis.

Currently the only supported device is Simband. All other devices fallback to a default setting.

## Workers
Cron jobs (aka workers) are found in lib/workers. They run regularly based on the config. Currently there are two jobs:

* Upload: upload archive files to S3.
* Remove: delete expired archive files from S3.

## Tests

Run tests like so:

``` npm test ```

Note that all tests are written using mocha. The React tests use mocha based on this: https://github.com/danvk/mocha-react

## DynamoDB Local

To run Dynamo local (instead of against the live database) please follow these instructions:

http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Tools.DynamoDBLocal.html

Then cd into the dir containing the jar and run:

``` java -Djava.library.path=./DynamoDBLocal_lib -jar DynamoDBLocal.jar -sharedDb ```

Local data can be viewed here:

``` http://localhost:8000/shell/ ```


## Auth

https://github.com/jaredhanson/passport-oauth2
and bearer: https://github.com/jaredhanson/passport-http-bearer

logout
https://accounts.samsungsami.io/logout?redirect_uri=https%3A%2F%2Ftrials.simband.io%2Fauthorize


## Scripts

The import_sami.py script can be used to import SAMI data based on either the JSON from the session clipboard
or the SAMI data visualization tool clipboard:

### usage examples

Import from session JSON:

```./import_sami.py -s '{"sessionId":"041550a47c66460f99e9f226b0b538fc","exportId":"91a68c4d429048d88a679ee39653408e","startDate":1440941185000,"endDate":1441114012428,"sdids":"2e71bd49506a411aa70fec0efb00b17f","uids":"b379322f43e641bd9ad6ee7787043fad","trialId":"b936c867117a4ae9a40bfb482682c1a8"}'```

Import from SAMI JSON:

```./import_sami.py -s '{"startDate":1440941185000,"endDate":1441114012428,"sdids":"2e71bd49506a411aa70fec0efb00b17f","uids":"b379322f43e641bd9ad6ee7787043fad","trialId":"b936c867117a4ae9a40bfb482682c1a8"}'```

Import JSON (session or SAMI) from a file:

```./import_sami.py -i import-json.txt```

Import from a session ID:

```./import_sami.py -i 5c5501c9119d4536ba4d9924f57401f6```

You can store your username and password locally (at your own risk!) in auth.ini - this will stop the need to manually add your details when prompted by the script.### egg

Search with JSON filters.

Search by filtering metadata key and value:

```./search.py -f '{"must":{"archive.metadata.key":"general/archivedBy/email","archive.metadata.value":"johndbarnard@gmail.com"},"must_not":{}}'```

or (defining number of results and page number of results):

```./search.py -f '{"must":{"archive.metadata.key":"general/archivedBy/email","archive.metadata.value":"johndbarnard@gmail.com"},"must_not":{}}' -pp 5 -p 1```

for example -pp 1 (--per_page 1) tells the script to return 1 item in the results and -p 2 (--page 2) tells the script to return the second page of results.

Search by filtering metadata key and value with a number range:

```./search.py -f '{"must":{"archive.metadata.key":"stats/ppg7/sampleCount","archive.metadata.value":{"from":"700","to":"800"}},"must_not":{}}'```

Search with text query:

```./search.py -q 'johndbarnard*'```

### egg

To create an egg run:

```python setup.py bdist_egg```

### tests

From scripts/trial_data_access/resources run:

```python -m unittest test_resources```
or
```python -m unittest discover . "test_*.py"```

## Search

We use [ElasticSearch](https://www.elastic.co/).

A sample archive search:

```http://localhost:3000/search?search_query=sami```

You can perform more advanced searches, for example:

```http://localhost:3000/search?field=archive.metadata.value&search_query=johndbarnard%40gmail.com```

The above defines which field to look in (in this case a metadata value) and the query string. The query could
use the keywords AND OR, for example:

```http://localhost:3000/search?field=archive.metadata.value&search_query=johndbarnard%40gmail.com OR johndbarnard%40samsung.com````

Or a wildcard:

```http://localhost:3000/search?field=archive.metadata.value&search_query=johndbarnard*````

```http://localhost:3000/search?q=%7B%22must%22%3A%7B%22archive.metadata.key%22%3A%22test%22%2C%22archive.metadata.value%22%3A%22test%22%7D%2C%22must%22%3A%7B%22archive.status%22%3A%22Removed%22%7D%7D```

The above is a URL encoded JSON string, which actually looks like this:

```{"must":{"archive.metadata.key":"test","archive.metadata.value":"test"},"must":{"archive.status":"Removed"}}```

Users can also search ahead for metadata keys, to help with selecting which key to search for:

```http://localhost:3000/search/metadata/key?q=general```

Finally there is a wildcard search, which is must looser and allows full text search:

```http://localhost:3000/search/find/q=john*```
