var objectAssign = require('object-assign');
var React = require('react');

var StubRouterContext = function(Component, props, stubs) {
  return React.createClass({
    childContextTypes: {
      makePath: function() {},
      makeHref: function() {},
      transitionTo: function() {},
      replaceWith: function() {},
      goBack: function() {},
      getCurrentPath: function() {},
      getCurrentRoutes: function() {},
      getCurrentPathname: function() {},
      getCurrentParams: function() {},
      getCurrentQuery: function() {},
      isActive: function() {}
    },

    getChildContext: function getChildContext() {
      return objectAssign({
        makePath: function() {},
        makeHref: function() {},
        transitionTo: function() {},
        replaceWith: function() {},
        goBack: function() {},
        getCurrentPath: function() {},
        getCurrentRoutes: function() {},
        getCurrentPathname: function() {},
        getCurrentParams: function() {},
        getCurrentQuery: function() {},
        isActive: function() {}
      }, stubs);
    },

    render: function render() {
      return React.createElement(Component, props);
    }
  });
};

module.exports = StubRouterContext;
