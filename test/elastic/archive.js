var assert = require("assert")
  , app = require('../../app')
  , archive = require('../../lib/elastic/archive');

describe('Elasticsearch Archive', function() {
  describe('Archive', function() {
    this.timeout(30000);

    it('should build an archive index.', function(done){
      archive.build(function(err, result){
        assert.equal(null, err);
        assert.equal('Archive indexing',result);
        done();
      });
    })
  })
});


// TODO search tests!
