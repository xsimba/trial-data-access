var assert = require("assert")
  , app = require('../../app')
  , dynamoose = require('dynamoose')
  , SessionModel = dynamoose.model('SessionData')
  , uuid = require('uuid');

describe('Session', function() {
  describe('SessionModel', function() {
    this.timeout(12000);
    var now = new Date().getTime();
    var archive = uuid.v4() + '.json';

    it('should create a session.', function(done){
      dynamoose.local();

      var sessionModel = new SessionModel();

      sessionModel.archive = archive;
      sessionModel.sessionId = '123';
      sessionModel.participantId = '456';
      sessionModel.devices = JSON.stringify(['123']);
      sessionModel.exports = JSON.stringify([]);
      sessionModel.metadata = JSON.stringify([{key:'k1',value:'v1'}]);
      sessionModel.start = now;
      sessionModel.end = now;
      sessionModel.trialId = '789';
      sessionModel.status = 'test';

      sessionModel.save(function(err){
        assert.equal(err, null);
        done();
      });
    });

    it('should find session.', function(done){
      dynamoose.local();

      SessionModel.scan('archive').eq(archive)
        .exec(function(err, sessions) {
          assert.equal(err, null);
          assert.equal(sessions.length,1);
          assert.equal(sessions[0].sessionId,'123');
          done();
        });
    });

    it('should delete session.', function(done){
      dynamoose.local();

      SessionModel.scan('archive').eq(archive)
        .exec(function(err, sessions) {
          assert.equal(err, null);
          assert.equal(sessions.length,1);
          var session = sessions[0];
          assert.equal(session.sessionId,'123');
          session.delete(function(err){
            assert.equal(err, null);
            done();
          });
        });
    });

    it('should delete all test sessions.', function(done){
      dynamoose.local();

      SessionModel.scan('status').eq('test')
        .exec(function(err, sessions) {
          assert.equal(err, null);
          sessions.forEach(function(session){
            session.delete(function(err){
              assert.equal(err, null);
            });
          });
          done();
        });
    });
  })
});
