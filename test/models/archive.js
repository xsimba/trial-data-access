var assert = require("assert")
  , app = require('../../app')
  , dynamoose = require('dynamoose')
  , ArchiveModel = dynamoose.model('ArchiveData')
  , uuid = require('uuid');

describe('Archive', function() {
  describe('ArchiveModel', function() {
    this.timeout(12000);
    var now = new Date().getTime();
    var archive = uuid.v4()+'.+now';

    it('should create an archive.', function(done){
      dynamoose.local();

      var archiveModel = new ArchiveModel();

      archiveModel.filename = archive;
      archiveModel.created = now;
      archiveModel.expires = now;
      archiveModel.user = '1';
      archiveModel.status = 'test';

      archiveModel.save(function(err){
        assert.equal(err, null);
        done();
      });
    });

    it('should find archive.', function(done){
      dynamoose.local();

      ArchiveModel.scan('filename').eq(archive)
        .exec(function(err, archives) {
          assert.equal(err, null);
          assert.equal(archives.length,1);
          assert.equal(archives[0].user,'1');
          done();
        });
    });

    it('should delete archive.', function(done){
      dynamoose.local();

      ArchiveModel.scan('filename').eq(archive)
        .exec(function(err, archives) {
          assert.equal(err, null);
          assert.equal(archives.length,1);
          var archive = archives[0];
          assert.equal(archive.user,'1');
          archive.delete(function(err){
            assert.equal(err, null);
            done();
          });
        });
    });

    it('should delete all test archives.', function(done){
      dynamoose.local();

      ArchiveModel.scan('status').eq('test')
        .exec(function(err, archives) {
          assert.equal(err, null);
          archives.forEach(function(archive){
            archive.delete(function(err){
              assert.equal(err, null);
            });
          });
          done();
        });
    });
  })
});
