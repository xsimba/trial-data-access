var assert = require("assert")
  , app = require('../../app')
  , dynamoose = require('dynamoose')
  , MetadataModel = dynamoose.model('MetadataData')
  , uuid = require('uuid');

describe('Metadata', function() {
  describe('MetadataModel', function() {
    this.timeout(12000);
    var metadata = uuid.v4();
    var archive = uuid.v4()+'.json';

    it('should create metadata.', function(done){
      dynamoose.local();

      var metadataModel = new MetadataModel();

      metadataModel.id = metadata;
      metadataModel.archive = archive;
      metadataModel.key = 'remove-test-key';
      metadataModel.value = '789';
      metadataModel.order = 0;

      metadataModel.save(function(err){
        assert.equal(err, null);
        done();
      });
    });

    it('should find metadata.', function(done){
      dynamoose.local();

      MetadataModel.scan('id').eq(metadata)
        .exec(function(err, metadatas) {
          assert.equal(err, null);
          assert.equal(metadatas.length,1);
          assert.equal(metadatas[0].archive, archive);
          done();
        });
    });

    it('should delete metadata.', function(done){
      dynamoose.local();

      MetadataModel.scan('id').eq(metadata)
        .exec(function(err, metadatas) {
          assert.equal(err, null);
          assert.equal(metadatas.length,1);
          var metadata = metadatas[0];
          assert.equal(metadata.archive, archive);
          metadata.delete(function(err){
            assert.equal(err, null);
            done();
          });
        });
    });

    it('should delete all test metadatas.', function(done){
      dynamoose.local();

      MetadataModel.scan('key').eq('remove-test-key')
        .exec(function(err, metadatas) {
          assert.equal(err, null);
          metadatas.forEach(function(metadata){
            metadata.delete(function(err){
              assert.equal(err, null);
            });
          });
          done();
        });
    });
  })
});
