var assert = require("assert")
  , app = require('../../app')
  , dynamoose = require('dynamoose')
  , SAMIModel = dynamoose.model('SAMIData')
  , uuid = require('uuid');

describe('SAMI', function() {
  describe('SAMIModel', function() {
    this.timeout(12000);
    var sami = uuid.v4();
    var archive = uuid.v4()+'.json';

    it('should create sami.', function(done){
      dynamoose.local();

      var samiModel = new SAMIModel();

      samiModel.archive = archive;
      samiModel.data = ['123','456'];

      samiModel.save(function(err){
        assert.equal(err, null);
        done();
      });
    });

    it('should find sami.', function(done){
      dynamoose.local();

      SAMIModel.query('archive').eq(archive)
        .exec(function(err, samis) {
          assert.equal(err, null);
          assert.equal(samis.length,1);
          assert.equal(JSON.stringify(samis[0].data),JSON.stringify(['123','456']));
          done();
        });
    });

    it('should delete sami.', function(done){
      dynamoose.local();

      SAMIModel.query('archive').eq(archive)
        .exec(function(err, samis) {
          assert.equal(err, null);
          assert.equal(samis.length,1);
          var sami = samis[0];
          assert.equal(JSON.stringify(samis[0].data),JSON.stringify(['123','456']));
          sami.delete(function(err){
            assert.equal(err, null);
            done();
          });
        });
    });
  })
});
