var assert = require("assert")
  , SessionValidator = require("../../lib/validators/session");

describe('Validator', function() {
  describe('Session', function() {
    it('should be valid JSON', function(done){

      var sessionJSON = {"sessionId":"041550a47c66460f99e9f226b0b538fc","exportId":"a2c94247ad9449518ebede499eaeda97","startDate":1440941185000,"endDate":1441114012428,"sdids":"2e71bd49506a411aa70fec0efb00b17f","uids":"b379322f43e641bd9ad6ee7787043fad","trialId":"b936c867117a4ae9a40bfb482682c1a8"};

      SessionValidator.Validate.JSON(sessionJSON, function(err, result){
        assert.equal(true, result);
        done();
      });
    });
    it('should be invalid JSON', function(done){

      var sessionJSON = {"sessionId":123,"startDate":1440941185000,"endDate":1441114012428,"sdids":"2e71bd49506a411aa70fec0efb00b17f","uids":"b379322f43e641bd9ad6ee7787043fad","trialId":"b936c867117a4ae9a40bfb482682c1a8"};

      SessionValidator.Validate.JSON(sessionJSON, function(err, result){
        assert.equal(2, err.length);
        assert.equal(false, result);
        done();
      });
    });
  });
});
