var assert = require("assert")
  , Metadata = require("../../lib/services/archive/metadata")
  , uuid = require('uuid')
  , path = require('path')
  , fs = require('fs')
  , config = require('../../config/config');

describe('Metadata', function() {
  describe('Convert', function() {
    it('should convert data', function(done){
      var sdids = "2e71bd49506a411aa70fec0efb00b17f";
      var data = {id : "707a1ecaa5934031bae289909fc0bc3d", email : "test@test.com"};
      var archiveDir = path.join(config.root,config.archive.dir);
      var archiveFileName = uuid.v4() + ".json";
      var archiveFilePath = path.join(archiveDir, archiveFileName);
      var expectedMetadata = {
        sami : {
          sdid : sdids
        },
        general : {
          archivedBy : {
            id : data.id,
            email: data.email
          },
          origin : 'sami'
        }
      };

      Metadata.Utility.Convert(sdids, data, archiveFileName, archiveFilePath, function(err, metadata){
        assert.equal(null, err);
        assert.equal(JSON.stringify(expectedMetadata), JSON.stringify(metadata));
        done();
      });
    });
  });
});
