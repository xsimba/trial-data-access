var assert = require("assert")
  , Session = require("../../lib/services/api/session")
  , uuid = require('uuid')
  , path = require('path')
  , fs = require('fs')
  , config = require('../../config/config');

describe('Session', function() {
  describe('Convert', function() {
    it('should convert data', function(done){
      var data = {"data":{"uuid":"041550a4-7c66-460f-99e9-f226b0b538fc","id":"041550a47c66460f99e9f226b0b538fc","trialId":"b936c867117a4ae9a40bfb482682c1a8","participantId":"b379322f43e641bd9ad6ee7787043fad","devices":[{"uuid":"8e42c19b-90a2-4bd3-91eb-0bdf8df6a442","id":"8e42c19b90a24bd391eb0bdf8df6a442","deviceId":"2e71bd49506a411aa70fec0efb00b17f"}],"start":1440941185000,"end":1441114012428,"metadatas":[{"uuid":"7c7b312e-2153-4c9e-96e5-91d8d6d5d43a","id":"7c7b312e21534c9e96e591d8d6d5d43a","key":"test","value":"simband"}],"exports":[{"uuid":"16e9b760-8958-4f66-8577-2aa86a5c97f4","id":"16e9b76089584f6685772aa86a5c97f4","exportId":"7cf9026da6e74d02b59c67a1b64abf5e","created":1441114047090},{"uuid":"cac9b291-0e08-4213-b6a0-a8097d4ed794","id":"cac9b2910e084213b6a0a8097d4ed794","exportId":"a2c94247ad9449518ebede499eaeda97","created":1441691096489}],"status":null}};

      var archiveDir = path.join(config.root,config.archive.dir);
      var archiveFileName = uuid.v4() + ".json";
      var archiveFilePath = path.join(archiveDir, archiveFileName);
      var expectedSession = JSON.stringify({"session" : {"data":{"uuid":"041550a4-7c66-460f-99e9-f226b0b538fc","id":"041550a47c66460f99e9f226b0b538fc","trialId":"b936c867117a4ae9a40bfb482682c1a8","participantId":"b379322f43e641bd9ad6ee7787043fad","devices":[{"uuid":"8e42c19b-90a2-4bd3-91eb-0bdf8df6a442","id":"8e42c19b90a24bd391eb0bdf8df6a442","deviceId":"2e71bd49506a411aa70fec0efb00b17f"}],"start":1440941185000,"end":1441114012428,"metadatas":[{"uuid":"7c7b312e-2153-4c9e-96e5-91d8d6d5d43a","id":"7c7b312e21534c9e96e591d8d6d5d43a","key":"test","value":"simband"}],"exports":[{"uuid":"16e9b760-8958-4f66-8577-2aa86a5c97f4","id":"16e9b76089584f6685772aa86a5c97f4","exportId":"7cf9026da6e74d02b59c67a1b64abf5e","created":1441114047090},{"uuid":"cac9b291-0e08-4213-b6a0-a8097d4ed794","id":"cac9b2910e084213b6a0a8097d4ed794","exportId":"a2c94247ad9449518ebede499eaeda97","created":1441691096489}],"status":null}}}).replace(/}$/, "") + ", ";

      Session.Sessions.Convert(JSON.stringify(data), archiveFileName, archiveFilePath, function(err, session){
        assert.equal(null, err);
        assert.equal(archiveFilePath, session);
        fs.readFile(session, 'utf8', function(err, result){
          assert.equal(null, err);
          assert.equal(expectedSession, result);
          fs.unlink(archiveFilePath);
          done();
        });
      });
    });
  });
});
