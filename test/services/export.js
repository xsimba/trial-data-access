var assert = require("assert")
  , app = require('../../app')
  , Export = require('../../lib/services/transform/export')
  , uuid = require('uuid')
  , path = require('path')
  , fs = require('fs')
  , config = require('../../config/config')
  , dynamoose = require('dynamoose');

describe('Export', function() {
  describe('ConvertExportFile', function() {
    this.timeout(12000);

    it('should extract export tgz and transform it into an archive document.', function(done){
      dynamoose.local();
      var archiveDir = path.join(config.root,config.archive.dir);
      var archiveFileName = uuid.v4() + ".json";
      var archiveFilePath = path.join(archiveDir, archiveFileName);
      var exportDir = path.join(config.root,config.api.sami.download.exports);

      var exportId = "a2c94247ad9449518ebede499eaeda97";

      var filename = exportId + ".tgz";
      var resourceFile = path.join(config.root,config.resources.dir,filename);
      var filePath = path.join(config.root,config.api.sami.download.exports,exportId + ".tgz");

      metadata = {
        sami : {
          sdid : "123"
        },
        general : {
          archivedBy : {
            id : "123",
            email: "123@123.com"
          },
          origin : 'test'
        }
      };

      copyFile(resourceFile, filePath, function(){
        Export.Utility.ConvertExportFile(
          exportId
          , exportDir
          , filePath
          , archiveFileName
          , archiveFilePath
          , metadata
          , function(err, exports){
            assert.equal(null, err);
            assert.equal(archiveFilePath, exports);
            fs.unlink(archiveFilePath);
            done();
          });
      });

    })
  })
});

function copyFile(source, target, cb) {
  var cbCalled = false;

  var rd = fs.createReadStream(source);
  rd.on("error", done);

  var wr = fs.createWriteStream(target);
  wr.on("error", done);
  wr.on("close", function(ex) {
    done();
  });
  rd.pipe(wr);

  function done(err) {
    if (!cbCalled) {
      cb(err);
      cbCalled = true;
    }
  }
}
