var assert = require("assert")
  , Transform = require("../../lib/services/transform/transform");

describe('Transform', function() {
  describe('Simband', function() {
    it('should transform exported data chunk into Simband archive format', function(done){
      var ts = 1441025538000;
      var data = {"payload":{"field":"ecgVisual","stream":"5b1b92bd","samples":"PANfOq0GWzzCDCI9jt4VPaDTmzvooo08mJB7u0pSD75aHOi9cA1jPUw0dr6cSi++f0tnQOIcYUFIyvJBngg9QmrfYkLN3ExClt3mQf/chMBKRhLCb1xqwiL1hsLxUonCfpmOwpjPosJ7f8PCKCjjwjYV9cJs0ffCxt71wqB//MLaPAjDzy0Vwy2xHsPPbSHDq/0fw7GKIMNsRSfD96Iyw9m+PMPdqkDDD+s+w9YMPcOzpkDDlDtKw0auVMO93FnD8UBYw/5wVMNZB1XDH5Jcw4QSZ8OseW3DzBlsw3crZsPmCGPDO3hnw/keccM4a3jD2GR3w2pEb8OAl2fD/XhnwwAA==","version":"0.12.0_2fbca1a"},"ecgVisual":[8.507257E-4,0.013368291,0.039562948,0.036589198,0.0047554523,0.017289594,-0.0038385745,-0.13996235,-0.11333533,0.055432737,-0.24043387,-0.17118305,3.613983,14.069551,30.34877,47.258415,56.718178,51.215626,28.858196,-4.151977,-36.56864,-58.590267,-67.478775,-68.661995,-71.29979,-81.40546,-97.748985,-113.57843,-122.54143,-123.90903,-122.935104,-126.24927,-136.2377,-149.17894,-158.6921,-161.42894,-159.99089,-160.54176,-167.27118,-178.63658,-188.7455,-192.66743,-190.9182,-189.05014,-192.65117,-202.23273,-212.68076,-217.86226,-216.25368,-212.44138,-213.0287,-220.57079,-231.07233,-237.47528,-236.10077,-230.16978,-227.03476,-231.46965,-241.12099,-248.41882,-247.39392,-239.26724,-231.5918,-231.47261],"sampleRate":128.0};

      var stats = { minTimestamp: 1441025538, maxTimestamp: 1441025544.609, totalSampleCount: 10476, versions: [], 'ecgVisual/sampleCount': 896, 'ecg/sampleCount': 768, 'accelerometerX/sampleCount': 768, 'ppg7/sampleCount': 768, 'ecgLead/sampleCount': 12, 'ppg2/sampleCount': 768, 'ppg4/sampleCount': 768, 'ppg0/sampleCount': 768, 'ppg6/sampleCount': 768, 'ppg1/sampleCount': 768, 'ppg5/sampleCount': 768, 'ppg3/sampleCount': 768, 'accelerometerY/sampleCount': 768, 'accelerometerZ/sampleCount': 768, 'gsrPhasic/sampleCount': 176, 'gsrTonic/sampleCount': 176 };
      var versions = { '0.12.0_2fbca1a': true };

      var expectedOutput = {"versions":{"0.12.0_2fbca1a":true},"stats":{"minTimestamp":1441025538,"maxTimestamp":1441025538,"totalSampleCount":10540,"versions":[],"ecgVisual/sampleCount":960,"ecg/sampleCount":768,"accelerometerX/sampleCount":768,"ppg7/sampleCount":768,"ecgLead/sampleCount":12,"ppg2/sampleCount":768,"ppg4/sampleCount":768,"ppg0/sampleCount":768,"ppg6/sampleCount":768,"ppg1/sampleCount":768,"ppg5/sampleCount":768,"ppg3/sampleCount":768,"accelerometerY/sampleCount":768,"accelerometerZ/sampleCount":768,"gsrPhasic/sampleCount":176,"gsrTonic/sampleCount":176},"data":{"ecgVisual":{"t":[1441025538,1441025538.0078125,1441025538.015625,1441025538.0234375,1441025538.03125,1441025538.0390625,1441025538.046875,1441025538.0546875,1441025538.0625,1441025538.0703125,1441025538.078125,1441025538.0859375,1441025538.09375,1441025538.1015625,1441025538.109375,1441025538.1171875,1441025538.125,1441025538.1328125,1441025538.140625,1441025538.1484375,1441025538.15625,1441025538.1640625,1441025538.171875,1441025538.1796875,1441025538.1875,1441025538.1953125,1441025538.203125,1441025538.2109375,1441025538.21875,1441025538.2265625,1441025538.234375,1441025538.2421875,1441025538.25,1441025538.2578125,1441025538.265625,1441025538.2734375,1441025538.28125,1441025538.2890625,1441025538.296875,1441025538.3046875,1441025538.3125,1441025538.3203125,1441025538.328125,1441025538.3359375,1441025538.34375,1441025538.3515625,1441025538.359375,1441025538.3671875,1441025538.375,1441025538.3828125,1441025538.390625,1441025538.3984375,1441025538.40625,1441025538.4140625,1441025538.421875,1441025538.4296875,1441025538.4375,1441025538.4453125,1441025538.453125,1441025538.4609375,1441025538.46875,1441025538.4765625,1441025538.484375,1441025538.4921875],"s":[0.0008507257,0.013368291,0.039562948,0.036589198,0.0047554523,0.017289594,-0.0038385745,-0.13996235,-0.11333533,0.055432737,-0.24043387,-0.17118305,3.613983,14.069551,30.34877,47.258415,56.718178,51.215626,28.858196,-4.151977,-36.56864,-58.590267,-67.478775,-68.661995,-71.29979,-81.40546,-97.748985,-113.57843,-122.54143,-123.90903,-122.935104,-126.24927,-136.2377,-149.17894,-158.6921,-161.42894,-159.99089,-160.54176,-167.27118,-178.63658,-188.7455,-192.66743,-190.9182,-189.05014,-192.65117,-202.23273,-212.68076,-217.86226,-216.25368,-212.44138,-213.0287,-220.57079,-231.07233,-237.47528,-236.10077,-230.16978,-227.03476,-231.46965,-241.12099,-248.41882,-247.39392,-239.26724,-231.5918,-231.47261]}}};
      var transformedData = Transform.SAMI.Simband(stats, versions, ts, data);

      assert.equal(JSON.stringify(expectedOutput), JSON.stringify(transformedData));
      done();
    });
  }),
  describe('Default', function() {
    it('should transform exported data chunk into Default archive format', function(done){
      var ts = 1433925467442;
      var data = {"description":"Run","heartRate":60,"activity":2,"stepCount":0};
      var stats = { minTimestamp: 1441025538, maxTimestamp: 1441025544.609, totalSampleCount: 10476, versions: [], 'ecgVisual/sampleCount': 896, 'ecg/sampleCount': 768, 'accelerometerX/sampleCount': 768, 'ppg7/sampleCount': 768, 'ecgLead/sampleCount': 12, 'ppg2/sampleCount': 768, 'ppg4/sampleCount': 768, 'ppg0/sampleCount': 768, 'ppg6/sampleCount': 768, 'ppg1/sampleCount': 768, 'ppg5/sampleCount': 768, 'ppg3/sampleCount': 768, 'accelerometerY/sampleCount': 768, 'accelerometerZ/sampleCount': 768, 'gsrPhasic/sampleCount': 176, 'gsrTonic/sampleCount': 176 };
      var versions = { '0.12.0_2fbca1a': true };
      var expectedOutput = { data: { ts: 1433925467442, data: { description: 'Run', heartRate: 60, activity: 2, stepCount: 0 } } };
      var transformedData = Transform.SAMI.Default(stats, versions, ts, data);

      assert.equal(JSON.stringify(expectedOutput), JSON.stringify(transformedData));
      done();
    });
  })
});
