{
	"title": "Trial Data Access",
	"welcome": "Welcome to",
	"error_occurred": "An error occurred: ",
	"error": "error",
	"archive_status": "Archive status: ",
	"New": "New",
	"Uploading": "Uploading",
	"Uploaded": "Uploaded",
	"Removing": "Removing",
	"Removed": "Removed",
	"Expired": "Expired",
	"Importing": "Importing"
}