var express = require('express'),
  config = require('./config/config'),
  glob = require('glob'),
  dynamoose = require('dynamoose'),
  socket_io = require('socket.io')
  AWS = require('aws-sdk');

dynamoose.AWS.config.update({
  accessKeyId: config.aws.key,
  secretAccessKey: config.aws.secret,
  region: config.aws.region
});

var db = null;

if(config.dynamodb && config.dynamodb === 'local') {
  console.log('Do not forget to run DynamoDB local or the app will crash at some point!');
  db = dynamoose.local();
} else {
  db = dynamoose.ddb();
  if(typeof db === "undefined") {
    throw new Error('Unable to connect to DynamoDB at ' + config.aws.key);
  }
}

var models = glob.sync(config.root + '/app/models/*.js');
models.forEach(function (model) {
  require(model);
});
var app = express();

var io = socket_io();
app.io = io;

require('./config/express')(app, config);

module.exports = app;

if(!module.parent) {
  var server = app.listen(process.env.PORT || config.port, function () {
    console.log('Express server listening on port ' + process.env.PORT || config.port);
    // Cron jobs
    S3Worker = require('./lib/workers/S3');
    //S3Worker.Tasks.RemoveAllDB();
    S3Worker.Tasks.CleanUpNew();
    S3Worker.Tasks.CleanUpUploading();
    S3Worker.Tasks.CleanUpRemoving();
    S3Worker.Jobs.Upload(app);
    S3Worker.Jobs.Remove(app);
    ElasticWorker = require('./lib/workers/Elastic');
    ElasticWorker.Jobs.Archive();
    // Build ElasticSearch indexes.
    ElasticWorker.Tasks.Startup();
  });
  var io = app.io;
  app.io.attach(server);
  require('./sockets/base')(app);
}
