var jsdom = require('mocha-jsdom');

var assert = require('assert');

describe('Error Message', function() {
  jsdom();

  it('display error message', function() {
    var React = require('react/addons');
    var ErrorMessage = require('../app/public/views/components/messages/error-message.jsx');
    var TestUtils = React.addons.TestUtils;

    var message = 'This is an error.';
    var errorMessage = TestUtils.renderIntoDocument(<ErrorMessage error={message} />);
    var p = TestUtils.findRenderedDOMComponentWithTag(errorMessage, 'p');

    assert.equal(p.getDOMNode().textContent, message);
  });
});
