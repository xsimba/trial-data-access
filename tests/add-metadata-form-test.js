var jsdom = require('mocha-jsdom');
var assert = require('assert');

describe('Add Metadata Form', function() {
  this.timeout(30000);
  jsdom();

  it('display add metadata form', function(done) {
    var React = require('react/addons');
    var AddMetadataForm = require('../app/public/views/components/metadata/add-metadata-form.jsx');
    var TestUtils = React.addons.TestUtils;

    var addMetadata = function() {};
    var addMetadataForm = TestUtils.renderIntoDocument(<AddMetadataForm addMetadata={addMetadata} />);

    var p = TestUtils.scryRenderedDOMComponentsWithTag(addMetadataForm, 'p');

    assert.equal(p.length, 2);
    var keys = p[0].getDOMNode().textContent.split(' ');
    var values = p[1].getDOMNode().textContent.split(' ');

    assert.equal(keys[0], 'Key:');
    assert.equal(values[0], 'Value:');
    done();
  });
});
