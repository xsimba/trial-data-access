var jsdom = require('mocha-jsdom');
var assert = require('assert');

describe('Import Archive Table', function() {
  jsdom();

  this.timeout(60000);

  it('display archive in table', function(done) {
    var React = require('react/addons');
    var ImportArchiveTable = require('../app/public/views/components/import/archive-table.jsx');
    var TestUtils = React.addons.TestUtils;

    var tableData = [['test',0.01,1441025538]];

    var importArchiveTable = TestUtils.renderIntoDocument(<ImportArchiveTable data={tableData} perPage={5} />);

    var tr = TestUtils.scryRenderedDOMComponentsWithTag(importArchiveTable, 'tr');
    var th = TestUtils.scryRenderedDOMComponentsWithTag(importArchiveTable, 'th');
    var td = TestUtils.scryRenderedDOMComponentsWithTag(importArchiveTable, 'td');

    setTimeout(function() {
      assert.equal(tr.length, 2);
      assert.equal(th.length, 5);
      assert.equal(td.length, 5);
      assert.equal(th[0].getDOMNode().textContent, 'Field');
      assert.equal(td[0].getDOMNode().textContent, 'test');
      done();
    }, 1000);

  });
});
