var jsdom = require('mocha-jsdom');
var assert = require('assert');

describe('Metadata Table', function() {
  jsdom();

  it('display metadata in table', function(done) {
    var React = require('react/addons');
    var MetadataTable = require('../app/public/views/components/metadata/metadata-table.jsx');
    var TestUtils = React.addons.TestUtils;

    var metadata = [
      {
        key: 'k1',
        value: 'v1'
      },
      {
        key: 'k2',
        value: 'v2'
      }
    ];

    var metadataTable = TestUtils.renderIntoDocument(<MetadataTable metadata={metadata} />);

    var div = TestUtils.scryRenderedDOMComponentsWithTag(metadataTable, 'div');

    assert.equal(div.length, 3);

    assert.equal(div[1].getDOMNode().textContent, 'k1 :' + ' v1');
    assert.equal(div[2].getDOMNode().textContent, 'k2 :' + ' v2');

    done();
  });
});
