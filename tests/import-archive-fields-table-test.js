var jsdom = require('mocha-jsdom');

var assert = require('assert');
var moment = require('moment');

describe('Import Archive Fields', function() {
  jsdom();
  this.timeout(60000);

  it('show archive fields', function(done) {
    var React = require('react/addons');
    var ArchiveFieldsTable = require('../app/public/views/components/import/archive-fields-table.jsx');
    var TestUtils = React.addons.TestUtils;

    var keys = ['test','hello','code'];
    var selectField = function() {};

    var archiveFieldsTable = TestUtils.renderIntoDocument(<ArchiveFieldsTable data={keys} selectField={selectField} />);
    var checkbox = TestUtils.scryRenderedDOMComponentsWithTag(archiveFieldsTable, 'input');

    setTimeout(function() {
      assert.equal(checkbox.length, 3);
      done();
    }, 1000);

  });
});
