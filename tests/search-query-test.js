var jsdom = require('mocha-jsdom');
var assert = require('assert');

describe('Search Query', function() {
  jsdom();

  it('display search query input', function(done) {
    var React = require('react/addons');
    var SearchQuery = require('../app/public/views/components/search/search-query.jsx');
    var TestUtils = React.addons.TestUtils;

    var submitted = false;

    var searchQuery = TestUtils.renderIntoDocument(<SearchQuery submitted = {submitted}/>);

    var input = TestUtils.scryRenderedDOMComponentsWithTag(searchQuery, 'input');

    assert.equal(input.length, 1);
    done();
  });

});
