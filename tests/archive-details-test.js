var jsdom = require('mocha-jsdom');

var assert = require('assert');
var moment = require('moment');

describe('Archive Details', function() {
  jsdom();
  this.timeout(60000);

  it('show archive details', function(done) {
    var React = require('react/addons');
    var ArchiveDetails = require('../app/public/views/components/archive/archive-details.jsx');
    var TestUtils = React.addons.TestUtils;
    var createdDate = Date.now();

    var archive = {
      canEdit: true,
      created: createdDate,
      expires: new Date('Thu Sep 17 2015 14:28:07 GMT+0100 (BST)'),
      filename: 'd427598e-55eb-4e83-8407-5a37105b7785.json',
      user: '707a1ecaa5934031bae289909fc0bc3d',
      status: 'Removed',
      url: 'http://test.com'
    };

    var archiveId = 'd427598e-55eb-4e83-8407-5a37105b7785';

    var createArchive = function() {};

    var createdDateString = moment(createdDate).format('MMMM Do YYYY, h:mm:ss a');

    var archiveDetails = TestUtils.renderIntoDocument(<ArchiveDetails archive={archive} createArchive={createArchive} archiveId={archiveId} />);
    var h2 = TestUtils.scryRenderedDOMComponentsWithTag(archiveDetails, 'h2');
    var p = TestUtils.scryRenderedDOMComponentsWithTag(archiveDetails, 'p');

    setTimeout(function() {
      assert.equal(h2.length, 1);
      assert.equal(h2[0].getDOMNode().textContent, 'Archive ' + archiveId);
      assert.equal(p.length, 1);
      assert.equal(p[0].getDOMNode().textContent, 'Created: ' + createdDateString + '.');
      done();
    }, 1000);

  });
});
