var jsdom = require('mocha-jsdom');
var assert = require('assert');

describe('Import Metadata Table', function() {
  jsdom();

  this.timeout(60000);

  it('display metadata in table', function(done) {
    var React = require('react/addons');
    var ImportMetadataTable = require('../app/public/views/components/import/metadata-table.jsx');
    var TestUtils = React.addons.TestUtils;

    var tableData = [['test',0.01,1441025538]];

    var selectMetadata = function() {};
    var metadata = [
      {
        key: 'k1',
        value: 'v1'
      },
      {
        key: 'k2',
        value: 'v2'
      }
    ];

    var importMetadataTable = TestUtils.renderIntoDocument(<ImportMetadataTable selectMetadata={selectMetadata} metadata={metadata} />);

    var tr = TestUtils.scryRenderedDOMComponentsWithTag(importMetadataTable, 'tr');
    var th = TestUtils.scryRenderedDOMComponentsWithTag(importMetadataTable, 'th');
    var td = TestUtils.scryRenderedDOMComponentsWithTag(importMetadataTable, 'td');

    setTimeout(function() {
      assert.equal(tr.length, 3);
      assert.equal(th.length, 3);
      assert.equal(td.length, 6);
      assert.equal(th[0].getDOMNode().textContent, 'Key');
      assert.equal(td[0].getDOMNode().textContent, 'k1');
      assert.equal(th[1].getDOMNode().textContent, 'Value');
      assert.equal(td[1].getDOMNode().textContent, 'v1');
      assert.equal(td[3].getDOMNode().textContent, 'k2');
      assert.equal(td[4].getDOMNode().textContent, 'v2');
      done();
    }, 1000);

  });
});
