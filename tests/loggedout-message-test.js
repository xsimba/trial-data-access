var jsdom = require('mocha-jsdom');

var assert = require('assert');

describe('Logged out message', function() {
  jsdom();

  it('display logged out message', function() {
    var React = require('react/addons');
    var LoggedoutMessage = require('../app/public/views/components/messages/loggedout-message.jsx');
    var TestUtils = React.addons.TestUtils;

    var loggedoutMessage = TestUtils.renderIntoDocument(<LoggedoutMessage />);
    var p = TestUtils.findRenderedDOMComponentWithTag(loggedoutMessage, 'p');

    assert.equal(p.getDOMNode().textContent, 'You have been logged out. Please login.');
  });
});
