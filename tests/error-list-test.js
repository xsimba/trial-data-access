var jsdom = require('mocha-jsdom');

var assert = require('assert');

describe('Error List', function() {
  jsdom();

  it('lists all the errors provided', function() {
    var React = require('react/addons');
    var ErrorList = require('../app/public/views/components/messages/error-list.jsx');
    var TestUtils = React.addons.TestUtils;

    var errors = ['error 1','error 2','error 3','error 4'];
    var errorList = TestUtils.renderIntoDocument(<ErrorList errors={errors} />);
    var li = TestUtils.scryRenderedDOMComponentsWithTag(errorList, 'li');

    assert.equal(li.length, 4);
  });
});
