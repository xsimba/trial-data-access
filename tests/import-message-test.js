var jsdom = require('mocha-jsdom');
var moment = require('moment');

var assert = require('assert');

describe('Import Message', function() {
  jsdom();

  it('displays import message', function() {
    var React = require('react/addons');
    var ImportMessage = require('../app/public/views/components/messages/import-message.jsx');
    var TestUtils = React.addons.TestUtils;

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);

    var message = {
      expires: tomorrow,
      url: 'http://test-url.com'
    };
    var importMsg = TestUtils.renderIntoDocument(<ImportMessage success={message} />);

    var p = TestUtils.findRenderedDOMComponentWithTag(importMsg, 'p');

    var expiryTime = moment(message.expires);
    var expiry = expiryTime.format('MMMM Do YYYY, h:mm:ss a');
    var expires = expiryTime.fromNow();
    var now = moment();
    var expiredTerm = now.diff(expiryTime) < 0 ? 'expires' : 'expired';


    assert.equal(p.getDOMNode().textContent, 'Your archive is ready and ' + expiredTerm + ' ' + expires + ' (' + expiry + ').');
  });
});
