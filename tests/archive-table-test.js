var jsdom = require('mocha-jsdom');
var assert = require('assert');

describe('Archive Table', function() {
  jsdom();

  this.timeout(60000);

  it('display archive in table', function(done) {
    var React = require('react/addons');
    var ArchiveTable = require('../app/public/views/components/archive/archive-table.jsx');
    var TestUtils = React.addons.TestUtils;

    var tableData = [['test',0.01,1441025538]];

    var archiveTable = TestUtils.renderIntoDocument(<ArchiveTable data={tableData} perPage={5} />);

    var tr = TestUtils.scryRenderedDOMComponentsWithTag(archiveTable, 'tr');
    var th = TestUtils.scryRenderedDOMComponentsWithTag(archiveTable, 'th');
    var td = TestUtils.scryRenderedDOMComponentsWithTag(archiveTable, 'td');

    setTimeout(function() {
      assert.equal(tr.length, 2);
      assert.equal(th.length, 4);
      assert.equal(td.length, 4);
      assert.equal(th[0].getDOMNode().textContent, 'Field');
      assert.equal(td[0].getDOMNode().textContent, 'test');
      done();
    }, 1000);

  });
});
