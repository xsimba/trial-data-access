var jsdom = require('mocha-jsdom');

var assert = require('assert');
var moment = require('moment');

describe('Archive Public View', function() {
  jsdom();
  this.timeout(60000);

  it('show archive data', function(done) {
    var React = require('react/addons');
    var ArchiveIndex = require('../app/public/views/archive/index.jsx');
    var TestUtils = React.addons.TestUtils;

    var tableData = [['test',0.01,1441025538]];
    var keys = ['test'];
    var data = [{test: {s:[0.01],t:[1441025538]}}];
    var metadata = [{key:'test/key', value:'123'}];

    var archiveIndex = TestUtils.renderIntoDocument(<ArchiveIndex tableData={tableData} keys={keys} data={data} metadata={metadata} />);

    var li = TestUtils.scryRenderedDOMComponentsWithTag(archiveIndex, 'li');
    var tr = TestUtils.scryRenderedDOMComponentsWithTag(archiveIndex, 'tr');
    var td = TestUtils.scryRenderedDOMComponentsWithTag(archiveIndex, 'td');

    setTimeout(function() {
      assert.equal(li.length, 6);
      assert.equal(tr.length, 2);
      assert.equal(td.length, 4);
      assert.equal(td[0].getDOMNode().textContent, 'test');
      done();
    }, 1000);

  });
});
