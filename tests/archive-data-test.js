var jsdom = require('mocha-jsdom');

var assert = require('assert');
var moment = require('moment');

describe('Archive Data', function() {
  jsdom();
  this.timeout(60000);

  it('show archive data', function(done) {
    var React = require('react/addons');
    var ArchiveData = require('../app/public/views/components/archive/archive-data.jsx');
    var TestUtils = React.addons.TestUtils;

    var keys = ['test','hello','code'];
    var data = [
      {test: {s:[0.01],t:[1441025538]}},
      {hello: {s:[0.012],t:[1441025539]}},
      {code: {s:[0.0123],t:[1441025540]}}
    ];

    var archiveData = TestUtils.renderIntoDocument(<ArchiveData keys={keys} data={data} />);
    var checkbox = TestUtils.scryRenderedDOMComponentsWithTag(archiveData, 'input');

    setTimeout(function() {
      assert.equal(checkbox.length, 3);
      done();
    }, 1000);

  });
});
