var jsdom = require('mocha-jsdom');

var assert = require('assert');
var moment = require('moment');

describe('Archive Manage', function() {
  jsdom();
  this.timeout(60000);

  it('show archive manage', function(done) {
    var React = require('react/addons');
    var ArchiveManage = require('../app/public/views/components/archive/archive-manage.jsx');
    var TestUtils = React.addons.TestUtils;

    var createdDate = Date.now();

    var archive = {
      canEdit: true,
      created: createdDate,
      expires: new Date('Thu Sep 17 2015 14:28:07 GMT+0100 (BST)'),
      filename: 'd427598e-55eb-4e83-8407-5a37105b7785.json',
      user: '707a1ecaa5934031bae289909fc0bc3d',
      status: 'Removed',
      url: 'http://test.com'
    };

    var addMetadata = function(){};

    var archiveId = 'd427598e-55eb-4e83-8407-5a37105b7785';

    var archiveManage = TestUtils.renderIntoDocument(<ArchiveManage archiveId={archiveId} archive={archive} addMetadata={addMetadata} />);

    var h2 = TestUtils.scryRenderedDOMComponentsWithTag(archiveManage, 'h2');
    var li = TestUtils.scryRenderedDOMComponentsWithTag(archiveManage, 'li');

    setTimeout(function() {
      assert.equal(h2.length, 4);
      assert.equal(h2[0].getDOMNode().textContent, 'Public link');
      assert.equal(h2[1].getDOMNode().textContent, 'Archive');
      assert.equal(h2[2].getDOMNode().textContent, 'Export');
      assert.equal(h2[3].getDOMNode().textContent, 'Metadata');
      assert.equal(li.length, 6);
      assert.equal(li[2].getDOMNode().textContent, 'Create duplicate archive');
      assert.equal(li[3].getDOMNode().textContent, 'Archive CSV');
      done();
    }, 1000);

  });
});
