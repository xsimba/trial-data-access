//var jsdom = require('mocha-jsdom');
//
//var assert = require('assert');
//var moment = require('moment');
//
//describe('Archive List', function() {
//  jsdom();
//  this.timeout(60000);
//
//  it('lists all the archives provided', function(done) {
//    var React = require('react/addons');
//    var ArchiveList = require('../app/public/views/components/archive/archive-list.jsx');
//    var TestUtils = React.addons.TestUtils;
//    var createdDate = Date.now();
//
//    var archives = [
//      {
//        created: createdDate,
//        expires: new Date('Thu Sep 17 2015 14:28:07 GMT+0100 (BST)'),
//        filename: 'd427598e-55eb-4e83-8407-5a37105b7785.json',
//        user: '707a1ecaa5934031bae289909fc0bc3d',
//        status: 'Removed'
//      }
//    ];
//
//    var name = archives[0].filename.replace(/\.[^/.]+$/, '');
//    var expires = moment(archives[0].expires).fromNow();
//    var createdDateString = moment(createdDate).format('MMMM Do YYYY, h:mm:ss a');
//
//    var archiveList = TestUtils.renderIntoDocument(<ArchiveList archives={archives} />);
//    var li = TestUtils.scryRenderedDOMComponentsWithTag(archiveList, 'li');
//
//    setTimeout(function() {
//      assert.equal(li.length, 1);
//      assert.equal(li[0].getDOMNode().textContent, name + createdDateString + '(' + archives[0].status + ' - expired ' + expires + '.)');
//      done();
//    }, 1000);
//
//  });
//});
