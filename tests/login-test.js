var jsdom = require('mocha-jsdom');

var assert = require('assert');

describe('Login / Logout', function() {
  jsdom();

  it('display logout link', function() {
    var React = require('react/addons');
    var Login = require('../app/public/views/components/login.jsx');
    var TestUtils = React.addons.TestUtils;

    var login = true;
    var logout = '/logout';

    var loginLi = TestUtils.renderIntoDocument(<Login loggedIn={login} logout={logout}/>);
    var li = TestUtils.findRenderedDOMComponentWithTag(loginLi, 'li');

    assert.equal(li.getDOMNode().textContent, 'logout');
  }),
  it('display login link', function() {
    var React = require('react/addons');
    var Login = require('../app/public/views/components/login.jsx');
    var TestUtils = React.addons.TestUtils;

    var login = false;
    var logout = '/logout';

    var loginLi = TestUtils.renderIntoDocument(<Login loggedIn={login} logout={logout}/>);
    var li = TestUtils.findRenderedDOMComponentWithTag(loginLi, 'li');

    assert.equal(li.getDOMNode().textContent, 'login');
  });
});
