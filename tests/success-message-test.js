var jsdom = require('mocha-jsdom');
var assert = require('assert');

describe('Success Message', function() {
  jsdom();

  it('displays success message', function() {
    var React = require('react/addons');
    var SuccessMessage = require('../app/public/views/components/messages/success-message.jsx');
    var TestUtils = React.addons.TestUtils;

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);

    var message = 'Complete';
    var successMsg = TestUtils.renderIntoDocument(<SuccessMessage success={message} />);

    var p = TestUtils.findRenderedDOMComponentWithTag(successMsg, 'p');

    assert.equal(p.getDOMNode().textContent, message);
  });
});
