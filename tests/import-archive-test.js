var jsdom = require('mocha-jsdom');
var assert = require('assert');

describe('Import Archive', function() {
  jsdom();

  it('display import archive information', function(done) {
    var React = require('react/addons');
    var ImportArchive = require('../app/public/views/components/sami/import-archive.jsx');
    var TestUtils = React.addons.TestUtils;

    var importingSami = false;
    var submitted = false;

    var importArchive = TestUtils.renderIntoDocument(<ImportArchive importingSami={importingSami} submitted={submitted} />);

    var p = TestUtils.scryRenderedDOMComponentsWithTag(importArchive, 'p');

    assert.equal(p.length, 2);
    assert.equal(p[0].getDOMNode().textContent, 'Create an archive from  SAMI data or a trial session.');
    assert.equal(p[1].getDOMNode().textContent, 'Paste JSON:');
    done();
  });

  it('display importing session message', function(done) {
    var React = require('react/addons');
    var ImportArchive = require('../app/public/views/components/sami/import-archive.jsx');
    var TestUtils = React.addons.TestUtils;

    var importingSami = false;
    var submitted = true;

    var importArchive = TestUtils.renderIntoDocument(<ImportArchive importingSami={importingSami} submitted={submitted} />);

    var p = TestUtils.scryRenderedDOMComponentsWithTag(importArchive, 'p');

    assert.equal(p.length, 2);
    assert.equal(p[0].getDOMNode().textContent, 'Create an archive from  SAMI data or a trial session.');
    assert.equal(p[1].getDOMNode().textContent, 'Importing session ...');
    done();
  });

  it('display importing SAMI message', function(done) {
    var React = require('react/addons');
    var ImportArchive = require('../app/public/views/components/sami/import-archive.jsx');
    var TestUtils = React.addons.TestUtils;

    var importingSami = true;
    var submitted = true;

    var importArchive = TestUtils.renderIntoDocument(<ImportArchive importingSami={importingSami} submitted={submitted} />);

    var p = TestUtils.scryRenderedDOMComponentsWithTag(importArchive, 'p');

    assert.equal(p.length, 2);
    assert.equal(p[0].getDOMNode().textContent, 'Create an archive from  SAMI data or a trial session.');
    assert.equal(p[1].getDOMNode().textContent, 'Importing SAMI data ...');
    done();
  });
});
