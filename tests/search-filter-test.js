var jsdom = require('mocha-jsdom');
var assert = require('assert');

describe('Search Filter', function() {
  jsdom();

  it('display search filter', function(done) {
    var React = require('react/addons');
    var SearchFilter = require('../app/public/views/components/search/search-filter.jsx');
    var TestUtils = React.addons.TestUtils;

    var addSearchFilter = function() {};
    var bool = ['must','must_not'];
    var field = ['_all','archive.metadata.key','archive.metadata.value'];
    var getMetadaKeySuggestions = function() {};
    var totalFilters = 1;
    var isLastFilter = true;
    var queryTypes = ['term','range'];
    var removeSearchFilter = function() {};
    var searchFilter = null;
    var submitted = false;

    var searchFilter = TestUtils.renderIntoDocument(<SearchFilter
      addSearchFilter = {addSearchFilter}
      bool = {bool}
      field = {field}
      getMetadaKeySuggestions = {getMetadaKeySuggestions}
      totalFilters = {totalFilters}
      isLastFilter = {isLastFilter}
      queryTypes = {queryTypes}
      removeSearchFilter = {removeSearchFilter}
      searchFilter = {searchFilter}
      submitted = {submitted}
    />);

    var input = TestUtils.scryRenderedDOMComponentsWithTag(searchFilter, 'input');
    var select = TestUtils.scryRenderedDOMComponentsWithTag(searchFilter, 'select');

    assert.equal(input.length, 1);
    assert.equal(select.length, 2);
    done();
  });

});
