// disabled due to issue with React-Router...

//var jsdom = require('mocha-jsdom');
//
//var assert = require('assert');
//var moment = require('moment');
//
//describe('Search Results', function() {
//  jsdom();
//  this.timeout(60000);
//
//  it('lists all the results provided', function(done) {
//    var React = require('react/addons');
//    var SearchResults = require('../app/public/views/components/search/search-results.jsx');
//    var StubRouterContext = require('../test/helpers/StubRouterContext.js');
//
//    var TestUtils = React.addons.TestUtils;
//
//    var createdDate = Date.now();
//
//    var results = [
//      {
//        _source: {
//          filename: 'd427598e-55eb-4e83-8407-5a37105b7785.json',
//          created: createdDate
//        }
//      },
//      {
//        _source: {
//          filename: 'd427598e-55eb-4e83-8407-5a37105b7786.json',
//          created: createdDate
//        }
//      }
//    ];
//
//    var SearchResults = StubRouterContext(SearchResults, {
//      results : results
//    });
//
//    var archiveList = TestUtils.renderIntoDocument(<SearchResults />);
//    var li = TestUtils.scryRenderedDOMComponentsWithTag(archiveList, 'li');
//
//    setTimeout(function() {
//      assert.equal(li.length, 2);
//      done();
//    }, 1000);
//
//  });
//});
