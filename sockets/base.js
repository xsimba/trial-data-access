
module.exports = function (app) {
  console.log('Set up sockets');
  app.io.on('connection', function(socket){
    if(socket && socket.request && socket.request.session && socket.request.session.hasOwnProperty('passport')
    && socket.request.session.passport.hasOwnProperty('user')) {
      socket.emit('archives', { updated: true });

      app.on('archives', function(data){
        socket.emit('archives',data);
      });
    }
  });
};
