require('node-jsx').install({extension: '.jsx'});

var express = require('express');
var glob = require('glob');

var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compress = require('compression');
var methodOverride = require('method-override');

var i18n = require('i18n-2');

var passport = require('passport')
  , OAuth2Strategy = require('passport-oauth').OAuth2Strategy
  , BearerStrategy = require('passport-http-bearer').Strategy;

var session = require('express-session');

var path = require('path');
var renderer = require('react-engine');

var useragent = require('express-useragent');

module.exports = function(app, config) {
  var env = process.env.NODE_ENV || 'development';
  app.locals.ENV = env;
  app.locals.ENV_DEVELOPMENT = env == 'development';

  // create the view engine with `react-engine`
  var engine = renderer.server.create({
    routes: require(path.join(config.root + '/app/public/routes.jsx')),
    routesFilePath: path.join(config.root + '/app/public/routes.jsx')
  });

  // set the engine
  app.engine('.jsx', engine);

  // set the view directory
  app.set('views', config.root + '/app/public/views');

  // set jsx as the view engine
  app.set('view engine', 'jsx');

  // finally, set the custom view
  app.set('view', renderer.expressView);

  // expose public folder as static assets
  app.use(express.static(config.root + '/app/public'));

  // attach i18n property to the express request object
  i18n.expressBind(app, {
    locales: ['en'],
    defaultLocale: 'en',
    cookieName: 'locale'
  });

  // Set a locale from req.cookies
  app.use(function(req, res, next){
    req.i18n.setLocaleFromCookie();
    next();
  });

  passport.serializeUser(function(user, done) {
    done(null, user);
  });

  passport.deserializeUser(function(user, done) {
    done(null, user);
  });

  passport.use('sami', new OAuth2Strategy({
      authorizationURL: config.oauth2.authorizationURL,
      tokenURL: config.oauth2.tokenURL,
      clientID: config.oauth2.clientID,
      clientSecret: config.oauth2.clientSecret,
      callbackURL: config.oauth2.callbackURL
    },
    function(accessToken, refreshToken, profile, done) {
      //see accessToken in session?
      var user = {
        provider: 'sami',
        name : accessToken
      };
      return done(null, user);
    }
  ));

  passport.use('bearer', new BearerStrategy(
    function(accessToken, done)
    {
      var user = {
        provider: 'bearer',
        name: accessToken
      };
      return done(null, user);
    }
  ));

  app.use(favicon(config.root + '/public/images/favicon.ico'));
  app.use(logger('dev'));
  app.use(bodyParser.json({limit: '50mb'}));
  app.use(bodyParser.urlencoded({
    extended: true
  }));
  app.use(cookieParser());
  app.use(compress());
  app.use(express.static(config.root + '/public'));
  app.use(methodOverride());

  var sess = {
    secret: 'simband',
    cookie: {},
    proxy: true,
    resave: false,
    saveUninitialized: false
  };

  if (app.get('env') === 'production') {
    app.set('trust proxy', 1); // trust first proxy
    sess.cookie.secure = true; // serve secure cookies
  }

  var sessionMiddleware = session(sess);
  app.use(sessionMiddleware);
  app.io.use(function(socket, next){
    sessionMiddleware(socket.request, socket.request.res, next);
  });
  app.use(passport.initialize());
  app.use(passport.session());

  app.use(useragent.express());

  var controllers = glob.sync(config.root + '/app/controllers/*.js');
  controllers.forEach(function (controller) {
    require(controller)(app);
  });

  app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  if(app.get('env') === 'development'){
    app.use(function (err, req, res, next) {
      res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: err,
        title: 'error'
      });
    });
  }

  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: {},
        title: 'error'
      });
  });

};
