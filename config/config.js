var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    dynamodb: 'local',
    aws: {
      key: 'AKIAJMUGKVW7WKF5SIBQ',
      secret: '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',
      region: 'us-west-2'
    },
    s3: {
      bucket: 'trial-archive'
    },
    elastic: {
      local: true,
      host: 'localhost:9200'
    },
    app: {
      name: 'trial-data-access',
      url: 'http://localhost:3000'
    },
    port: 3000,
    logger: {
      name: 'trial-data-access-development',
      type: 'console'
    },
    api: {
      sessions: {
        endpoint: 'http://localhost:9000/v1.1/session-api',
        routes: {
          get: '/sessions/%s'
        }
      },
      sami: {
        endpoint: 'https://api.samsungsami.io/v1.1',
        routes: {
          result: '/messages/export/%s/result',
          status: '/messages/export/%s/status',
          export: '/messages/export',
          profile: '/users/self',
          trials: '/users/%s/trials?role=administrator',
          participants: '/trials/%s/participants',
          devices: '/trials/%s/participants/%s/devices'
        },
        download: {
          exports: '/exports'
        }
      }
    },
    archive: {
      dir: '/archive',
      data: '/data',
      dataUrl: 'https://s3-us-west-2.amazonaws.com/trial-archive/data/',
      url: 'https://s3-us-west-2.amazonaws.com/trial-archive/archive/',
      expiryType: 'min',
      expiry: 2, // 2 mins (based on presence of expiryType)
      proxy: 'http://localhost:3000/archive/id/'
    },
    cron: {
      schedule : '1 * * * * *', // every 1 minute
      timezone : 'America/Los_Angeles',
      elastic: {
        //schedule : '1 * * * * *' // every 1 minute'
        schedule : '0 0 0 * * *' // every 1 day'
      }
    },
    oauth2: {
      authorizationURL: "https://accounts.samsungsami.io/authorize",
      tokenURL: "https://accounts.samsungsami.io/token",
      clientID: "8b5293b6705042bfb5ced15e88b37469",
      clientSecret: "d0e8d63cc8f6439981fe8ce32a51b514",
      callbackURL: "http://localhost:3000/authorize"
    },
    links: {
      logout: "https://accounts.samsungsami.io/logout?redirect_uri=",
      logoutRedirect : "/"
    },
    websocket: {
      url : "http://localhost:3000"
    }
  },

  test: {
    root: rootPath,
    dynamodb: 'local',
    aws: {
      key: 'AKIAJMUGKVW7WKF5SIBQ',
      secret: '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',
      region: 'us-west-2'
    },
    s3: {
      bucket: 'trial-archive'
    },
    elastic: {
      local: true,
      host: 'localhost:9200'
    },
    app: {
      name: 'trial-data-access',
      url: 'http://localhost:3000'
    },
    port: 3000,
    logger: {
      name: 'trial-data-access-test',
      type: 'file'
    },
    api: {
      sessions: {
        endpoint: 'http://trials.simband.io/v1.1/session-api',
        routes: {
          get: '/sessions/%s'
        }
      },
      sami: {
        endpoint: 'https://api.samsungsami.io/v1.1',
        routes: {
          result: '/messages/export/%s/result',
          status: '/messages/export/%s/status',
          export: '/messages/export',
          profile: '/users/self',
          trials: '/users/%s/trials?role=administrator',
          participants: '/trials/%s/participants',
          devices: '/trials/%s/participants/%s/devices'
        },
        download: {
          exports: '/test/resources/exports'
        }
      }
    },
    resources: {
      dir: '/test/resources'
    },
    archive: {
      dir: '/test/resources/archive',
      data: '/test/resources/data',
      dataUrl: 'https://s3-us-west-2.amazonaws.com/trial-archive/data/',
      url: 'https://s3-us-west-2.amazonaws.com/trial-archive/archive/',
      expiry: 30, // 30 days
      proxy: 'http://localhost:3000/archive/id/'
    },
    cron: {
      schedule : '1 * * * * *', // every 1 minute
      timezone : 'America/Los_Angeles',
      elastic: {
        schedule : '0 0 0 * * *' // every 1 day'
      }
    },
    oauth2: {
      authorizationURL: "https://accounts.samsungsami.io/authorize",
      tokenURL: "https://accounts.samsungsami.io/token",
      clientID: "8b5293b6705042bfb5ced15e88b37469",
      clientSecret: "d0e8d63cc8f6439981fe8ce32a51b514",
      callbackURL: "http://localhost:3000/authorize"
    },
    links: {
      logout: "https://accounts.samsungsami.io/logout?redirect_uri=",
      logoutRedirect : "/"
    }
  },

  production: {
    root: rootPath,
    aws: {
      key: 'AKIAJMUGKVW7WKF5SIBQ',
      secret: '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',
      region: 'us-west-2'
    },
    s3: {
      bucket: 'trial-archive'
    },
    elastic: {
      local: false,
      host: 'search-trial-archive-azj2gsyyrvxizzea7y2y5hjtwu.us-west-2.es.amazonaws.com'
    },
    app: {
      name: 'trial-data-access',
      url: 'https://trial-data-access.herokuapp.com'
    },
    port: 3000,
    logger: {
      name: 'trial-data-access-production',
      type: 'console'
    },
    api: {
      sessions: {
        endpoint: 'http://trials.simband.io/v1.1/session-api',
        routes: {
          get: '/sessions/%s'
        }
      },
      sami: {
        endpoint: 'https://api.samsungsami.io/v1.1',
        routes: {
          result: '/messages/export/%s/result',
          status: '/messages/export/%s/status',
          export: '/messages/export',
          profile: '/users/self',
          trials: '/users/%s/trials?role=administrator',
          participants: '/trials/%s/participants',
          devices: '/trials/%s/participants/%s/devices'
        },
        download: {
          exports: '/exports'
        }
      }
    },
    archive: {
      dir: '/archive',
      data: '/data',
      dataUrl: 'https://s3-us-west-2.amazonaws.com/trial-archive/data/',
      url: 'https://s3-us-west-2.amazonaws.com/trial-archive/archive/',
      expiry: 1, // 1 day
      proxy: 'https://trial-data-access.herokuapp.com/archive/id/'
    },
    cron: {
      schedule : '1 * * * * *', // every 1 minute
      timezone : 'America/Los_Angeles',
      elastic: {
        schedule : '0 0 0 * * *' // every 1 day'
      }
    },
    oauth2: {
      authorizationURL: "https://accounts.samsungsami.io/authorize",
      tokenURL: "https://accounts.samsungsami.io/token",
      clientID: "d7d848777a6d4791bd8386c469799696",
      clientSecret: "f6285562dbe44f36bfc9afc64eca75ba",
      callbackURL: "https://trial-data-access.herokuapp.com/authorize"
    },
    links: {
      logout: "https://accounts.samsungsami.io/logout?redirect_uri=",
      logoutRedirect : "/"
    }
  }
};

module.exports = config[env];
