var env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    websocket: {
      url : "http://localhost:3000"
    }
  },

  test: {
    websocket: {
      url : "http://localhost:3000"
    }
  },

  production: {
    websocket: {
      url : "trial-data-access.herokuapp.com"
    }
  }
};

module.exports = config[env];
