var logger = require("../logger")
  , config = require('../../config/config')
  , CronJob = require('cron').CronJob
  , archive = require('../elastic/archive');

var Elastic = function(){};

Elastic.prototype.Tasks = {
  /**
   * Build all indexes now.
   *
   * @constructor
   */
  Startup: function() {
    archive.build(function(err, result){
      if(err) {
        logger.error(err);
      } else {
        logger.info(result);
      }
    });
  }
};

Elastic.prototype.Jobs = {
  /**
   * Scheduled task to create archives elasticsearch index
   * @constructor
   */
  Archive: function() {
    new CronJob(config.cron.elastic.schedule, function() {
      logger.info('Elastic Job: Archive');
      archive.build(function(err, result){
        if(err) {
          logger.error(err);
        } else {
          logger.info(result);
        }
      });
    }, null, true, config.cron.timezone);
  }
};

module.exports = new Elastic();
