var logger = require("../logger")
  , config = require('../../config/config')
  , CronJob = require('cron').CronJob
  , aws_dynamodb = require('../services/aws/dynamodb')
  , aws_s3 = require('../services/aws/s3');

var S3 = function(){};

S3.prototype.Tasks = {
  /**
   * Find all archives that have the status 'New' but an expired date
   * and set to Removed.
   * @constructor
   */
  CleanUpNew: function() {
    logger.info('Clean up archives stuck on New but with an expired date.');
    aws_s3.CleanUpNew();
  },
  /**
   * Find all archives that have the status 'Uploading' and change to 'New'
   * to allow cron to pick it up and try uploading again.
   * @constructor
   */
  CleanUpUploading: function() {
    logger.info('Clean up archives stuck on Uploading.');
    aws_s3.CleanUpUploading();
  },
  /**
   * Find all archives that have the status 'Removing' and change to 'Uploaded'
   * to allow cron to pick it up and try removing again.
   * @constructor
   */
  CleanUpRemoving: function() {
    logger.info('Clean up archives stuck on Removing.');
    aws_s3.CleanUpRemoving();
  },
  RemoveAllDB: function() {
    logger.info('Remove all database entries!');
    aws_dynamodb.RemoveAll();
  }
};

S3.prototype.Jobs = {
  /**
   * Scheduled task to upload files to S3.
   *
   * @constructor
   */
  Upload: function(app) {
    new CronJob(config.cron.schedule, function() {
      aws_s3.Upload();
      if(app) {
        app.emit('archives', { updated: true, action: 'uploaded' });
      }
    }, null, true, config.cron.timezone);
  },
  /**
   * Scheduled task to remove files from S3.
   * @constructor
   */
  Remove: function(app) {
    new CronJob(config.cron.schedule, function() {
      aws_s3.Remove();
      if(app) {
        app.emit('archives', { updated: true, action: 'removed' });
      }
    }, null, true, config.cron.timezone);
  }
};

module.exports = new S3();
