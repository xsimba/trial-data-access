var config = require('../../config/config')
  , dynamoose = require('dynamoose')
  , ArchiveModel = dynamoose.model('ArchiveData')
  , logger = require('../logger')
  , elasticsearch = require('elasticsearch')
  , async = require('async')
  , dynamodb = require('../../lib/services/aws/dynamodb')
  , path = require('path')
  , request = require('superagent')
  , config = require('../../config/config')
  , fs = require('fs')
  , httpawses = require('http-aws-es')
  , _ = require('lodash');

var es_options = {
  hosts: config.elastic.host,
  connectionClass: httpawses,
  amazonES: {
    region: config.aws.region,
    accessKey: config.aws.key,
    secretKey: config.aws.secret
  }
};

if(config.elastic.local === true) {
  es_options = {
    hosts: config.elastic.host,
    log: 'error'
  };
}

var Archive = function() {
  logger.info('elastic archive');
  this.client = new elasticsearch.Client(es_options);
};

Archive.prototype.build = function(done) {
  var self = this;
  self.index(function(err, indexed){
    if(err) return done(err, null);
    logger.info('Archive index creation: ' + JSON.stringify(indexed));
    self.all(function(err, finished){
      if(err) logger.error(err);
      return done(null,'Archive indexing');
    });
  });
};

Archive.prototype.all = function(done) {
  var self = this;

  ArchiveModel.scan().exec(function(err, archives) {
    if (err) return done(err, null);

    logger.info('Add archives to elasticsearch index');

    async.each(archives, function(archive) {
      dynamodb.ArchiveMetadata(archive.filename, function (err, metadata) {
        var name = path.basename(archive.filename, path.extname(archive.filename)).split('-').join('');

        // Get archive data from S3
        var url = config.archive.dataUrl + path.basename(archive.filename, path.extname(archive.filename));

        request
          .get(url)
          .end(function (err, response) {
            if (err) logger.error(err);

            var data = '';

            if(typeof response !== "undefined" && response.hasOwnProperty('body')) {
              data = response.body;
            }

            if(Array.isArray(metadata)) {
              metadata = metadata.map(function (m) {
                var key = _.isObject(m.key) ? JSON.stringify(m.key) : m.key;
                var value = _.isObject(m.value) ? JSON.stringify(m.value) : m.value;
                return {
                  key: key,
                  value: value
                }
              })
            } else {
              metadata = [];
            }

            self.client.index({
              index: 'archives',
              type: 'archive',
              body: {
                filename: archive.filename,
                name: name,
                created: archive.created,
                expires: archive.expires,
                user: archive.user,
                status: archive.status,
                revision: archive.revision,
                metadata: metadata,
                data: JSON.stringify(data),
                timestamp: new Date().getTime().toString()
              }
            }, function (err, response) {
              if (err) return done(err, null);
              logger.info('Indexed: ' + response);
            });
          });
      });
    });

    done(null, 'Indexing archives');
  });
};

Archive.prototype.add = function(archiveId, done) {
  var self = this;

  dynamodb.GetByUUID(archiveId, function(err, archive){
    if (err) return done(err, null);

    dynamodb.ArchiveMetadata(archive.filename, function (err, metadata) {

      // Get archive data from S3
      var url = config.archive.dataUrl + path.basename(archive.filename, path.extname(archive.filename));

      request
        .get(url)
        .end(function (err, response) {
          if (err) logger.error(err);

          var data = '';

          if (typeof response === "undefined" || !response.hasOwnProperty('body')) {
            logger.error('No data found.');
          } else {
            data = response.body;
          }

          var dataFile = path.join(config.root,config.archive.data, archive.filename);
          if(fs.existsSync(dataFile)) {
            data = fs.readFileSync(path.join(config.root,config.archive.data, archive.filename));
          }

          var name = path.basename(archive.filename, path.extname(archive.filename)).split('-').join('');

          if(Array.isArray(metadata)) {
            metadata = metadata.map(function (m) {
              var key = _.isObject(m.key) ? JSON.stringify(m.key) : m.key;
              var value = _.isObject(m.value) ? JSON.stringify(m.value) : m.value;
              return {
                key: key,
                value: value
              }
            })
          } else {
            metadata = [];
          }

          self.client.index({
            index: 'archives',
            type: 'archive',
            body: {
              filename: archive.filename,
              name: name,
              created: archive.created,
              expires: archive.expires,
              user: archive.user,
              status: archive.status,
              revision: archive.revision,
              metadata: metadata,
              data: JSON.stringify(data),
              timestamp: new Date().getTime().toString()
            }
          }, function (err, response) {
            if (err) return done(err, null);
            logger.info('Indexed: ' + response);
          });
        });
    }, function(err) {
      if(err) return done(err, null);
      logger.info('Archive index created.');
      return done(null, 'complete');
    });
  });

};

Archive.prototype.update = function(archiveId, done) {
  var self = this;

  dynamodb.GetByUUID(archiveId, function(err, archive){
    if (err) return done(err, null);

    dynamodb.ArchiveMetadata(archive.filename, function (err, metadata) {

      var name = path.basename(archive.filename, path.extname(archive.filename)).split('-').join('');

      self.client.search({
        index: 'archives',
        body: {
          query: {
            bool: {
              must: {
                term: {
                  "archive.name" : name
                }
              }
            }
          }
        }
      }).then(function (resp) {
        if(resp.hits.total === 0) {
          return self.add(archiveId, done);
        }
        var id = resp.hits.hits[0]._id;

        self.client.update({
          index: 'archives',
          type: 'archive',
          id: id,
          body: {
            doc: {
              created: archive.created,
              expires: archive.expires,
              status: archive.status,
              revision: archive.revision,
              metadata: metadata.map(function(m){
                return {
                  key: m.key,
                  value: m.value
                }
              }),
              timestamp: new Date().getTime().toString()
            }
          }
        }, function(err, response){
          if(err) return done(err, null);
          logger.info('Indexed: ' + response);
        });
      }, function (err) {
        logger.error(err.message);
        return done(err, null);
      });

    }, function(err) {
      if(err) return done(err, null);
      logger.info('Archive index created.');
      return done(null, 'complete');
    });
  });

};

Archive.prototype.index = function(done) {
  var self = this;
  self.delete(function(err, response){
    // If index not found then carry on to attempt to create it.
    if(err && err.status && err.status !== "404") return done(err, null);
    logger.info(response);
    var mapping = {
      mappings: {
        metadata: {
          properties: {
            id: {
              type: 'string'
            },
            filename: {
              type: 'string',
              index: 'not_analyzed' // prevent it being split etc by hyphens http://stackoverflow.com/questions/11566838/elastic-search-hyphen-issue-with-term-filter
            },
            name: {
              type: 'string'
            },
            created: {
              type: 'string'
            },
            expires: {
              type: 'string'
            },
            user: {
              type: 'string'
            },
            status: {
              type: 'string'
            },
            metadata: {
              type: 'nested',
              properties: {
                key: {
                  type: 'string',
                  index: 'not_analyzed'
                },
                value: {
                  type: 'string',
                  index: 'not_analyzed'
                }
              }
            },
            data: {
              type: 'string',
              index: 'not_analyzed'
            },
            timestamp: {
              type: 'string'
            }
          }
        }
      }
    };
    self.client.indices.create({index:'archives', type:'archive', body:JSON.stringify(mapping)}, function(err, response, status){
      logger.info('archive elastic index create: ' + status);
      if(err) return done(err, null);
      return done(null, response);
    });
  });
};

Archive.prototype.delete = function(done) {
  var self = this;
  self.client.indices.delete({index:'archives'},function(err, response, status){
    logger.info('archives elastic index delete: ' + status);
    if(err) return done(err, null);
    return done(null, response);
  });
};

Archive.prototype.search = function(req, done) {
  var pageNum = req.query.page || 1;
  var perPage = req.query.per_page || 15;
  var userQuery = req.query.search_query || '';
  var field = req.query.field || null;
  var q = req.query.q || null;

  var self = this;

  var query = createQuery(userQuery, field, q);

  self.client.search({
    index: 'archives',
    from: (pageNum - 1) * perPage,
    size: perPage,
    body: {
      query: query,
      sort: {'archive.created':{order:'desc'}}
    }
  }).then(function (resp) {
    return done(null, {
      results: resp.hits.hits,
      page: pageNum,
      pages: Math.ceil(resp.hits.total / perPage)
    });
  }, function (err) {
    logger.error(err.message);
    return done(err, null);
  });
};

Archive.prototype.text_search = function(req, done) {
  var pageNum = req.query.page || 1;
  var perPage = req.query.per_page || 15;
  var userQuery = req.query.q || '';

  var self = this;

  var type = 'wildcard';
  if(userQuery.indexOf('*') === -1) type = 'term';

  var query = {
    bool: {
      must: [{}],
      should: [{}]
    }
  };

  query.bool.must[0][type] = {'archive.metadata.value':userQuery};
  query.bool.should[0][type] = {'archive.metadata.key':userQuery};

  self.client.search({
    index: 'archives',
    from: (pageNum - 1) * perPage,
    size: perPage,
    body: {
      query: query,
      sort: {'archive.created':{order:'desc'}}
    }
  }).then(function (resp) {
    return done(null, {
      results: resp.hits.hits,
      page: pageNum,
      pages: Math.ceil(resp.hits.total / perPage)
    });
  }, function (err) {
    logger.error(err.message);
    return done(err, null);
  });
};

Archive.prototype.users_archives = function(req, done) {
  var pageNum = req.query.page || 1;
  var perPage = req.query.per_page || 15;

  var self = this;

  var must = [
    {
      match_all: {}
    }
  ];

  if(req.session.userId) {
    must.push({term: {
      'archive.user' : req.session.userId
    }})
  }

  self.client.search({
    index: 'archives',
    from: (pageNum - 1) * perPage,
    size: perPage,
    body: {
      query: {
        bool: {
          must: must
        }
      },
      sort: {'archive.created':{order:'desc'}}
    }
  }).then(function (resp) {
    return done(null, {
      results: resp.hits.hits,
      page: pageNum,
      pages: Math.ceil(resp.hits.total / perPage)
    });
  }, function (err) {
    logger.error(err.message);
    return done(err, null);
  });
};

Archive.prototype.get_archive = function(archiveId, done) {
  var self = this;

  var name = path.basename(archiveId, path.extname(archiveId)).split('-').join('');

  self.client.search({
    index: 'archives',
    body: {
      query: {
        bool: {
          must: {
            term: {
              "archive.name" : name
            }
          }
        }
      }
    }
  }).then(function (resp) {
    if(resp.hits.total === 0) return done('Not found.', null);
    return done(null, resp.hits.hits[0]);
  });

};


function createQuery(userQuery, field, q) {
  if(field == null && q == null) {
    return {
      filtered: {
        query: {
          match: {
            _all: userQuery
          }
        }
      }
    };
  }

  var query = {
    bool: {
      must: [],
      must_not: []
    }
  };

  if(field) {
    var match = {};
    match[field] = userQuery;
    query.bool.must.push({match : match});
  }

  if(q) {
    q = JSON.parse(q);

    if(q.must) {
      for(var key in q.must) {
        if(q.must.hasOwnProperty(key)) {
          var field = {};
          var value = q.must[key];

          field[key] = value;

          if(typeof value === 'object') {
            if(!value.hasOwnProperty('from') && !value.hasOwnProperty('to')) {
              throw Error('Range values are missing');
            }
            query.bool.must.push({range:field});

          } else {
            query.bool.must.push({match:field});
          }
        }
      }
    }

    if(q.must_not) {
      for(var key in q.must_not) {
        if(q.must_not.hasOwnProperty(key)) {
          var field = {};
          var value = q.must_not[key];
          field[key] = value;

          if(typeof value === 'object') {
            if(!value.hasOwnProperty('from') && !value.hasOwnProperty('to')) {
              throw Error('Range values are missing');
            }
            query.bool.must_not.push({range:field});

          } else {
            query.bool.must_not.push({match:field});
          }
        }
      }
    }

  }

  return query;
}

module.exports = new Archive();
