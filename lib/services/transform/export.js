var logger = require("../../logger/index")
  , async = require('async')
  , _ = require('lodash')
  , fs = require('fs')
  , tar = require('tar')
  , zlib = require('zlib')
  , csv = require('csv-streamify')
  , path = require('path')
  , FileTransform = require('./file')
  , dynamoose = require('dynamoose')
  , uuid = require('uuid');

var Export = function(){};

Export.prototype.Utility = {
  ConvertExportFile: function(exportId, exportDir, filePath, archiveFileName, archiveFilePath, metadata, callback) {
    var self = this;

    if(!exportId) return callback("No exportId", null);
    if(!exportDir) return callback("No exportDir", null);
    if(!filePath) return callback("No filePath", null);

    async.waterfall([
        function(cb) {
          logger.info('Extract export.');
          self.ExtractExportFile(exportId, exportDir, filePath, function (err, exportOutputDir) {
            if (err) return cb(err, null);
            cb(null, exportOutputDir);
          });
        }
        ,
        function(exportOutputDir, cb){
          logger.info('Transform export.');
          FileTransform.transform(exportOutputDir, archiveFileName, archiveFilePath, metadata, function(err, exports){
            if (err) return cb(err, null);
            cb(null, exports);
          });
        }
      ],
      function(err, exports) {
        if(err) return callback(err, null);
        callback(null, exports);
      });
  },
  ExtractExportFile: function(exportId, exportDir, filePath, cb) {
    logger.info("Extracting export " + exportDir + " " + filePath);

    var outputDir = path.join(exportDir, exportId);

    if(!fs.existsSync(outputDir)) fs.mkdirSync(outputDir);

    var extractor = tar.Extract({path:exportDir});
    extractor.on('error', function(err){
      logger.info(error);
      return cb(err, null);
    });
    extractor.on('end', function(){
      logger.info('Export file extracted.');
      fs.unlink(filePath, function(err){
        if(err) logger.error(err);
      });
      cb(null, outputDir);
    });

    var gunZip = zlib.createGunzip();
    gunZip.on('error', function(err){
      return cb(err, null);
    });

    fs.createReadStream(filePath)
      // uncompress
      .pipe(gunZip)
      // extract onto file system
      .pipe(extractor);
  }
};

module.exports = new Export();
