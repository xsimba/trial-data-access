var logger = require('../../logger/index')
  , async = require('async')
  , _ = require('lodash')
  , fs = require('fs')
  , rmdir = require('rimraf')
  , JSONStream = require('JSONStream')
  , csv = require('csv-streamify')
  , path = require('path')
  , Transformer = require('./transform')
  , csvString = require('csv-string')
  , dynamoose = require('dynamoose')
  , uuid = require('uuid')
  , MetadataModel = dynamoose.model('MetadataData')
  , config = require('../../../config/config')
  , s3 = require('../aws/s3');

var File = function(){};

File.prototype.transform = function(outputDir, archiveFileName, archiveFilePath, metadata, cb) {
  logger.info("Transforming export files " + outputDir);

  if(!fs.existsSync(outputDir)) cb(outputDir + " does not exist.", null);

  var files = fs.readdirSync(outputDir);
  logger.info(files.length + " files to transform.");
  if(files.length < 1) cb("No files found.", null);

  var fileType = path.extname(files[0]);

  switch(fileType) {
    case '.json':
      logger.info('Convert json');
      this.transformJsonFiles(outputDir, files, archiveFileName, archiveFilePath, metadata, function(err, result){
        rmdir(outputDir, function(err){
          if(err) logger.info("Unable to remove directory: " + outputDir);
        });
        if(err) cb(err, null);
        cb(null, result);
      });
      break;
    case '.csv':
      logger.info('Convert csv');
      this.transformCsvFiles(outputDir, files, archiveFileName, archiveFilePath, metadata, function(err, result){
        rmdir(outputDir, function(err){
          if(err) logger.info("Unable to remove directory: " + outputDir);
        });
        if(err) cb(err, null);
        cb(null, result);
      });
      break;
    default:
      cb("Unknown file type.", null);
      break;
  }
};

File.prototype.transformJsonFiles = function(dir, files, archiveFileName, archiveFilePath, metadata, cb) {
  var dataType = null;
  var outputFileType = path.extname(archiveFilePath);

  var versions = {};
  var stats = {
    minTimestamp: new Date().valueOf() / 1000, maxTimestamp: 0, totalSampleCount: 0, versions: []
  };
  var isSimband = false;

  var self = this;

  async.map(files, function(file, callback) {
    logger.info("transform " + file);
    var data = [];
    var dataKeys = {};
    var stream = fs.createReadStream(path.join(dir, file));

    var parser = JSONStream.parse('data.*');

    parser.on('data', function(msg){
      if(msg.data) {

        if(!dataType) {
          if(msg.data.payload && msg.data.payload.field) {
            dataType = 'Simband';
            isSimband = true;
          } else {
            dataType = 'Default';
          }
        }

        var results = Transformer.SAMI[dataType](stats, versions, msg.ts, msg.data);
        if(dataType === 'Simband') {
          versions = results.versions;
          stats = results.stats;
        }

        var keys = Object.keys(results.data);
        var key = keys[0];

        if(dataKeys.hasOwnProperty(key)) {
          // aggregate data
          var existingData = data[dataKeys[key]][key];
          if(!existingData.hasOwnProperty('t')) existingData['t'] = {};
          if(!existingData.hasOwnProperty('s')) existingData['s'] = {};
          var t = results.data[key].hasOwnProperty('t') ? results.data[key].t : null;
          var s = results.data[key].hasOwnProperty('s') ? results.data[key].s : null;
          if(t !== null) existingData.t = existingData.t.concat(t);
          if(s !== null) existingData.s = existingData.s.concat(s);
        } else {
          data.push(results.data);
          dataKeys[key] = data.length -1;
        }
      }
    });

    parser.on('end', function(err) {
      if(err) callback(err, null);

      if(isSimband) {
        if(typeof versions !== "undefined" && versions !== null) stats.versions = Object.keys(versions);

        var statObject = {};
        for(s in stats) {
          if(s.indexOf("/sampleCount") > -1) {
            var stat = s.split("/");
            statObject[stat[0]] = {};
            statObject[stat[0]][stat[1]] = stats[s];
          } else {
            statObject[s] = stats[s];
          }

          var newMetadata = new MetadataModel({
            id: uuid.v4(),
            archive: archiveFileName,
            key: 'stats/'+s,
            value: stats[s]
          });

          newMetadata.save(function(err){
            if(err) {
              logger.error(err);
            }
          });
        }

        metadata.stat = statObject;
      }

      self.writeArchiveMetadataToJSONFile(archiveFilePath, metadata, function(err){
        if(err) return callback(err, null);
        callback(err, JSON.stringify(data));
      });
    });

    stream.pipe(parser);

  }, function(err, results) {
    logger.info("All JSON results transformed.");
    if (err) return cb(err, null);

    var dataDir = path.join(config.root,config.archive.data);
    if(!fs.existsSync(dataDir)) fs.mkdirSync(dataDir);
    var dataFilePath = path.join(dataDir, archiveFileName);

    if(results.length > 1) {
      // merge multiple device data.
      var ts = null;
      var data = null;
      results.forEach(function(result){
        if(typeof result !== "undefined") {
          var r = JSON.parse(result)[0];

          if(typeof r !== "undefined") {
            if(r.hasOwnProperty('ts')) {
              if(ts === null || r.ts > ts) ts = r.ts;
            }
            if(r.hasOwnProperty('data')) {
              if(data === null) {
                data = r.data;
              } else {
                var keys = Object.keys(r.data);
                keys.forEach(function(key){
                  if(data.hasOwnProperty(key)) {
                    if(Array.isArray(data[key]) && Array.isArray(r.data[key])) {
                      data[key].concat(r.data[key]);
                    }
                    if(_.isObject(data[key]) && _.isObject(r.data[key])) {
                      Object.assign(data[key],r.data[key]);
                    }
                  } else {
                    data[key] = r.data[key];
                  }
                })
              }
            }
          }
        }
      });
      results = [JSON.stringify([{
        ts: ts,
        data: data
      }])]
    }

    fs.writeFileSync(dataFilePath, results);
    s3.UploadSAMIData();

    if(outputFileType === '.json') {
      self.writeArchiveDataToJSONFile(archiveFilePath, results, function(err){
        if(err) return cb(err, null);
        return cb(null, archiveFilePath);
      });
    } else {
      self.writeArchiveDataToCSVFile(archiveFilePath, results, function(err){
        if(err) return cb(err, null);
        return cb(null, archiveFilePath);
      });
    }

  });
};

File.prototype.transformCsvFiles = function(dir, files, archiveFileName, archiveFilePath, metadata, cb) {
  var dataType = null;
  var outputFileType = path.extname(archiveFilePath);
  var self = this;
  var versions = {};
  var stats = {
    minTimestamp: new Date().valueOf() / 1000, maxTimestamp: 0, totalSampleCount: 0, versions: []
  };
  var isSimband = false;

  async.map(files, function(file, callback) {
    logger.info("transform " + file);
    var data = [];
    var dataKeys = {};
    var stream = fs.createReadStream(path.join(dir, file));
    var parser = csv({objectMode:true});

    parser.on('readable', function () {
      var line = parser.read();
      if(line && parser.lineNo > 0) {

        var ts = line[0];
        var d = JSON.parse(line[line.length-1]);

        if(!dataType) {
          if(d.sampleRate && d.payload && d.payload.field) {
            dataType = 'Simband';
            isSimband = true;
          } else {
            dataType = 'Default';
          }
        }

        var results = Transformer.SAMI[dataType](stats, versions, ts, d);
        versions = results.versions;
        stats = results.stats;

        var keys = Object.keys(results.data);
        var key = keys[0];

        if(dataKeys.hasOwnProperty(key)) {
          // aggregate data
          var existingData = data[dataKeys[key]][key];
          existingData.t = existingData.t.concat(results.data[key].t);
          existingData.s = existingData.s.concat(results.data[key].s);
        } else {
          data.push(results.data);
          dataKeys[key] = data.length -1;
        }
      }
    });

    parser.on('end', function(err) {
      if(err) callback(err, null);

      if(isSimband) {
        stats.versions = Object.keys(versions);

        var statObject = {};
        for(s in stats) {
          if(s.indexOf("/sampleCount") > -1) {
            var stat = s.split("/");
            statObject[stat[0]] = {};
            statObject[stat[0]][stat[1]] = stats[s];
          } else {
            statObject[s] = stats[s];
          }

          var newMetadata = new MetadataModel({
            id: uuid.v4(),
            archive: archiveFileName,
            key: 'stats/'+s,
            value: stats[s]
          });

          newMetadata.save(function(err){
            if(err) {
              logger.error(err);
            }
          });
        }

        metadata.stat = statObject;
      }

      self.writeArchiveMetadataToJSONFile(archiveFilePath, metadata, function(err){
        if(err) return callback(err, null);
        callback(err, JSON.stringify(data));
      });

    });

    stream.pipe(parser);

  }, function(err, results) {
    logger.info("All CSV results transformed.");
    if (err) cb(err, null);

    results = _.reject(results, function(result){
      return result.length < 1;
    });

    var dataDir = path.join(config.root,config.archive.data);
    if(!fs.existsSync(dataDir)) fs.mkdirSync(dataDir);

    var archiveFileNameJson = path.parse(archiveFileName).name + '.json';
    var dataFilePathJson = path.join(dataDir, archiveFileNameJson);

    fs.writeFileSync(dataFilePathJson, results);

    s3.UploadSAMIData();

    if(outputFileType === '.json') {
      self.writeArchiveDataToJSONFile(archiveFilePath, results, function(err){
        if(err) return cb(err, null);
        cb(null, archiveFilePath);
      });
    } else {
      self.writeArchiveDataToCSVFile(archiveFilePath, results, function(err){
        if(err) return cb(err, null);
        cb(null, archiveFilePath);
      });
    }
  });

};

File.prototype.writeArchiveMetadataToJSONFile = function(archiveFilePath, metadata, callback) {
  fs.appendFileSync(archiveFilePath, '"metadata":'+ JSON.stringify(metadata) + ', ');
  callback(null);
};

File.prototype.writeArchiveDataToJSONFile = function(archiveFilePath, results, callback) {
  fs.appendFileSync(archiveFilePath, '"data":');
  results.forEach(function (result) {
    if (result.length > 2) { // string of length 2 will be empty array ([])
      fs.appendFileSync(archiveFilePath, result);
    }
  });
  fs.appendFileSync(archiveFilePath, '}');
  callback(null);
};

File.prototype.writeArchiveDataToCSVFile = function(archiveFilePath, results, callback) {
  var data = fs.readFileSync(archiveFilePath,'utf-8');
  var pos = data.lastIndexOf(',');
  data = data.substring(0,pos) + '}' + data.substring(pos+1)

  var dataJson = JSON.parse(data);
  var session = dataJson.hasOwnProperty('session') ? dataJson.session.data : {};
  var metadata = dataJson.hasOwnProperty('metadata') ? dataJson.metadata : {};

  var headers = ['session', 'metadata', 'data'].join() + "\r\n";
  fs.writeFileSync(archiveFilePath, headers);
  results.forEach(function(result, index, array){
    if(result.length > 2) { // string of length 2 will be empty array ([])
      var data = JSON.parse(result);
      data.forEach(function(row){
        var resultCSVRow = [JSON.stringify(session), JSON.stringify(metadata), JSON.stringify(row)];
        fs.appendFileSync(archiveFilePath, csvString.stringify(resultCSVRow));
      });
    }
  });
  callback(null);
};


module.exports = new File();
