var logger = require('../../logger/index')
  , _ = require('lodash')
  , fs = require('fs')
  , csv = require('csv-streamify')
  , path = require('path')
  , dynamoose = require('dynamoose')
  , uuid = require('uuid');

var Transform = function(){};

var checkValue = function(value) {
  var v = parseFloat(value);
  if(isNaN(v)) value = "0";
  return value;
};

Transform.prototype.SAMI = {
  Default: function(stats, versions, ts, data) {

    var d = {
      ts: ts,
      data: data
    };

    return {
      data: d
    };
  },
  Simband: function(stats, versions, ts, data) {
    if(!data) throw new Error('No data', null);

    var ts = ts / 1000;

    if(typeof stats === "undefined" || !data.hasOwnProperty('payload') || !data.payload.hasOwnProperty('field')) {
      var d = {
        ts: ts,
        data: data
      };

      return {
        data: d
      };
    }

    var field = data.payload.field;

    var rate = data.sampleRate || 1;

    var samples = data[field];

    if(typeof(samples) == "undefined") throw new Error('No data samples', null);

    if(typeof(samples) == "number") samples = [samples];

    stats.minTimestamp = Math.min(stats.minTimestamp, ts);
    stats.maxTimestamp = Math.max(stats.minTimestamp, ts);
    versions[data.payload.version] = true;

    stats[field + '/sampleCount'] = (stats[field + '/sampleCount'] || 0) + samples.length;
    stats.totalSampleCount += samples.length;

    var d = {t:[], s:[]};

    for (var i = 0; i < samples.length; ++i,ts += 1 / rate) {
      var reading = checkValue(samples[i]);
      d.t.push(ts);
      d.s.push(reading);
    }

    var result = {};
    result[field] = d;

    return {
      versions: versions,
      stats: stats,
      data: result
    };

  }
};

Transform.prototype.Table = {
  Archive: function(data, callback) {
    var isSimband = typeof data[0].data === "undefined";

    if(isSimband) {
      this.SimbandArchive(data, function(err, tableData){
        if(err) return callback(err, null);
        callback(null, tableData);
      });
    } else {
      this.DefaultArchive(data, function(err, tableData){
        if(err) return callback(err, null);
        callback(null, tableData);
      });
    }
  },
  SimbandArchive: function(data, callback) {
    var tableData = data.map(function (k) {
      var key = Object.keys(k)[0];
      return k[key].t.map(function (t, i) {
        return [key, k[key].s[i], t];
      });
    });
    return callback(null, _.flatten(tableData));
  },
  DefaultArchive: function(data, callback) {
    var tableData = data.map(function (f) {
      var d = f.data;
      var keys = Object.keys(d);
      return keys.map(function(k, i){
        return [k,d[k],f.ts];
      });
    });
    return callback(null, _.flatten(tableData));
  }
};

Transform.prototype.Object = {
  Archive: function(data, callback) {
    // expect an array or arrays
    var archive = [];
    data.forEach(function (el, i, array) {
      archive.push({field:el[0], timestamp:el[1], sample:el[2]});
    });
    return callback(null, archive);
  }

};

module.exports = new Transform();
