var fs = require('fs')
  , logger = require('../../logger/index')
  , dynamoose = require('dynamoose')
  , MetadataModel = dynamoose.model('MetadataData')
  , dynamodb = require('../aws/dynamodb')
  , _ = require('lodash')
  , uuid = require('uuid')
  , ElasticArchive = require("../../elastic/archive");

var Metadata = function(){};

Metadata.prototype.Utility = {
  Convert: function(sdids, data, archiveFileName, archiveFilePath, callback) {

    var metadata = {
      sami : {
        sdid : sdids
      },
      general : {
        archivedBy : {
          id : data.id,
          email: data.email
        },
        origin : 'sami'
      }
    };

    var keys = ['sami/sdid', 'general/archivedBy/id', 'general/archivedBy/email', 'general/origin'];
    var values = [sdids, data.id, data.email, 'sami'];


    keys.forEach(function(key, index){
      var md = new MetadataModel();
      md.id = uuid.v4();
      md.archive = archiveFileName;
      md.key = key;
      md.value = values[index];
      md.order = index;

      md.save(function(err){
        if(err) logger.error(err);
      });
    });

    callback(null, metadata);
  }
};

Metadata.prototype.Service = {
  Add: function(archiveId, data, callback) {

    dynamodb.GetByUUID(archiveId, function(err, archive){
      if(err) return callback(err, null);

      dynamodb.ArchiveMetadata(archive.filename, function(err, metadata){
        if(err) return callback(err, null);

        var keys = _.uniq(metadata.map(function(m){
          return m.key;
        }));

        if(_.contains(keys, data.key)) {
          return callback('Key already exists', null);
        }

        var newMetadata = new MetadataModel({
          id: uuid.v4(),
          archive: archive.filename,
          key: data.key,
          value: data.value,
          order: metadata.length + 1
        });

        newMetadata.save(function(err){
          if(err) {
            logger.error(err);
            return callback(err, null);
          }
          ElasticArchive.update(archiveId, function(err, message){
            if(err) logger.error(err);
            if(message) logger.info(message);
          });
        });

        callback(null, newMetadata);
      })
    });
  },
  Get: function(archiveId, data, callback) {
    dynamodb.GetByUUID(archiveId, function(err, archive){
      if(err) return callback(err, null);

      dynamodb.ArchiveMetadata(archive.filename, function(err, metadata){
        if(err) return callback(err, null);

        callback(null, metadata);
      })
    });
  }
};

module.exports = new Metadata();
