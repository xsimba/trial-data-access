var logger = require('../../logger')
  , async = require('async')
  , _ = require('lodash')
  , fs = require('fs')
  , path = require('path')
  , dynamoose = require('dynamoose')
  , ArchiveModel = dynamoose.model('ArchiveData')
  , SamiAPI = require('../api/sami')
  , Metadata = require('./metadata')
  , Export = require('../transform/export')
  , config = require('../../../config/config')
  , uuid = require('uuid')
  , s3 = require('../aws/s3')
  , CSVValidator = require('../../../lib/validators/csv')
  , utility = require('./utility')
  , fileTransform = require('../transform/file');

var csv = function(){};

/**
 * Create an Archive from the uploaded CSV.
 *
 *
 * @param req
 * @param callback
 * @returns {*}
 * @constructor
 */
csv.prototype.DataImport = function(req, filepath, callback) {
  var token = utility.getToken(req);
  var archiveDir = path.join(config.root,config.archive.dir);
  if(!fs.existsSync(archiveDir)) fs.mkdirSync(archiveDir);

  var type = '.csv',
      format = 'csv1';

  var archiveFileName = uuid.v4() + type;
  var archiveFilePath = path.join(archiveDir, archiveFileName);

  async.waterfall([
      /**
       * Validate the POSTED CSV.
       *
       * @param cb
       */
        function(cb) {
        CSVValidator.Validate.CSV(req.files.file, function(err, result) {
          if (err && result === false) {
            return cb(JSON.stringify(err), result);
          }
          cb(null, result);
        });
      },
      /**
       * Get user information.
       *
       * @param cb
       */
        function(valid, cb){
        SamiAPI.Users.Profile(token, function(err, result){
          if(err) return cb(err, null);
          if(!result.body || !result.body.data) return cb('No user profile data.', null);

          console.log('user ' + result.body.data);
          if(!req.user) req.user = {};
          req.user.sami = result.body.data;
          console.log(req.user);

          // create the file
          fs.writeFileSync(archiveFilePath, '{');

          Metadata.Utility.Convert(req.body.sdids, result.body.data, archiveFileName, archiveFilePath, function(err, metadata){
            if(err) return cb(err, null);
            cb(null, metadata);
          });
        });
      },
      /**
       * Get the CSV data.
       *
       * @param cb
       */
        function(metadata, cb){

        var dir = path.dirname(filepath);
        var files = [path.basename(filepath)];

        fileTransform.transformCsvFiles(dir, files, archiveFileName, archiveFilePath, metadata, function(err, archiveFilePath){
            if(err) {
              logger.error(err);
              return cb(err, null);
            } else {
              logger.info("CSV file has been converted for " + archiveFilePath);
              s3.Upload(true);
              cb(null, archiveFilePath);
            }
        });
      },
      /**
       * Create an Archive from the gathered data.
       *
       * @param cb
       */
        function(archiveFilePath, cb) {
        var arc = new ArchiveModel();
        var expires = new Date();
        if(config.archive.expiryType && config.archive.expiryType === 'min') {
          // increase by mins
          expires = new Date(expires.getTime() + config.archive.expiry*60000)
        } else {
          // increase by days
          expires.setDate(expires.getDate() + config.archive.expiry);
        }
        arc.created = Date.now();
        arc.expires = expires;
        arc.filename = archiveFileName;
        arc.user = req.user.sami.id;
        arc.save(function(err){
          if(err) return cb(err, null);
          cb(null, {
            url : config.archive.proxy + path.basename(archiveFileName, path.extname(archiveFileName)),
            expires : expires
          });
        });
      }
    ],
    function(err, result) {
      if(err) {
        logger.error(err);
        return callback(err, null);
      }

      logger.info("Archive created.");

      return callback(null, result);
    });
};

module.exports = new csv();
