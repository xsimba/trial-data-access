var logger = require('../../logger')
  , async = require('async')
  , _ = require('lodash')
  , fs = require('fs')
  , path = require('path')
  , dynamoose = require('dynamoose')
  , ArchiveModel = dynamoose.model('ArchiveData')
  , Metadata = require('./metadata')
  , Export = require('../transform/export')
  , config = require('../../../config/config')
  , uuid = require('uuid')
  , s3 = require('../aws/s3')
  , dynamodb = require('../aws/dynamodb')
  , ElasticArchive = require('../../elastic/archive')
  , utility = require('./utility')
  , filtered = require('./filtered');

var aws = function(){};

/**
 * Recreate AWS link from stored data and update archive model.
 *
 * @param req
 * @param callback
 * @constructor
 */
aws.prototype.CreateFromDatabase = function(req, callback) {
  var archiveId = req.params.archiveId;

  async.waterfall([
    /**
     * Get archive and check that is has been removed from AWS.
     *
     * @param callback
     */
      function (callback) {
      dynamodb.GetByUUID(archiveId, function (err, archive) {
        if (err) return callback(err);
        var status = archive.status;
        if(status === 'Importing') status = 'Removed';
        if (status !== 'Removed') return callback('Valid archive already exists.');
        callback(null, archive);
      });
    },
    /**
     * Get filtered data information.
     *
     * @param archive
     * @param callback
     */
    function(archive, callback) {
      filtered.Get(archive.filename, function(err, filters){
        if (err) return callback(err);
        callback(null, archive, filters);
      });
    },
    /**
     * Get metadata.
     *
     * @param archive
     * @param callback
     */
      function (archive, filters, callback) {
      dynamodb.ArchiveMetadata(archive.filename, function (err, metadata) {
        if (err) return callback(err);

        if(metadata.length < 1) return callback(null, archive, '');

        var m = {};
        metadata.forEach(function(element, index, array) {
          var included = true;

          if(filters && filters.metadata) {
            var filter = _.find(filters.metadata, function(f) {
              return f.key === element.key && f.value === element.value;
            });
            if(filter && filter.hidden) included = false;
          }

          if(included) utility.createNestedObject(m, element.key.split('/'), element.value);
        });

        callback(null, archive, m, filters);
      });
    },
    /**
     * Get session.
     *
     * @param archive
     * @param metadata
     * @param callback
     */
      function (archive, metadata, filters, callback) {
      dynamodb.ArchiveSession(archive.filename, function (err, session) {
        if (err) {
          logger.error(err);
          if(err!=='No session found.') return callback(err, null);
        }
        if(err === 'No session found.' || !!session) return callback(null, archive, metadata, '', filters);
        var sessionData = {
          data: {
            id: session.sessionId,
            participantId: session.participantId,
            devices: JSON.parse(session.devices),
            exports: JSON.parse(session.exports),
            metadata: JSON.parse(session.metadata||[]),
            start: session.start,
            end: session.end,
            trialId: session.trialId,
            status: session.status || null
          }
        };
        callback(null, archive, metadata, sessionData, filters);
      });
    },
    /**
     * Write data to file.
     *
     * @param archive
     * @param metadata
     * @param session
     * @param callback
     */
      function (archive, metadata, session, filters, callback) {
      console.log(arguments);
      var archiveDir = path.join(config.root,config.archive.dir);
      if(!fs.existsSync(archiveDir)) fs.mkdirSync(archiveDir);

      var archiveFilePath = path.join(archiveDir, archive.filename);

      fs.writeFileSync(archiveFilePath, '{');
      if(session !== '') fs.appendFileSync(archiveFilePath, '"session":' + JSON.stringify(session) + ', ');
      if(metadata !== '') fs.appendFileSync(archiveFilePath, '"metadata":'+ JSON.stringify(metadata) + ', ');
      callback(null, archive, metadata, session, filters, archiveFilePath);
    },
    /**
     * Get saved SAMI data and write to file.
     *
     * @param archive
     * @param metadata
     * @param session
     * @param filepath
     * @param callback
     */
      function (archive, metadata, session, filters, filepath, callback) {
      s3.SAMIData(archive.filename, function (err, data) {
        if (err) {
          logger.error(err);
        }
        if(err || data.length < 1) {
          return callback('No SAMI data found.');
        }

        // Filters only for items that are SIMBAND...

        if(typeof data !== "object") data = JSON.parse(data);

        var isSimband = typeof data[0].data === "undefined";

        // Apply filters
        if(isSimband && filters && filters.data) {

          var filter = {};
          filters.data.forEach(function(f){
            if(f.field) {
              var key = f.field;
              if(!filter.hasOwnProperty(key)) filter[key] = {t: [], s: []};
              filter[key].t.push(f.timestamp);
              filter[key].s.push(f.sample);
            }
          });

          var filteredDataKeys = {};
          var filteredData = [];

          data.forEach(function(d){
            var key = Object.keys(d)[0];
            var index = filteredData.length;

            if(!filteredDataKeys.hasOwnProperty(key)) {
              filteredDataKeys[key] = filteredData.length;
              var item = {};
              item[key] = {t: [], s: []};
              filteredData.push(item);
            } else {
              index = filteredDataKeys[key];
            }

            var filteredSamplerates = [];
            filteredData[index][key]['t'] = d[key]['t'].filter(function(t, i){
              var s = d[key]['s'][i];

              var foundFilter = false;
              if(filter.hasOwnProperty(key)) {
                var filteredTimestampIndex = filter[key]['t'].indexOf(t);
                if(filteredTimestampIndex > -1) {
                  if(filter[key]['s'][filteredTimestampIndex] === s) {
                    logger.info('Filtered out ' + key + ' ' + s + ' ' + t);
                    foundFilter = true;
                  }
                }
              }

              if(foundFilter) {
                filteredSamplerates.push(i);
                return false;
              }
              return true;
            });
            filteredData[index][key]['s'] = d[key]['s'].filter(function(s, i){
              if(_.contains(filteredSamplerates, i)) return false;
              return true;
            })
          });

          data = filteredData;
        }


        fs.appendFileSync(filepath, '"data":'+ JSON.stringify(data) + '}');
        callback(null, archive, filepath);
      });

    },
    /**
     * Update archive.
     *
     * @param archive
     * @param filepath
     */
      function (archive, filepath) {
      logger.info('Re-created archive ' + filepath);

      var expires = new Date();
      if(config.archive.expiryType && config.archive.expiryType === 'min') {
        // increase by mins
        expires = new Date(expires.getTime() + config.archive.expiry*60000)
      } else {
        // increase by days
        expires.setDate(expires.getDate() + config.archive.expiry);
      }

      ArchiveModel.update(archive, {$PUT: {expires: expires, status: 'New'}, $ADD: {revision: 1}}, function (err) {
        if(err) return callback(err, null);
        dynamodb.GetByUUID(archiveId, function(err, archive){
          if(err) return callback(err, null);
          s3.Upload();
          ElasticArchive.update(archiveId, function(err, message){
            if(err) logger.error(err);
            if(message) logger.info(message);
          });
          archive.url = config.archive.proxy + path.basename(archive.filename, path.extname(archive.filename));
          callback(null, archive);
        });

      });

    }
  ], function (err, archive) {
    if(err) return callback(err, null);
    callback(null, archive);
  });
};

module.exports = new aws();
