var logger = require('../../logger');

var Utility = function(){};

Utility.prototype.getToken = function(req) {
  var token = req.headers.authorization;
  if(!token) {
    token = "Bearer " + req.session.passport.user.name;
  }
  return token;
};

Utility.prototype.createNestedObject = function(base, names, value) {
  var lastName = arguments.length === 3 ? names.pop() : false;

  for( var i = 0; i < names.length; i++ ) {
    base = base[ names[i] ] = base[ names[i] ] || {};
  }

  if( lastName ) base = base[ lastName ] = value;

  return base;
};

module.exports = new Utility();
