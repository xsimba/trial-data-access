var logger = require('../../logger')
  , async = require('async')
  , _ = require('lodash')
  , fs = require('fs')
  , path = require('path')
  , dynamoose = require('dynamoose')
  , ArchiveModel = dynamoose.model('ArchiveData')
  , SamiAPI = require('../api/sami')
  , Metadata = require('./metadata')
  , Export = require('../transform/export')
  , config = require('../../../config/config')
  , uuid = require('uuid')
  , s3 = require('../aws/s3')
  , SamiValidator = require("../../../lib/validators/sami")
  , utility = require('./utility');

var sami = function(){};

/**
 * Create an Archive from the SAMI JSON snippet posted.
 *
 *
 * @param req
 * @param callback
 * @returns {*}
 * @constructor
 */
sami.prototype.DataImport = function(req, callback) {
  var token = utility.getToken(req);
  var archiveDir = path.join(config.root,config.archive.dir);
  if(!fs.existsSync(archiveDir)) fs.mkdirSync(archiveDir);

  var type = '.json';
  var format = 'json';
  if(req.query && req.query.archive && req.query.archive === 'csv') {
    type = '.csv';
    format = 'csv1';
  }

  var archiveFileName = uuid.v4() + type;
  var archiveFilePath = path.join(archiveDir, archiveFileName);

  async.waterfall([
      /**
       * Validate the POSTED JSON.
       *
       * @param cb
       */
        function(cb) {
        SamiValidator.Validate.JSON(req.body, function(err, result) {
          if (err && result === false) {
            return cb(JSON.stringify(err), result);
          }
          cb(null, result);
        });
      },
      /**
       * Create a SAMI export.
       *
       * @param cb
       */
        function(validated, cb){
        var exportData = req.body;
        exportData.order = 'asc';
        exportData.format = format;
        SamiAPI.Exports.Create(token, exportData, function(err, result){
          if(err) return cb(err, null);
          console.log(result);
          cb(null, result);
        });
      },
      /**
       * Get user information.
       *
       * @param cb
       */
        function(exported, cb){
        SamiAPI.Users.Profile(token, function(err, result){
          if(err) return cb(err, null);
          if(!result.body || !result.body.data) return cb('No user profile data.', null);

          if(!req.user) req.user = {};
          req.user.sami = result.body.data;

          // create the file
          fs.writeFileSync(archiveFilePath, '{');

          Metadata.Utility.Convert(req.body.sdids, result.body.data, archiveFileName, archiveFilePath, function(err, metadata){
            if(err) return cb(err, null);
            cb(null, exported, metadata);
          });
        });
      },
      /**
       * Check the status of the SAMI export.
       *
       * @param cb
       */
        function(exported, metadata, cb){
        var exportId = exported.body.data.exportId;

        async.forever(
          function(next) {
            SamiAPI.Exports.Status(token, exportId, function(err, result){
              if(err) return cb(err, null);
              if(!result.body||!result.body.status) return cb('No export status found for ' + exportId + '.', null);
              if(result.body.status === 'Expired') return cb('SAMI export data expired for ' + exportId + '.', null);
              if(result.body.status === 'Success' && result.body.totalMessages === 0) {
                logger.error('SAMI data export found no matching records ' + exportId + '.');
                return cb('There is no data for your selected dates.', null);
              }

              logger.info("SAMI Export status: " + result.body.status);

              if(result.body.status === 'InProgress') {
                setTimeout(next,1000);
              } else {
                return cb(null, metadata, exportId);
              }
            });
          },
          function(err) {
            cb(err);
          }
        )
      },
      /**
       * Get the SAMI export data.
       *
       * @param cb
       */
        function(metadata, exportId, cb){
        SamiAPI.Exports.Result(token,exportId, function(err, result){
          if(err) return cb(err, null);
          if(!result.exportId || !result.exportDir || !result.filePath) return cb("Invalid object", null);

          Export.Utility.ConvertExportFile(result.exportId, result.exportDir, result.filePath, archiveFileName, archiveFilePath, metadata, function(err, exports){
            if(err) {
              logger.error(err);
              return cb(err, null);
            } else {
              logger.info("Export file has been converted for " + archiveFilePath);
              s3.Upload(true);
            }
          });

          cb(null, archiveFilePath);
        });
      },
      /**
       * Create an Archive from the gathered data.
       *
       * @param cb
       */
        function(archiveFilePath, cb) {
        var arc = new ArchiveModel();
        var expires = new Date();
        if(config.archive.expiryType && config.archive.expiryType === 'min') {
          // increase by mins
          expires = new Date(expires.getTime() + config.archive.expiry*60000)
        } else {
          // increase by days
          expires.setDate(expires.getDate() + config.archive.expiry);
        }
        arc.created = Date.now();
        arc.expires = expires;
        arc.filename = archiveFileName;
        arc.user = req.user.sami.id;
        arc.save(function(err){
          if(err) return cb(err, null);
          cb(null, {
            url : config.archive.proxy + path.basename(archiveFileName, path.extname(archiveFileName)),
            expires : expires
          });
        });
      }
    ],
    function(err, result) {
      if(err) {
        logger.error(err);
        return callback(err, null);
      }

      logger.info("Archive created.");

      return callback(null, result);
    });
};

module.exports = new sami();
