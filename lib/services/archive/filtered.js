var logger = require('../../logger')
  , async = require('async')
  , _ = require('lodash')
  , fs = require('fs')
  , path = require('path')
  , dynamoose = require('dynamoose')
  , FilteredModel = dynamoose.model('FilteredData')
  , MetadataModel = dynamoose.model('MetadataData')
  , Metadata = require('./metadata')
  , Export = require('../transform/export')
  , config = require('../../../config/config')
  , uuid = require('uuid')
  , s3 = require('../aws/s3')
  , dynamodb = require('../aws/dynamodb')
  , utility = require('./utility');

var filtered = function(){};

filtered.prototype.Reset = function(req, done) {
  var archive = req.params.archiveId;

  MetadataModel.scan('archive').beginsWith(archive).exec(function(err, allmetadata) {
    if(err) return done(err, null);

    async.each(allmetadata, function(md, callback){
      MetadataModel.update(md, {$PUT: {hidden: false}}, function (err) {
        if(err) {
          logger.error(err);
          return callback(err);
        }
        callback();
      });
    }, function(err) {
      if(err) logger.error(err);

      FilteredModel.scan('archive').beginsWith(archive).exec(function(err, allfilters) {
        if(err || allfilters.length < 1) return done(null, 'No filters');

        async.each(allfilters, function(f, callback){
          f.delete(function(err){
            if(err) logger.error(err);
            callback();
          });
        }, function(err){
          if(err) return done(err, null);
          done(null, 'Reset');
        });
      });
    });

  });
};

filtered.prototype.Import = function(req, done) {
  var load = req.body;
  var archive = req.params.archiveId;

  var data = load.data;
  var metadata = load.metadata;

  MetadataModel.scan('archive').beginsWith(archive).exec(function(err, allmetadata){
    if(err) return done(err, null);

    async.series({
        data: function(cb){
          var filteredData = [];
          data.forEach(function(d){
            filteredData.push({
              field: d[0],
              sample: d[1],
              timestamp: d[2]
            })
          });

          var fm = new FilteredModel({
            id: uuid.v4(),
            archive: archive,
            data: JSON.stringify(filteredData)
          });

          fm.save(function(err){
            if(err) {
              logger.error(err);
              return cb(err);
            }
            logger.info(JSON.stringify(fm));
            logger.info('filter saved');
            cb();
          });
        },
        metadata: function(cb){
          async.each(metadata, function(m, callback){

            var md = _.find(allmetadata, function(am){
              return am.key === m.key && am.value === m.value;
            });

            if(!md) return callback('Missing metadata', null);

            MetadataModel.update(md, {$PUT: {hidden: true}}, function (err) {
              if(err) {
                logger.error(err);
                return callback(err);
              }
              logger.info('Metadata hidden.');
              callback();
            });

          }, function(err){
            if(err) logger.error(err);
            cb();
          });
        }
      },
      function(err) {
        if(err) logger.error(err);
        done(null, 'Imported');
      });
  });
};

filtered.prototype.Get = function(archiveId, done) {
  archiveId = path.parse(archiveId).name;
  logger.info('Get filters for ' + archiveId);
  MetadataModel.scan('archive').beginsWith(archiveId).exec(function(err, allmetadata) {
    if (err) return done(err, null);
    var metadata = _.filter(allmetadata, function(m){
      return m.hidden === true;
    });
    FilteredModel.scan('archive').beginsWith(archiveId).exec(function(err, filters) {
      if (err) return done(err, null);

      var filter = filters[0] || null;

      var data = [];

      if(filter && filter.data) {
        data = JSON.parse(filter.data);
      }

      done(null,{
        data: data,
        metadata: metadata
      })
    });
  });
};


module.exports = new filtered();
