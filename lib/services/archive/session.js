var logger = require('../../logger')
  , async = require('async')
  , _ = require('lodash')
  , fs = require('fs')
  , path = require('path')
  , dynamoose = require('dynamoose')
  , ArchiveModel = dynamoose.model('ArchiveData')
  , SessionAPI = require('../api/session')
  , SamiAPI = require('../api/sami')
  , Metadata = require('./metadata')
  , Export = require('../transform/export')
  , config = require('../../../config/config')
  , uuid = require('uuid')
  , s3 = require('../aws/s3')
  , dynamodb = require('../aws/dynamodb')
  , SessionValidator = require("../../../lib/validators/session")
  , utility = require('./utility');

var session = function(){};

/**
 * Create an Archive from the session JSON snippet posted.
 *
 *
 * @param req
 * @param callback
 * @returns {*}
 * @constructor
 */
session.prototype.DataImport = function(req, callback) {
  var token = utility.getToken(req);
  var sessionId = req.body.sessionId;
  var exportId = req.body.exportId;
  var archiveDir = path.join(config.root,config.archive.dir);
  if(!fs.existsSync(archiveDir)) fs.mkdirSync(archiveDir);

  var type = '.json';
  if(req.query && req.query.archive && req.query.archive === 'csv') {
    type = '.csv';
  }

  var archiveFileName = uuid.v4() + type;
  var archiveFilePath = path.join(archiveDir, archiveFileName);

  async.waterfall([
      /**
       * Validate the POSTED JSON.
       *
       * @param cb
       */
        function(cb) {
        SessionValidator.Validate.JSON(req.body, function(err, result) {
          if (err && result === false) {
            return cb(JSON.stringify(err), result);
          }
          cb(null, result);
        });
      },
      /**
       * Get session data.
       *
       * @param cb
       */
        function(validate, cb){
        SessionAPI.Sessions.Get(token, sessionId, function(err, result){
          if(err) {
            logger.error(err);
            return cb('No session data.', null);
          }
          SessionAPI.Sessions.Convert(result.text, archiveFileName, archiveFilePath, function(err, session){
            if(err) return cb(err, null);
            cb(null, session);
          });
        });
      },
      /**
       * Get user information.
       *
       * @param cb
       */
        function(session, cb){
        SamiAPI.Users.Profile(token, function(err, result){
          if(err) logger.error(err);
          if(err || !result.body || !result.body.data) return cb('No user profile data.', null);

          console.log('user ' + result.body.data);
          if(!req.user) req.user = {};
          req.user.sami = result.body.data;
          console.log(req.user);

          Metadata.Utility.Convert(req.body.sdids, result.body.data, archiveFileName, archiveFilePath, function(err, metadata){
            if(err) return cb(err, null);
            cb(null, metadata);

          });
        });
      },
      /**
       * Check the status of the SAMI export.
       *
       * @param cb
       */
        function(metadata, cb){
        SamiAPI.Exports.Status(token, exportId, function(err, result){
          if(err) logger.error(err);
          if(err||!result.body||!result.body.status) return cb('No export status found for ' + exportId + '.', null);
          if(result.body.status === 'Expired') return cb('SAMI export data expired for ' + exportId + '.', null);
          cb(null, metadata, result.body.status);
        });
      },
      /**
       * Get the SAMI export data.
       *
       * @param cb
       */
        function(metadata, status, cb){
        SamiAPI.Exports.Result(token, exportId, function(err, result){
          if(err) {
            if(err) logger.error(err);
            return cb('No SAMI export data.', null);
          }
          if(!result.exportId || !result.exportDir || !result.filePath) return cb("Invalid object", null);

          Export.Utility.ConvertExportFile(result.exportId, result.exportDir, result.filePath, archiveFileName, archiveFilePath, metadata, function(err, exports){
            if(err) {
              logger.error(err);
            } else {
              logger.info("Export file has been converted for " + archiveFilePath);
              s3.Upload(true);
            }
          });

          cb(null, archiveFilePath);
        });
      },
      /**
       * Create an Archive from the gathered data.
       *
       * @param cb
       */
        function(archiveFilePath, cb) {
        var arc = new ArchiveModel();
        var expires = new Date();
        if(config.archive.expiryType && config.archive.expiryType === 'min') {
          // increase by mins
          expires = new Date(expires.getTime() + config.archive.expiry*60000)
        } else {
          // increase by days
          expires.setDate(expires.getDate() + config.archive.expiry);
        }
        arc.created = Date.now();
        arc.expires = expires;
        arc.filename = archiveFileName;
        arc.user = req.user.sami.id;
        arc.save(function(err){
          if(err) return cb(err, null);
          cb(null, {
            url : config.archive.proxy + path.basename(archiveFileName, path.extname(archiveFileName)),
            expires : expires
          });
        });
      }
    ],
    function(err, archive) {
      if(err) {
        return callback(err, null);
      }

      logger.info("Archive created.");

      return callback(null, archive);
    });
};

session.prototype.GetByUUID = function(req, anon, callback) {
  if(!req.params||!req.params.archiveId) return callback("No archive ID", null);

  var archiveId = req.params.archiveId;

  var token = anon ? null : utility.getToken(req);

  dynamodb.GetByUUID(archiveId, function(err, archive){
    if(err) return callback("No archive found.", null);

    archive.url = config.archive.url + archiveId;
    archive.type = path.extname(archive.filename);

    if(anon) return callback(null, archive);

    if(req.user && req.user.sami) {
      archive.canEdit = (archive.user === req.user.sami.id);
      return callback(null, archive);
    }

    SamiAPI.Users.Profile(token, function (err, result) {
      if (err) return callback(err, null);
      if (!result.body || !result.body.data) return cb('No user profile data.', null);

      if(!req.user) req.user = {};
      req.user.sami = result.body.data;
      archive.canEdit = (archive.user === req.user.sami.id);
      return callback(null, archive);
    });
  });
};

module.exports = new session();
