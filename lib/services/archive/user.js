var logger = require('../../logger')
  , async = require('async')
  , _ = require('lodash')
  , fs = require('fs')
  , path = require('path')
  , dynamoose = require('dynamoose')
  , SamiAPI = require('../api/sami')
  , Metadata = require('./metadata')
  , Export = require('../transform/export')
  , config = require('../../../config/config')
  , uuid = require('uuid')
  , dynamodb = require('../aws/dynamodb')
  , utility = require('./utility');

var user = function(){};

user.prototype.List = function(req, callback) {
    var token = utility.getToken(req);
    // get user from session or sami
    async.series({
        user: function (cb) {
          if (req.user && req.user.sami && req.user.sami.id) {
            cb(null, req.user.sami);
          } else {
            SamiAPI.Users.Profile(token, function (err, result) {
              if (err) return cb(err, null);
              if (!result.body || !result.body.data) return cb('No user profile data.', null);
              if(!req.user) req.user = {};
              req.user.sami = result.body.data;
              cb(null, result);
            });
          }
        },
        // get archives for user
        archives: function (cb) {
          dynamodb.UserArchives(req.user.sami.id, function(err, result) {
            if (err) return cb(err, null);
            cb(null, result);
          });
        }
      },
      function (err, results) {
        if (err) return callback(err, null);

        callback(null, results['archives']);
      });
  };
user.prototype.IsOwner = function(req, archiveId, callback) {
    var token = utility.getToken(req);
    dynamodb.GetByUUID(archiveId, function (err, archive) {
      if (err) return callback(err);
      SamiAPI.Users.Profile(token, function(err, result) {
        if (err) return callback(err, null);
        if(!result.body || !result.body.data) return cb('No user profile data.', null);
        var user = result.body.data.id;
        callback(null, archive.user === user);
      });
    });
  };

module.exports = new user();
