var logger = require('../../logger')
  , async = require('async')
  , _ = require('lodash')
  , fs = require('fs')
  , path = require('path')
  , dynamoose = require('dynamoose')
  , ArchiveModel = dynamoose.model('ArchiveData')
  , MetadataModel = dynamoose.model('MetadataData')
  , Metadata = require('./metadata')
  , Export = require('../transform/export')
  , config = require('../../../config/config')
  , uuid = require('uuid')
  , s3 = require('../aws/s3')
  , utility = require('./utility')
  , es_archive = require('../../../lib/elastic/archive')
  , dynamodb = require('../aws/dynamodb')
  , uuid = require('uuid')
  , EventEmitter = require('events');

var duplicate = function(){};

duplicate.prototype.Create = function(req, archiveId, callback) {

  var newArchiveId = uuid.v4();
  var archiveFileName = newArchiveId + '.json';

  async.series({
    archive: function(cb) {
      dynamodb.GetByUUID(archiveId, function(err, archive){
        if(err) return cb(err, null);
        logger.info('Found archive ' + archiveId);
        cb(null, archive);
      });
    },
    metadata: function(cb) {
      dynamodb.ArchiveMetadataScan(archiveId, function(err, metadata){
        if(err) return cb(err, null);
        logger.info('Found archive\'s metadata ' + archiveId);
        cb(null, metadata);
      })
    },
    elastic: function(cb) {
      es_archive.get_archive(archiveId, function(err, archive){
        if(err) return cb(err, null);
        logger.info('Found archive data ' + archiveId);
        if(!archive.hasOwnProperty('_source') && !archive._source.hasOwnProperty('data')) {
          return cb('No data found.', null);
        }

        var data = archive._source.data;
        if(typeof data !== "object") data = JSON.parse(data);

        var dataDir = path.join(config.root,config.archive.data);
        if(!fs.existsSync(dataDir)) fs.mkdirSync(dataDir);
        var dataFilePath = path.join(dataDir, archiveFileName);

        fs.writeFileSync(dataFilePath, JSON.stringify(data));
        s3.UploadSAMIData(function(err, message){
          if(err) return cb(err, null);
          logger.info('Uploaded ' + archiveId + ' data to S3.');
          cb(null, message);
        });
      });
    }
  }, function(err, results){
    if(err) return callback(err, null);

    var expires = new Date();
    if(config.archive.expiryType && config.archive.expiryType === 'min') {
      // increase by mins
      expires = new Date(expires.getTime() + config.archive.expiry*60000)
    } else {
      // increase by days
      expires.setDate(expires.getDate() + config.archive.expiry);
    }
    var archive = new ArchiveModel({
      created: Date.now(),
      filename: archiveFileName,
      user: req.user.sami.id,
      parent: archiveId,
      duplicatedVersion: results.archive.version
    });

    archive.save(function(err){
      if(err) return callback(err, null);
      logger.info('Created ' + archiveId);

      results.metadata.forEach(function(metadata){
        var m = new MetadataModel({
          id : uuid.v4(),
          archive : archiveFileName,
          key : metadata.key,
          value : metadata.value,
          order : metadata.order
        });
        m.save(function(err){
          if(err) logger.error(err);
        })
      });

      es_archive.add(archiveFileName, function(err, complete){
        if(err) {
          logger.error(err);
        } else {
          logger.info(archiveId + ' elastic index added: ' + complete);
        }
      });
      callback(null, archive);
    });

  });
};

module.exports = new duplicate();
