var logger = require("../../logger/index")
  , _ = require('lodash')
  , path = require('path')
  , util = require('util')
  , fs = require('fs')
  , request = require('superagent')
  , config = require("../../../config/config");

var SamiAPI = function(){};

function getSAMIError(err) {
  if(err.response && err.response.body && err.response.body.error && err.response.body.error.message) {
    return err.response.body.error.message;
  } else {
    return err;
  }
}

SamiAPI.prototype.Users = {
  Profile: function(token, callback) {
    var url = config.api.sami.endpoint
      + config.api.sami.routes.profile;

    logger.info("Get." + url);

    request
      .get(url)
      .set('Authorization', token)
      .set('Accept', 'application/json')
      .end(function(err, res){
        if(err) return callback(getSAMIError(err), null);
        callback(null, res);
      });
  },
  Trials: function(userId, token, callback) {
    var url = config.api.sami.endpoint
      + util.format(config.api.sami.routes.trials, userId);

    logger.info("Get." + url);

    request
      .get(url)
      .set('Authorization', token)
      .set('Accept', 'application/json')
      .end(function(err, res){
        if(err) return callback(getSAMIError(err), null);
        callback(null, res);
      });
  }
};

SamiAPI.prototype.Trials = {
  Participants: function(trialId, token, callback) {
    var url = config.api.sami.endpoint
      + util.format(config.api.sami.routes.participants, trialId);

    logger.info("Get." + url);

    request
      .get(url)
      .set('Authorization', token)
      .set('Accept', 'application/json')
      .end(function(err, res){
        if(err) return callback(getSAMIError(err), null);
        callback(null, res);
      });
  },
  UserDevices: function(trialId, participantId, token, callback) {
    var url = config.api.sami.endpoint
      + util.format(config.api.sami.routes.devices, trialId, participantId);

    logger.info("Get." + url);

    request
      .get(url)
      .set('Authorization', token)
      .set('Accept', 'application/json')
      .end(function(err, res){
        if(err) return callback(getSAMIError(err), null);
        callback(null, res);
      });
  }
};


SamiAPI.prototype.Exports = {
  Create: function(token, exportData, callback) {
    var url = config.api.sami.endpoint + config.api.sami.routes.export;

    logger.info('Post.' + url);

    request
      .post(url)
      .send(exportData)
      .set('Authorization', token)
      .set('Accept', 'application/json')
      .end(function(err, res){
        if(err) return callback(getSAMIError(err), null);
        callback(null, res);
      });
  },
  Status: function(token, id, callback) {
    var url = config.api.sami.endpoint
      + util.format(config.api.sami.routes.status, id);

    logger.info("Get." + url);

    request
      .get(url)
      .set('Authorization', token)
      .set('Accept', 'application/json')
      .end(function(err, res){
        if(err) return callback(getSAMIError(err), null);
        callback(null, res);
      });
  },
  Result: function(token, id, callback) {
    var url = config.api.sami.endpoint
      + util.format(config.api.sami.routes.result, id);

    logger.info("Get." + url);

    var exportDir = path.join(config.root,config.api.sami.download.exports);

    if(!fs.existsSync(exportDir)) fs.mkdirSync(exportDir);

    var filePath = path.join(exportDir, id + '.tgz');
    var stream = fs.createWriteStream(filePath);

    var req = request
      .get(url)
      .set('Authorization', token)
      .set('Accept', 'application/x-compressed')
      .buffer();

    req.pipe(stream);

    req.on('end', function(err, res){
      if(err) return callback(getSAMIError(err), null);

      logger.info("Downloaded: " + exportDir);

      callback(null, {
        exportId: id,
        exportDir: exportDir,
        filePath: filePath
      });
    });
  }
};

module.exports = new SamiAPI();
