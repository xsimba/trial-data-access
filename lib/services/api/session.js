var logger = require("../../logger/index")
  , _ = require('lodash')
  , util = require('util')
  , request = require('superagent')
  , config = require("../../../config/config")
  , fs = require('fs')
  , dynamoose = require('dynamoose')
  , SessionModel = dynamoose.model('SessionData');

var SessionAPI = function(){};

function getSessionError(err) {
  if(err.response && err.response.body && err.response.body.error && err.response.body.error.message) {
    return err.response.body.error.message;
  } else {
    return err;
  }
}

SessionAPI.prototype.Sessions = {
  Get: function(token, id, callback) {
    var url = config.api.sessions.endpoint
      + util.format(config.api.sessions.routes.get, id);

    logger.info("Get." + url);

    request
      .get(url)
      .set('Authorization', token)
      .set('Accept', 'application/json')
      .end(function(err, res){
        if(err) return callback(getSessionError(err), null);
        return callback(null, res);
      });
  },
  Convert: function(session, archiveFileName, archiveFilePath, callback) {
    fs.writeFileSync(archiveFilePath, '{"session":' + session + ', ');

    var sessionModel = new SessionModel();

    var s = JSON.parse(session);
    s = s.data;

    sessionModel.archive = archiveFileName;
    sessionModel.sessionId = s.id;
    sessionModel.participantId = s.participantId;
    sessionModel.devices = JSON.stringify(s.devices);
    sessionModel.exports = JSON.stringify(s.exports);
    sessionModel.metadata = JSON.stringify(s.metadata);
    sessionModel.start = s.start;
    sessionModel.end = s.end;
    sessionModel.trialId = s.trialId;
    sessionModel.status = s.status;

    sessionModel.save(function(err){
      if(err) logger.error(err);
    });

    callback(null, archiveFilePath);
  }
};

module.exports = new SessionAPI();
