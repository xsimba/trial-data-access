var logger = require('../../logger')
  , fs = require('fs')
  , path = require('path')
  , dynamoose = require('dynamoose')
  , ArchiveModel = dynamoose.model('ArchiveData')
  , MetadataModel = dynamoose.model('MetadataData')
  , SessionModel = dynamoose.model('SessionData')
  , SAMIModel = dynamoose.model('SAMIData')
  , FilterModel = dynamoose.model('FilteredData')
  , config = require('../../../config/config')
  , _ = require('lodash');

var dynamodb = function(){};

dynamodb.prototype.GetByUUID = function(archiveID, callback) {
  ArchiveModel.scan('filename').beginsWith(archiveID).exec(function(err, archives){
    if(err) {
      logger.error(err);
      return callback(err, null);
    }
    if(archives.length < 1) return callback("No archives found.", null);

    return callback(null, archives[0]);
  });
};

dynamodb.prototype.GetMetadataByUUID = function(metadataUUID, callback) {
  MetadataModel.scan('id').beginsWith(metadataUUID).exec(function(err, metadatas){
    if(err) {
      logger.error(err);
      return callback(err, null);
    }
    if(metadatas.length < 1) return callback("No metadata found.", null);

    return callback(null, metadatas[0]);
  });
};

dynamodb.prototype.Status = function(archiveID, callback) {
  ArchiveModel.scan('filename').beginsWith(archiveID).exec(function(err, archives){
    if(err) {
      logger.error(err);
      return callback(err, null);
    }
    if(archives.length < 1) return callback("No archives found.", null);

    return callback(null, archives[0].status);
  });
};

dynamodb.prototype.UserArchives = function(userId, callback) {
  ArchiveModel.scan('user').eq(userId).exec(function(err, archives){
    if(err) {
      logger.error(err);
      return callback(err, null);
    }
    archives = archives.sort(function(a,b) {
      return new Date(b.expires) - new Date(a.expires);
    });

    archives = _.reject(archives, function(archive){
      return !archive.hasOwnProperty("created");
    });

    return callback(null, archives.slice(0,5));
  });
};

dynamodb.prototype.MetadataKeys = function(callback) {
  MetadataModel.scan().exec(function(err, metadata){
    if(err) {
      logger.error(err);
      return callback(err, null);
    }

    var keys = _.uniq(metadata.map(function(m){
      return m.key;
    }));

    return callback(null, keys);
  })
};

dynamodb.prototype.ArchiveMetadata = function(archive, callback) {
  MetadataModel.scan('archive').eq(archive).exec(function(err, metadata){
    if(err) {
      logger.error(err);
      return callback(err, null);
    }

    return callback(null, metadata);
  })
};

dynamodb.prototype.ArchiveMetadataScan = function(archiveID, callback) {
  MetadataModel.scan('archive').beginsWith(archiveID).exec(function(err, metadata){
    if(err) {
      logger.error(err);
      return callback(err, null);
    }

    return callback(null, metadata);
  })
};

dynamodb.prototype.ArchiveSession = function(archive, callback) {
  SessionModel.scan('archive').eq(archive).exec(function(err, session){
    if(err) {
      logger.error(err);
      return callback(err, null);
    }

    if(session.length<1) return callback('No session found.', null);

    return callback(null, session[0]);
  })
};

dynamodb.prototype.SAMIData = function(archive, callback) {
  SAMIModel.scan('archive').eq(archive).exec(function(err, data){
    if(err) {
      logger.error(err);
      return callback(err, null);
    }

    return callback(null, data);
  })
};

dynamodb.prototype.GetSAMIDataByUUID = function(archiveID, callback) {
  SAMIModel.scan('archive').beginsWith(archiveID).exec(function(err, data){
    if(err) {
      logger.error(err);
      return callback(err, null);
    }

    return callback(null, data);
  })
};

dynamodb.prototype.ArchiveMetadataKeySearch = function(query, callback) {
  MetadataModel.scan('key').contains(query).exec(function(err, metadata){
    if(err) {
      logger.error(err);
      return callback(err, null);
    }

    metadata = _.uniq(metadata.map(function(m){
      return m.key
    }));

    return callback(null, metadata);
  })
};

dynamodb.prototype.RemoveArchive = function() {
  ArchiveModel.$__.table.delete(function (err) {
    if(err) {
      logger.error(err);
    }
    delete ArchiveModel;
    logger.info('Deleted Archive DB');
  });
};

dynamodb.prototype.RemoveMetadata = function() {
  MetadataModel.$__.table.delete(function (err) {
    if(err) {
      logger.error(err);
    }
    delete MetadataModel;
    logger.info('Deleted Metadata DB');
  });
};

dynamodb.prototype.RemoveSession = function() {
  SessionModel.$__.table.delete(function (err) {
    if(err) {
      logger.error(err);
    }
    delete SessionModel;
    logger.info('Deleted Session DB');
  });
};

dynamodb.prototype.RemoveSAMI = function() {
  SAMIModel.$__.table.delete(function (err) {
    if(err) {
      logger.error(err);
    }
    delete SAMIModel;
    logger.info('Deleted SAMI DB');
  });
};

dynamodb.prototype.RemoveFilter = function() {
  FilterModel.$__.table.delete(function (err) {
    if(err) {
      logger.error(err);
    }
    delete FilterModel;
    logger.info('Deleted Filtered DB');
  });
};

dynamodb.prototype.RemoveAll = function() {
  this.RemoveArchive();
  this.RemoveMetadata();
  this.RemoveSession();
  this.RemoveSAMI();
  this.RemoveFilter();
};

module.exports = new dynamodb();
