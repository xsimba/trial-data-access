var logger = require('../../logger')
  , fs = require('fs')
  , path = require('path')
  , dynamoose = require('dynamoose')
  , ArchiveModel = dynamoose.model('ArchiveData')
  , knox = require('knox')
  , config = require('../../../config/config')
  , _ = require('lodash')
  , dir = require('node-dir')
  , ElasticArchive = require("../../elastic/archive")
  , request = require('superagent');

var s3Client = knox.createClient({
  key: config.aws.key
  , secret: config.aws.secret
  , bucket: config.s3.bucket
});

var s3 = function(){};

/**
 * Upload files to S3.
 * @constructor
 */
s3.prototype.Upload = function(index) {
  ArchiveModel.scan('status').eq('New')
    .filter('expires').gt(new Date())
    .exec(function(err, archives){
      if(err) throw err;
      archives.forEach(function(archive){
        var archiveDir = path.join(config.root,config.archive.dir);
        var archiveFilePath = path.join(archiveDir, archive.filename);
        var archiveName = 'archive/' + path.basename(archive.filename, path.extname(archive.filename));

        fs.stat(archiveFilePath, function(err, stat) {

          archive.status = 'Uploading';
          archive.save(function(err){
            if(err) logger.info(err);
          });

          var contentType = 'application/json';
          if(path.extname(archive.filename) === '.csv') contentType = 'application/vnd.ms-excel';

          var headers = {
            'Content-Length': stat.size,
            'Content-Type': contentType,
            'x-amz-acl': 'public-read'
          };

          s3Client.putFile(archiveFilePath, archiveName, headers, function(err, resp) {
            if (err) {
              logger.error(err);

              archive.status = 'New';
              archive.save(function(err){
                if(err) logger.error(err);
                logger.info('Reverted file status to "New".');
              });
            } else {
              logger.info('Uploaded ' + archive.filename + ' to S3.');
              archive.status = 'Uploaded';
              archive.save(function(err){
                if(err) logger.error(err);
                logger.info('Updated file status to "Uploaded".');
                if(index) {
                  logger.info('Add new archive ' + archive.filename + ' to elastic index');
                  ElasticArchive.add(archive.filename, function(err, message){
                    if(err) logger.error(err);
                    if(message) logger.info(message);
                  });
                }
                if(!err) {
                  // remove file.
                  fs.unlink(archiveFilePath, function(err){
                    if(err) logger.error(err);
                  });
                }
              });
            }
          });
        });
      });
    });
};

/**
 * Remove objects from S3.
 * @constructor
 */
s3.prototype.Remove = function() {
  ArchiveModel
    .scan ('status')
    .eq('Uploaded')
    .filter('expires').le(new Date())
    .exec(function(err, archives){
      if(err) throw err;
      archives.forEach(function(archive){
        var archiveName = 'archive/' + path.basename(archive.filename, path.extname(archive.filename));

        archive.status = 'Removing';
        archive.save(function(err){
          if(err) logger.info(err);
        });

        s3Client.deleteFile(archiveName, function(err, res){
          if(err) {
            logger.error(err);
            archive.status = 'Uploaded';
            archive.save(function(err){
              if(err) logger.error(err);
              logger.info('Reverted file status to "Uploaded".');
            });
          } else {
            logger.info('Removed ' + archive.filename + ' from S3.');
            archive.status = 'Removed';
            archive.save(function(err){
              if(err) logger.error(err);
              logger.info('Updated file status to "Removed".');
            });
          }
        });
      });
    });
};

/**
 * Mark archives with status New but an expired date as Removed.
 * @constructor
 */
s3.prototype.CleanUpNew = function() {
  ArchiveModel.scan('status').eq('New')
    .filter('expires').lt(new Date())
    .exec(function(err, archives){
      if (err) throw err;
      archives.forEach(function (archive) {
        archive.status = 'Removed';
        archive.save(function (err) {
          if (err) logger.info(err);
        });
      });
    });
};

/**
 * Change archives from Uploading to New.
 * @constructor
 */
s3.prototype.CleanUpUploading = function() {
  ArchiveModel.scan('status').eq('Uploading')
    .exec(function(err, archives) {
      if (err) throw err;
      archives.forEach(function (archive) {
        archive.status = 'New';
        archive.save(function (err) {
          if (err) logger.info(err);
        });
      });
    });
};

/**
 * Change archives from Removing to Uploaded.
 * @constructor
 */
s3.prototype.CleanUpRemoving = function() {
  ArchiveModel.scan('status').eq('Removing')
    .exec(function(err, archives) {
      if (err) throw err;
      archives.forEach(function (archive) {
        archive.status = 'Uploaded';
        archive.save(function (err) {
          if (err) logger.info(err);
        });
      });
    });
};

/**
 * Upload all SAMI data files.
 * @constructor
 */
s3.prototype.UploadSAMIData = function(callback) {
  var dataDir = path.join(config.root,config.archive.data);

  dir.files(dataDir, function(err, files) {
    if (err) throw err;

    files.forEach(function(file) {
      var archiveName = path.basename(file, path.extname(file));
      var name = 'data/' + archiveName;

      fs.stat(file, function(err, stat) {
        var contentType = 'application/json';
        if(path.extname(file) === '.csv') contentType = 'application/vnd.ms-excel';

        var headers = {
          'Content-Length': stat.size,
          'Content-Type': contentType,
          'x-amz-acl': 'public-read'
        };

        s3Client.putFile(file, name, headers, function(err, resp) {
          if (err) {
            logger.error(err);
          } else {
            logger.info('Uploaded data ' + file + ' to S3.');
            // remove file.
            fs.unlink(file, function(err){
              if(err) logger.error(err);
            });

            ElasticArchive.add(archiveName, function(err, message){
              if(err) logger.error(err);
              if(message) logger.info(message);
              if(callback) callback(null, message);
            });
          }
        });
      });
    })
  });
};

/**
 * Get SAMI Data from S3.
 *
 * @param archiveFilename
 * @param callback
 * @constructor
 */
s3.prototype.SAMIData = function(archiveFilename, callback) {
  var url = config.archive.dataUrl + path.basename(archiveFilename, path.extname(archiveFilename));

  request
    .get(url)
    .end(function (err, response) {
      if (err) {
        return callback(err, null);
      }
      return callback(null, response.body);
    });
};


module.exports = new s3();
