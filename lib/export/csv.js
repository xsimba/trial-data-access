var logger = require('../logger')
  , async = require('async')
  , _ = require('lodash')
  , fs = require('fs')
  , path = require('path')
  , Metadata = require('../services/archive/metadata')
  , Export = require('../services/transform/export')
  , config = require('../../config/config')
  , uuid = require('uuid')
  , s3 = require('../services/aws/s3')
  , dynamodb = require('../services/aws/dynamodb')
  , ElasticArchive = require('../elastic/archive')
  , utility = require('../services/archive/utility')
  , json2csv = require('json2csv')
  , transform = require('../services/transform/transform')
  , zip = require('node-zip')();

var metadataFields = ['key', 'value'];
var archiveFields = ['field', 'timestamp', 'sample'];

var csv = function(){};

/**
 * Create CSV from archive.
 *
 * @param req
 * @param callback
 * @constructor
 */
csv.prototype.all = function(archiveId, callback) {
  var self = this;

  ElasticArchive.get_archive(archiveId, function(err, archive){
    if(err) return callback(err, null);
    self.transform.metadata(archive._source.metadata, function(err, metadataCsv) {
      if(err) return callback(err, null);
      transform.Table.Archive(archive._source.data, function(err, data){
        if(err) return callback(err, null);
        transform.Object.Archive(data, function(err, data){
          if(err) return callback(err, null);
          self.transform.archive(data, function(err, archiveCsv) {
            if(err) return callback(err, null);
            zip.file('archive.csv', archiveCsv);
            zip.file('metadata.csv', metadataCsv);
            var zipped = zip.generate({base64:false,compression:'DEFLATE'});
            return callback(null, zipped);
          });
        });
      });
    });
  });
};

csv.prototype.archive = function(archiveId, callback) {
  var self = this;

  ElasticArchive.get_archive(archiveId, function(err, archive){
    if(err) return callback(err, null);
    transform.Table.Archive(archive._source.data, function(err, data){
      if(err) return callback(err, null);
      transform.Object.Archive(data, function(err, data){
        if(err) return callback(err, null);
        self.array.archive(data, function(err, archiveCsv) {
          if(err) return callback(err, null);
          return callback(null, archiveCsv);
        });
      });
    });
  });
};

csv.prototype.metadata = function(archiveId, callback) {
  var self = this;

  ElasticArchive.get_archive(archiveId, function(err, archive){
    if(err) return callback(err, null);
    self.array.metadata(archive._source.metadata, function(err, metadataCsv) {
      if(err) return callback(err, null);
      return callback(null, metadataCsv);
    });
  });
};

csv.prototype.array = {
  metadata: function(metadata, callback) {
    var fields = metadataFields;
    metadata.unshift(fields);
    return callback(null, metadata);
  },
  archive: function(archive, callback) {
    var fields = archiveFields;
    archive.unshift(fields);
    return callback(null, archive);
  }
};

csv.prototype.transform = {
  metadata: function(metadata, callback) {
    var fields = metadataFields;
    json2csv({data: metadata, fields: fields}, function(err, csv){
      if(err) return callback(err, null);
      return callback(null, csv);
    });
  },
  archive: function(archive, callback) {
    var fields = archiveFields;
    json2csv({data: archive, fields: fields}, function(err, csv){
      if(err) return callback(err, null);
      return callback(null, csv);
    });
  }
};

module.exports = new csv();
