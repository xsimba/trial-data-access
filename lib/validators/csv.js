var logger = require("../logger");

var CSVValidator = function(){};

var validTypes = ['text/csv','application/vnd.ms-excel','application/csv'];

CSVValidator.prototype.Validate = {
  CSV: function(file, callback) {
    if(validTypes.indexOf(file.type) < 0) return callback('Invalid file type.', null);

    if(file.size < 1) return callback('Empty file.', null);

    callback(null, true);
  }
};

module.exports = new CSVValidator();
