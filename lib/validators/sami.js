var logger = require("../logger")
  , validator = require('is-my-json-valid');

var SamiValidator = function(){};

SamiValidator.prototype.Validate = {
  JSON: function(session, callback) {
    var validate = validator({
      required: true,
      type: 'object',
      properties: {
        startDate: {
          required: true,
          maxLength : 13,
          type: 'number'
        },
        endDate: {
          required: true,
          maxLength : 13,
          type: 'number'
        },
        sdids: {
          required: true,
          minLength : 4,
          type: 'string'
        },
        uids: {
          required: true,
          type: 'string'
        },
        trialId: {
          required: false,
          minLength : 32,
          maxLength : 32,
          type: 'string'
        }
      }
    });

    var results = validate(session);

    if(validate.errors) {
      logger.error('Invalid sami JSON: ' + JSON.stringify(validate.errors));
      return callback(validate.errors, results);
    }

    callback(null, results);
  }
};

module.exports = new SamiValidator();
