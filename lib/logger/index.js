var config = require("../../config/config")
  , winston = require("winston")
  , fs = require("fs");

var name = config.logger.name
  , type = config.logger.type

var FileLogger = function() {
  var filename = "logs/" + name + ".log";
  return new (winston.Logger)({
    transports: [
      new (winston.transports.File)({
        filename: filename
      })
    ]
  });
};

var ConsoleLogger = function() {
  return new (winston.Logger)({
    transports: [
      new (winston.transports.Console)({
        colorize: 'all'
      })
    ]
  });
};

var logger;


if (type === "file")
  logger = FileLogger();
else
  logger = ConsoleLogger();

module.exports = logger;
