import ConfigParser, os.path

__author__ = 'john'


def get_config():
    config = ConfigParser.ConfigParser()
    config.read('config.ini')
    return config


def set_bearer(bearer_token):
    cfgfile = open('bearer.ini','w')
    config = ConfigParser.ConfigParser()
    config.add_section('Tokens')
    config.set('Tokens', 'Bearer', bearer_token)
    config.write(cfgfile)
    cfgfile.close()


def get_bearer():
    if os.path.isfile('bearer.ini') is True:
        config = ConfigParser.ConfigParser()
        config.read('bearer.ini')
        return config.get('Tokens', 'bearer')
    else:
        return False


def get_auth_config():
    config = ConfigParser.ConfigParser()
    config.read('auth.ini')
    return config
