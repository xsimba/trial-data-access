import json

import validate
import requests
import get_config

__author__ = 'john'


def filterQuery(query, per_page, page, token):
    config = get_config.get_config()

    headers = {'Authorization': token, 'Content-Type': 'application/json'}

    payload = query.replace(' ', '').replace('\n', '').replace('\r', '')

    if validate.is_json(query):

        if per_page is not None and page is not None:
            payload = {'q':payload, 'per_page': per_page, 'page': page}
        else:
            payload = {'q':payload}

        r = requests.get(config.get('URLS', 'SearchFilter'), params=payload, headers=headers)

        return r.text
    else:
        return 'Invalid JSON'


def stringQuery(query, per_page, page, token):
    config = get_config.get_config()

    headers = {'Authorization': token, 'Content-Type': 'application/json'}

    payload = query.replace(' ', '').replace('\n', '').replace('\r', '')

    if per_page is not None and page is not None:
        payload = {'q':payload, 'per_page': per_page, 'page': page}
    else:
        payload = {'q':payload}

    r = requests.get(config.get('URLS', 'SearchQuery'), params=payload, headers=headers)

    return r.text