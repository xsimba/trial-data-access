import json

import validate
import requests
import get_config
import time

__author__ = 'john'


def import_sami_data(data, token):
    """ Return server response

        Requests session data based on the session export JSON supplied.
    """
    config = get_config.get_config()

    headers = {'Authorization': token, 'Content-Type': 'application/json'}

    payload = data.replace(' ', '').replace('\n', '').replace('\r', '')

    if validate.is_json(payload):
        importData = json.loads(payload)
        if 'sessionId' in importData:
            r = requests.post(config.get('URLS', 'SessionImport'), data=payload, headers=headers)
        else:
            r = requests.post(config.get('URLS', 'SAMIImport'), data=payload, headers=headers)

        resData = json.loads(r.text)

        if resData['url'] and resData['expires']:
            return complete_import(resData['url'], token)
        else:
            return r.text
    else:
        return 'Invalid JSON'

def complete_import(url, token):
    """
    :param url: exported URL
    :param token: session token
    :return: archive details
    """
    urlList = url.split('/')
    session_id = urlList[-1]
    config = get_config.get_config()

    headers = {'Authorization': token, 'Content-Type': 'application/json'}

    print 'Waiting for data...'
    time.sleep(20)

    payload = '{"data":[],"metadata":[]}'

    r = requests.post(config.get('URLS', 'CompleteImport')%(session_id), data=payload, headers=headers)

    return r.text

def export_properties(session_id, token):
    """ Return server response

        Requests the session export JSON used to get all trial session data based on the given session ID.
    """
    config = get_config.get_config()
    url = config.get('URLS', 'SessionAPIExportProperties')%(session_id)
    print url

    headers = {'Authorization': token, 'Content-Type': 'application/json'}

    r = requests.get(url, headers=headers)
    data = json.loads(r.text)
    result = json.dumps(data['data'])
    return result


def is_logged_in(token):
    """ Return bool to show if user has a valid token

        Call SAMI API user/self to determine if token is valid.
    """
    config = get_config.get_config()
    url = config.get('URLS', 'SAMIAPIUser')

    headers = {'Authorization': token, 'Content-Type': 'application/json'}

    r = requests.get(url, headers=headers)

    if r.status_code == requests.codes.unauthorized:
        return False

    if validate.is_json(r.text):
        return True

    return False