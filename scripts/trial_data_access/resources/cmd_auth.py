import getpass
import sys

import auth
import get_config
import sami

__author__ = 'john'


def login():
    cached_bearer_token = get_config.get_bearer()
    if cached_bearer_token is not False and sami.is_logged_in(cached_bearer_token):
        return cached_bearer_token

    config = get_config.get_auth_config()
    username = config.get('Auth', 'Username')
    password = config.get('Auth', 'Password')

    if username == '' or password == '':
        username = raw_input('Your Samsung username: ')
        password = getpass.getpass('Your Samsung password: ')

    try:
        bearer_token = auth.login(username, password)
        get_config.set_bearer(bearer_token)
        return bearer_token
    except ValueError as err:
        print(err.args[0])
        sys.exit()

