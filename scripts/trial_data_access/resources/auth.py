import re

import mechanize
import get_config

__author__ = 'john'


def login(username, password):
    """ Return bearer token

        Login to trial data access web app to obtain bearer token.
    """
    print 'Authenticating...'

    config = get_config.get_config()

    login = config.get('URLS', 'TrialDataAccessLogin')
    br = mechanize.Browser()
    br.set_handle_robots(False)
    br.open(login)

    br.follow_link(text_regex=r'login', nr=0)

    br.select_form(name='frm')
    br.form.set_all_readonly(False)
    br.submit()

    br.select_form(name= 'signInForm')
    br.form.set_value(username, nr=1)
    br.form.set_value(password, nr=2)
    br.form.action = config.get('URLS', 'SamsungAccountLoginFormAction')
    br.submit()

    br.select_form(name= 'frm')

    if br.form.action == 'https://account.samsung.com/account/signIn.do':
        raise ValueError('Samsung login failed - please check your username and password.')

    r = br.submit()
    result = r.read()
    match = re.search(r'\"access_token\"\:\"(\w+)\"', result)
    token = match.group(1)
    bearer_token = 'Bearer %s'%(token)
    print bearer_token

    return bearer_token
