import unittest, validate, get_config


class TestResourceModules(unittest.TestCase):

    def test_validate_is_json_true(self):
        self.assertTrue(validate.is_json('{"test":"john"}'))

    def test_validate_is_json_false(self):
        self.assertFalse(validate.is_json('test'))

    def test_set_bearer(self):
        bearer = '123'
        get_config.set_bearer(bearer)
        self.assertEqual(bearer, get_config.get_bearer())

if __name__ == '__main__':
    unittest.main()
