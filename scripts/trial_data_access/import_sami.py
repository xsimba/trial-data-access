#!/usr/bin/env python
import sys
import argparse

from resources import sami
from resources import cmd_auth


def main(argv):
    filename = 'import_sami.py'
    username = ''
    password = ''
    content = ''

    parser = argparse.ArgumentParser(description='Import Trial Sessions', epilog='Please note only one option can be used at a time.')

    parser.add_argument('-f', '--file', type=str, nargs='?', help='file containing SAMI or session JSON')
    parser.add_argument('-s', '--string', type=str, nargs='?', help='string containing SAMI or session JSON')
    parser.add_argument('-i', '--sessionid', type=str, nargs='?', help='session ID used to fetch session JSON')

    print '\n___Import SAMI data from trials and trial sessions___\n'


    args = parser.parse_args()

    if args.file is not None:
        inputfile = args.file
        print 'Input file is', inputfile
        print '\n'
        with open(inputfile, 'r') as content_file:
            content = content_file.read()
    elif args.string is not None:
        inputstring = args.string
        print 'Input string is', inputstring
        print '\n'
        content = inputstring
    elif args.sessionid is not None:
        sessionId = args.sessionid
        print 'Session ID is', sessionId
        print '\n'
        token = cmd_auth.login()
        content = sami.export_properties(sessionId, token)

    if username == '' or password == '':
        token = cmd_auth.login()

    print 'Result: \n'
    print sami.import_sami_data(content, token)

if __name__ == "__main__":
    main(sys.argv[1:])