#!/usr/bin/env python
import sys
import argparse

from resources import search
from resources import cmd_auth


def main(argv):
    filename = 'search.py'
    username = ''
    password = ''

    parser = argparse.ArgumentParser(description='Import Trial Sessions', epilog='Please note only one option can be used at a time.')

    parser.add_argument('-f', '--filters', type=str, nargs='?', help='string containing search filters JSON')
    parser.add_argument('-q', '--query', type=str, nargs='?', help='string representing search query')
    parser.add_argument('-pp', '--per_page', type=int, nargs='?', help='number of pages to return in results')
    parser.add_argument('-p', '--page', type=int, nargs='?', help='results page to return')

    print '\n___Search archives___\n'

    args = parser.parse_args()

    filters = False

    if args.filters is not None:
        filters = True
        query = args.filters
    elif args.query is not None:
        query = args.query
    else:
        sys.exit('No valid query arguments provided.')


    if args.per_page and args.page:
        per_page = args.per_page
        page = args.page
    else:
        per_page = None
        page = None


    if username == '' or password == '':
        token = cmd_auth.login()

    print 'Result: \n'
    if filters:
        print search.filterQuery(query, per_page, page, token)
    else:
        print search.stringQuery(query, per_page, page, token)

if __name__ == "__main__":
    main(sys.argv[1:])