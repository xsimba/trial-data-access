from setuptools import setup, find_packages

setup(
    name="trial_data_access",
    version="0.1",
    packages=find_packages()
)
