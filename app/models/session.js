var dynamoose = require('dynamoose'),
  Schema = dynamoose.Schema;

var SessionSchema = new Schema({
  archive: {
    type: String,
    validate: /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}[.a-z]*/i
  },
  sessionId: String,
  participantId: String,
  devices: String,
  exports: String,
  metadata: String,
  start: {
    type: Number,
    validate: function(value) {
      var v = ''+value;
      return (v.length <= 13 && v.length > 0);
    }
  },
  end: {
    type: Number,
    validate: function(value) {
      var v = ''+value;
      return (v.length <= 13 && v.length > 0);
    }
  },
  trialId: String,
  status: String
});

dynamoose.model('SessionData', SessionSchema);
