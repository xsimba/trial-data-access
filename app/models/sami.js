var dynamoose = require('dynamoose'),
  Schema = dynamoose.Schema;

var SAMISchema = new Schema({
  archive: {
    type: String,
    validate: /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}[.a-z]*/i
  },
  data: Array
});

dynamoose.model('SAMIData', SAMISchema);
