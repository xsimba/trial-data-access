var dynamoose = require('dynamoose'),
  Schema = dynamoose.Schema;

var MetadataSchema = new Schema({
  id: {
    type: String,
    validate: /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i
  },
  archive: {
    type: String,
    validate: /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}[.a-z]*/i
  },
  key: String,
  value: String,
  order: Number,
  hidden: {
    type: Boolean,
    default: false
  }
});

dynamoose.model('MetadataData', MetadataSchema);
