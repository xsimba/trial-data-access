var dynamoose = require('dynamoose'),
  Schema = dynamoose.Schema;

var ArchiveSchema = new Schema({
  filename: {
    type: String,
    validate: /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}[.a-z]*/i
  },
  created: { type: Date, default: Date.now()},
  expires: Date,
  user: String,
  status: {
    type: String,
    default: 'Importing',
    validate: function(value) {
      var availableStatus = ['Importing', 'New', 'Uploading', 'Uploaded', 'Removing', 'Removed', 'test', 'Test'];
      return availableStatus.indexOf(value) > -1;
    }
  },
  revision: { type: Number, default: 1 },
  parent: String,
  duplicatedVersion: Number
});

dynamoose.model('ArchiveData', ArchiveSchema);
