var dynamoose = require('dynamoose'),
  Schema = dynamoose.Schema;

var FilteredSchema = new Schema({
  archive: {
    id: {
      type: String,
      validate: /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i
    },
    type: String,
    validate: /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}[.a-z]*/i
  },
  data: String // JSON string of {'field',000,111} field name, sample, timestamp
});

dynamoose.model('FilteredData', FilteredSchema);
