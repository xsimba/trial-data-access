'use strict';

var Layout = require('./layout.jsx');
var React = require('react');

module.exports = React.createClass({

  render: function render() {

    return (
      <Layout {...this.props}>
        <h3>Error: {this.props.message}</h3>
        <h6>{this.props.error}</h6>
      </Layout>
    );
  }
});
