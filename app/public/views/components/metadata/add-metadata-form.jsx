'use strict';

var React = require('react');
var Progress = require('../progress-bar.jsx');

module.exports = React.createClass({
  getInitialState: function () {
    if(typeof(window) == 'undefined'){
      global.window = new Object();
    }

    return {
      submitted: false,
      key: '',
      value: ''
    };
  },
  handleKeyChange: function(event) {
    this.setState({key: event.target.value});
  },
  handleValueChange: function(event) {
    this.setState({value: event.target.value});
  },
  addMetadata: function() {
    this.setState({
      submitted: true
    });

    this.props.addMetadata(this.state.key, this.state.value);

    this.setState({
      key: '',
      value: '',
      submitted: false
    });
  },
  render: function render() {
    return (
      <div className='metadata-form'>
        {this.state.submitted?<Progress />:''}
        <p><label>Key: <input disabled={this.state.submitted} value={this.state.key} onChange={this.handleKeyChange} /></label></p>
        <p><label>Value: <input disabled={this.state.submitted} value={this.state.value} onChange={this.handleValueChange} /></label></p>
        {this.state.submitted?'':<button className='submit' onClick={this.addMetadata}>add metadata</button>}
      </div>
    );
  }
});
