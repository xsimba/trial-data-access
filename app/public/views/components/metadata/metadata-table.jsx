'use strict';

var React = require('react');

module.exports = React.createClass({
  getInitialState: function () {
    if(typeof(window) == 'undefined'){
      global.window = new Object();
    }

    return {};
  },
  render: function render() {
    return (
      <div className="metadata-tags">
      {
        this.props.metadata.map(function(metadata, index){
          return <div key={'metadata_'+index}>{metadata.key} : {metadata.value}</div>
        })
      }
      </div>
    );
  }
});
