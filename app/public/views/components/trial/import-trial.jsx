'use strict';

var React = require('react');
var Qajax = require('qajax');
var moment = require('moment');
var _ = require('lodash');
var Progress = require('../../components/progress-bar.jsx');
var Select = require('react-select');
var DateTimePicker = require('react-widgets/lib/DateTimePicker');

module.exports = React.createClass({
  getInitialState: function () {
    if(typeof(window) == 'undefined'){
      global.window = new Object();
    }
    return {
      devices: [],
      participants: [],
      trials: [],
      trial: null,
      selectedParticipants: [],
      selectedDevices: [],
      startDate: moment().add(-1, 'months').toDate(),
      endDate: moment().toDate()
    };
  },
  getTrials: function() {
    var self = this;
    Qajax({ url: "/trial/list", headers: { "Content-Type" : "application/json"}, method: "GET" })
      .then(Qajax.filterSuccess)
      .get("responseText")
      .then(function (txt) {
        try {
          self.setState({trials: JSON.parse(txt)});
        } catch(e) {
          console.log(e);
        }
      }, function (err) {
        console.log("xhr failure: ", err);
        if(err && err.status && err.status === 401) {
          self.setState({loggedout: true});
        }
      });
  },
  getParticipants: function(trialId) {
    var self = this;
    if(!trialId) trialId = self.state.trial;
    Qajax({ url: "/trial/" + trialId + "/participants", headers: { "Content-Type" : "application/json"}, method: "GET" })
      .then(Qajax.filterSuccess)
      .get("responseText")
      .then(function (txt) {
        try {
          self.setState({participants: JSON.parse(txt)});
        } catch(e) {
          console.log(e);
        }
      }, function (err) {
        console.log("xhr failure: ", err);
        if(err && err.status && err.status === 401) {
          self.setState({loggedout: true});
        }
      });
  },
  getParticipantDevices: function(trialId, participantId) {
    var self = this;
    if(!trialId) trialId = self.state.trial;
    Qajax({ url: "/trial/" + trialId + "/participants/" + participantId + '/devices', headers: { "Content-Type" : "application/json"}, method: "GET" })
      .then(Qajax.filterSuccess)
      .get("responseText")
      .then(function (txt) {
        try {
          var devices = JSON.parse(txt);
          var participants = self.state.participants;
          participants = participants.map(function(participant){
            if(participant.id === participantId) {
              participant.devices = devices;
            }
            return participant
          });
          self.setState({participants: participants});
        } catch(e) {
          console.log(e);
        }
      }, function (err) {
        console.log("xhr failure: ", err);
        if(err && err.status && err.status === 401) {
          self.setState({loggedout: true});
        }
      });
  },
  componentDidMount: function() {
    this.getTrials();
  },
  handleTrialChange: function(trialId) {
    this.setState({trial: trialId});
    //this.setTrialDates(trialId);
    this.getParticipants(trialId);
  },
  setTrialDates: function(trialId) {
    var trials = this.state.trials;
    var trial = _.find(trials, function(t){
      return t.id === trialId;
    });
    if(typeof trial !== 'undefined') {
      var startDate = moment().add(-1, 'months');
      var endDate = moment();
      this.setState({
        startDate: startDate,
        endDate: endDate
      });
    }
  },
  selectParticipant: function(event) {
    var isNew = false;
    var participants = this.state.selectedParticipants;
    var participant = event.target.value;
    if(participants.indexOf(participant) < 0) {
      participants.push(participant);
      isNew = true;
    } else {
      var index = participants.indexOf(participant);
      participants.splice(index, 1);
    }
    this.setState({selectedParticipants: participants});
    if(isNew) this.getParticipantDevices(this.state.trial, participant);
  },
  selectDevice: function(event) {
    var devices = this.state.selectedDevices;
    var device = event.target.value;
    if(devices.indexOf(device) < 0) {
      devices.push(device);
    } else {
      var index = devices.indexOf(device);
      devices.splice(index, 1);
    }
    this.setState({selectedDevices: devices});
  },
  handleStartDateChange: function(date) {
    this.setState({startDate: date});
  },
  handleEndDateChange: function(date) {
    this.setState({endDate: date});
  },
  importTrial: function(event) {
    event.preventDefault();
    this.props.importTrial();
  },
  render: function render() {
    var self = this;
    var canImport = (this.state.trial !== null && this.state.selectedParticipants.length > 0 && this.state.selectedDevices.length > 0);
    var trials = this.state.trials.map(function(trial){
      return {
        value: trial.id,
        label: trial.name
      }
    });
    return (
      <div className="import-trial">

        {this.props.submitted ? '' :
        <div>

          <p>Choose trial:</p>
          <Select
            name="select-trial"
            value={this.state.trial}
            options={trials}
            onChange={this.handleTrialChange}
          />

          {this.state.trial === null ? '' :
          <div className="trial-dates">
            <div>
              <p>Start date:</p>
              <DateTimePicker defaultValue={new Date()} value={this.state.startDate} onChange={this.handleStartDateChange} />
            </div>
            <div>
              <p>End date:</p>
              <DateTimePicker defaultValue={new Date()} value={this.state.endDate} onChange={this.handleEndDateChange} />
            </div>
          </div>
          }

          {this.state.participants.length > 0 ? <p>Choose participant(s) and device(s):</p> :'' }

          <ul className="trial-participant-list">
          {this.state.participants.map(function(participant){
            return <li>
              <input onChange={self.selectParticipant} type='checkbox' value={participant.id} /> {participant.fullName}
              {participant.hasOwnProperty('devices') ?
                <ul>
                {participant.devices.map(function(device){
                  return <li><input onChange={self.selectDevice} type='checkbox' value={device.id} /> {device.name}</li>
                })}
                </ul>
              :''}
            </li>
          })}
          </ul>

          {this.state.devices.map(function(device){
            return <div><input onChange={self.selectDevice} type='checkbox' value={device.id} /> {device.name}</div>
          })}

        </div>}

      {canImport ?
        <div>
          {this.props.submitted?<Progress />:''}
          {this.props.submitted?'':<button className='submit' onClick={this.importTrial}>import</button>}
        </div>
        : ''}

      </div>
    );
  }
});
