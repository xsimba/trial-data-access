'use strict';

var React = require('react');

module.exports = React.createClass({

  render: function render() {
    return (
      <li>{this.props.loggedIn ? <a href={this.props.logout}>logout</a> : <a href="/login">login</a>}</li>
    );
  }
});
