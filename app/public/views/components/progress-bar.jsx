'use strict';

var React = require('react');

module.exports = React.createClass({
  render: function() {
    return (
      <div>
        <img className='progress' src='/images/progress.gif' alt='loading' />
      </div>
    );
  }
});
