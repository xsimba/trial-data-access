'use strict';

var React = require('react');

module.exports = React.createClass({

  render: function render() {
    return (
      <ul>
        {this.props.errors.map(function(error, index){
          return <li key={index}><p>{error}</p></li>;
        })}
      </ul>
    );
  }
});
