'use strict';

var React = require('react');
var ErrorList = require('./error-list.jsx');
var ErrorMessage = require('./error-message.jsx');
var ImportedMessage = require('./imported-message.jsx');
var ImportMessage = require('./import-message.jsx');
var LoggedoutMessage = require('./loggedout-message.jsx');
var ErrorMessage = require('./error-message.jsx');
var SuccessMessage = require('./success-message.jsx');
var classNames = require('classnames');

module.exports = React.createClass({
  getInitialState: function () {
    return {
      hidden: true
    }
  },
  componentWillReceiveProps: function(newProps) {
    this.setState({hidden: false});
    window.scrollTo(0,0);
  },
  close: function() {
    this.setState({hidden: true});
    this.props.close();
  },
  render: function render() {

    if(this.props.type === 'loggedout') {
      var message = this.props.message?<LoggedoutMessage />:'';
    } else if(this.props.type === 'error') {
      var message = Array.isArray(this.props.message) ? <ErrorList errors={this.props.message} /> : <ErrorMessage error={this.props.message} />;
    } else if(this.props.type === 'success') {
      var message = <SuccessMessage success={this.props.message} />;
    } else if(this.props.type === 'imported') {
      var message = <ImportedMessage success={this.props.message} />;
    } else {
      var message = <ImportMessage success={this.props.message} />
    }

    var messageClasses = this.props.message?classNames('message', 'message-'+this.props.type, {'message-hide' : this.state.hidden}):'';

    return (
      <div className={messageClasses}>
        <span className='message-close' onClick={this.close} />
        {message}
      </div>
    );
  }
});
