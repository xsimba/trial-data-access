'use strict';

var React = require('react');
var Moment = require('moment');

module.exports = React.createClass({

  render: function render() {
    return (
      <div>
        <p>You have been logged out. <a href="/login">Please login</a>.</p>
      </div>
    );
  }
});
