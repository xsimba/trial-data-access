'use strict';

var React = require('react');
var Moment = require('moment');

module.exports = React.createClass({

  render: function render() {
    var expiryTime = Moment(this.props.success.expires);
    var expiry = expiryTime.format('MMMM Do YYYY, h:mm:ss a');
    var expires = expiryTime.fromNow();
    var now = Moment();
    var expiredTerm = now.diff(expiryTime) < 0 ? 'expires' : 'expired';
    return (
      <div>
        {this.props.success ? <p>Your <a href={this.props.success.url} target='_blank'>archive</a> is ready and {expiredTerm} {expires} ({expiry}).</p> : ''}
      </div>
    );
  }
});
