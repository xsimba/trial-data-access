'use strict';

var React = require('react');

module.exports = React.createClass({

  render: function render() {
    return (
      <p>{this.props.error}</p>
    );
  }
});
