'use strict';

var React = require('react');
var Moment = require('moment');

module.exports = React.createClass({
  getInitialState: function () {
    if(typeof(window) == 'undefined'){
      global.window = new Object();
    }

    return {
      countdown: 10,
      url: null,
      timeunit: 'seconds'
    };
  },
  updateCountdown: function(countdown) {
    var timeunit = countdown === 1 ? 'second' : 'seconds';
    this.setState({
      countdown:countdown,
      timeunit: timeunit
    });
  },
  componentWillReceiveProps: function(newProps) {
    var expiryTime = Moment(newProps.success.expires);
    var expires = expiryTime.fromNow();
    var now = Moment();

    var successUrl = newProps.success.url || '';
    var archiveId = successUrl.split('/');
    archiveId = archiveId[archiveId.length -1];

    if(archiveId) {
      var url = '/archive/edit/id/' + archiveId;
      var seconds = 10;
      var second = 0;
      var countdown = seconds - second;

      var self = this;
      self.updateCountdown(countdown);

      var interval = setInterval(function() {
        countdown = seconds - second;

        self.updateCountdown(countdown);

        if (second >= seconds) {
          window.location = url;
          clearInterval(interval);
        }

        second++;
      }, 1000);
    }
  },
  render: function render() {
    return (
      <div>
        {this.props.success ?
          <div>
            <p>Please <a href={this.state.url}>complete your import.</a></p>
            <p>You will be redirected in {this.state.countdown} {this.state.timeunit}...</p>
          </div>
          : ''}
      </div>
    );
  }
});
