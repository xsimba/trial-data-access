'use strict';

var React = require('react');

module.exports = React.createClass({
  getInitialState: function () {
    if(typeof(window) == 'undefined'){
      global.window = new Object();
    }

    return {};
  },
  render: function render() {
    var self = this;
    return (
      <table className="metadata-table">
        <thead>
          <tr>
            <th>Key</th>
            <th>Value</th>
            <th>Import</th>
          </tr>
        </thead>
      {
        this.props.metadata.map(function(metadata, index){
          return <tr key={'metadata_'+index}>
            <td>{metadata.key}</td>
            <td>{metadata.value}</td>
            <td><input type="checkbox" value={index} defaultChecked={metadata.import} onChange={self.props.selectMetadata} /></td>
          </tr>
        })
      }
      </table>
    );
  }
});
