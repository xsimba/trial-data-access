'use strict';

var React = require('react');
var _ = require('lodash');
var Moment = require('moment');

module.exports = React.createClass({
  getInitialState: function () {
    if(typeof(window) == 'undefined'){
      global.window = new Object();
    }

    return {};
  },
  render: function render() {
    var self = this;
    return (
      <div>
        <p>Select or unselect all data samples with the following field name(s).</p>
        <div className="archive-charts">
          {
            this.props.data.map(function(d, index){
              return <div className="chart-select" key={'table'+index}>
                {d.key} <input type="checkbox" value={d.key} defaultChecked={d.import} onChange={self.props.selectField} />
              </div>
            })
          }
        </div>
      </div>
    );
  }
});
