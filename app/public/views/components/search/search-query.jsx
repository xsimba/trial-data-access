'use strict';

var React = require('react');

module.exports = React.createClass({
  getInitialState: function () {
    return {
      value: ''
    };
  },
  handleValueChange: function(event) {
    this.setState({value: event.target.value});
  },
  render: function() {
    return (
      <div>
        <input className='search-query' disabled={this.props.submitted} value={this.state.value} onChange={this.handleValueChange} />
      </div>
    );
  }
});
