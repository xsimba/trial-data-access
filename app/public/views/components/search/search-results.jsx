'use strict';

var React = require('react');
var Router = require('react-router');
var Link = Router.Link;
var Moment = require('moment');

module.exports = React.createClass({
  render: function() {
    return (
      <div>
        {this.props.results !== null && this.props.results.length > 0 ?
          <div className='search-results'>
            {this.props.heading ? <h2>{this.props.heading}</h2> : ''}
            <ul>
              {
                this.props.results.map(function(result){
                  var archive = result._source;
                  var filename = archive.filename.split('.');
                  var archiveId = filename[0];
                  return <li><Link to="edit-archive" params={{archiveId: archiveId}}>{archive.filename} ({Moment(archive.created).format('MMMM Do YYYY, h:mm:ss a')})</Link></li>
                })
              }
            </ul>
          </div>
          :''}

        {this.props.results === null ? <p>No archives found.</p> : ''}
      </div>
    );
  }
});
