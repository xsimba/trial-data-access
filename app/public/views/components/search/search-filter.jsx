'use strict';

var React = require('react');
var Autosuggest = require('react-autosuggest');

module.exports = React.createClass({
  getInitialState: function () {
    return {
      bool: 'must',
      field: '_all',
      value: '',
      queryTypes : ['term', 'range'],
      queryType : 'term',
      from: '',
      to: ''
    };
  },
  handleBoolChange: function(event) {
    this.setState({bool: event.target.value});
  },
  handleFieldChange: function(event) {
    this.setState({
      field: event.target.value,
      queryType: 'term'
    });
  },
  handleValueChange: function(event) {
    this.setState({value: event.target.value});
  },
  handleValueSelected: function(suggest, event){
    event.preventDefault();
    this.setState({value: suggest});
  },
  handQueryTypeChange: function(event){
    this.setState({queryType: event.target.value});
  },
  handleFromChange: function(event){
    this.setState({from: event.target.value});
  },
  handleToChange: function(event){
    this.setState({to: event.target.value});
  },
  render: function() {
    return (
      <div>
        <select disabled={this.props.submitted} onChange={this.handleBoolChange}>
          {this.props.bool.map(function(b, index){
            return <option key={'bool_'+index} value={b}>{b}</option>
          })}
        </select>
        <select disabled={this.props.submitted} onChange={this.handleFieldChange}>
          {this.props.field.map(function(f, index){
            return <option key={'field_'+index} value={f}>{f}</option>
          })}
        </select>
        {this.state.field === 'archive.metadata.value' ?
        <select disabled={this.props.submitted} onChange={this.handQueryTypeChange}>
        {this.state.queryTypes.map(function(q) {
          return <option value={q}>{q}</option>
        })}
        </select> : ''}
        {this.state.queryType === 'range'
          ? <div className="range">
              <input value={this.state.from} onChange={this.handleFromChange} /> to <input value={this.state.to} onChange={this.handleToChange} />
            </div>
          : ''
        }

        {this.state.field === 'archive.metadata.key' && this.state.queryType === 'term'
          ? <Autosuggest suggestions={this.props.getMetadaKeySuggestions} onSuggestionSelected={this.handleValueSelected}/>
          : ''
        }
        {this.state.field !== 'archive.metadata.key' && this.state.queryType === 'term'
          ? <input disabled={this.props.submitted} value={this.state.value} onChange={this.handleValueChange} />
          : ''
        }
        {this.props.totalFilters > 1?<button onClick={this.props.removeSearchFilter.bind(null,this.props.searchFilter)}>Remove</button>:''}
        {this.props.isLastFilter ? <button onClick={this.props.addSearchFilter.bind(null)}>add filter</button> : ''}
      </div>
    );
  }
});
