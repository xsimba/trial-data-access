'use strict';

var React = require('react');
var ArchiveChart = require('./archive-chart.jsx');
var _ = require('lodash');
var Moment = require('moment');

module.exports = React.createClass({
  getInitialState: function () {
    if(typeof(window) == 'undefined'){
      global.window = new Object();
    }

    return {
      charts: []
    };
  },
  isSelected: function(key) {
    return this.state.charts.indexOf(key) > -1;
  },
  onSelect: function(event) {
    var key = event.target.value;
    var charts = this.state.charts;
    if(this.isSelected(key)) {
      var index = charts.indexOf(key);
      if (index > -1) charts.splice(index, 1);
    } else {
      charts.push(key);
    }
    this.setState({charts: charts});
  },
  getArchiveData: function(chart){
    var keys = this.props.keys[0].hasOwnProperty('key') ? _.pluck(this.props.keys, 'key') : this.props.keys;
    var data = this.props.data[keys.indexOf(chart)];
    data = data[chart].s.map(function(d, index){
      return {x: data[chart].t[index], y: d};
    });

    var lineData = [
      {
        name: chart,
        values: data,
        strokeWidth: 3,
        strokeDashArray: "5,5"
      }
    ];

    return lineData;
  },
  getXAxisLabel: function(chart){
    var keys = this.props.keys[0].hasOwnProperty('key') ? _.pluck(this.props.keys, 'key') : this.props.keys;
    var data = this.props.data[keys.indexOf(chart)];
    var timestamps = data[chart].t;
    var start = Moment(timestamps[0] * 1000).format('MMMM Do YYYY, h:mm:ss a');
    var end = Moment(timestamps[timestamps.length-1] * 1000).format('MMMM Do YYYY, h:mm:ss a');
    return start + ' - ' + end;
  },
  render: function render() {
    var self = this;

    return (
      <div className="archive-charts">
        {this.props.keys.length < 1?<p>Charts are only available for Simband data.</p>:''}
        {
          this.props.keys.map(function(key, index){
            var k = key.hasOwnProperty('key') ? key.key : key;
            return <div className="chart-select" key={'k_'+index}>{k} <input type="checkbox" value={k} selected={self.isSelected(k)} onChange={self.onSelect} /></div>
          })
        }
        {
          this.state.charts.map(function(chart, index){
            return <ArchiveChart key={'chart_'+index} data={self.getArchiveData(chart)} xAxisLabel={self.getXAxisLabel(chart)} name={chart} />
          })
        }
      </div>

    );
  }
});
