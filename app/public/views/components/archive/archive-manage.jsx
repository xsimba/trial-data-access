'use strict';

var React = require('react');
var Moment = require('moment');
var AddMetadataForm = require('../../components/metadata/add-metadata-form.jsx');

module.exports = React.createClass({
  getInitialState: function () {
    if(typeof(window) == 'undefined'){
      global.window = new Object();
    }

    return {};
  },
  render: function render() {
    var expiryTime = Moment(this.props.archive.expires);
    var expires = expiryTime.fromNow();
    var now = Moment();
    var expiredTerm = now.diff(expiryTime) < 0 ? 'Expires' : 'Expired';
    var status = this.props.archive.status;

    var action;
    if(status === 'Removed' && this.props.archive.canEdit) {
      action = <span> (<a onClick={this.props.createArchive}>Re-create&#63;</a>)</span>;
    } else if(status === 'Removed' && !this.props.archive.canEdit) {
      action = '';
    } else if (status === 'Importing') {
      action = <span></span>;
    } else {
      action = <span> (<a href={this.props.archive.url} target="_blank">View</a>)</span>;
    }

    var archiveId = this.props.archiveId;
    var archiveExportURL = '/export/csv/archive/' + archiveId;
    var metadataExportURL = '/export/csv/metadata/' + archiveId;
    var exportURL = '/export/csv/' + archiveId;

    return (
      <div className="archive-manage">
        <h2>Public link</h2>
        <ul>
          <li><strong>Status:</strong> {this.props.archive.status} {action}</li>
          {this.props.archive.status === 'Importing' ? '' : <li>{expiredTerm} {expires}.</li>}
        </ul>

        {this.props.archive.status === 'Importing' ? '' :
        <div>
          <h2>Archive</h2>
          <ul>
            <li>
              {this.props.duplicating ? <p>Duplicating...</p> : <a onClick={this.props.duplicate}>Create duplicate archive</a>}
            </li>
          </ul>
        </div>
        }

        <h2>Export</h2>
        <ul>
          <li><a href={archiveExportURL}>Archive CSV</a></li>
          <li><a href={metadataExportURL}>Metadata CSV</a></li>
          <li><a href={exportURL}>Archive and Metadata CSVs</a></li>
        </ul>

        {this.props.archive && this.props.archive.canEdit ?
          <div>
            <h2>Metadata</h2>
            <AddMetadataForm addMetadata={this.props.addMetadata} />
          </div>
          : ''}
      </div>
    );
  }
});
