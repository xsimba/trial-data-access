'use strict';

var React = require('react');
var Moment = require('moment');
var Router = require('react-router');
var Link = Router.Link;

module.exports = React.createClass({

  render: function render() {
    return (
      <ul className='section-nav-list'>
        {this.props.archives.sort(function(a,b){
          return new Date(b.expires) - new Date(a.expires);
        }).map(function(archive, index){
          var name = archive.filename.replace(/\.[^/.]+$/, '');
          var created = Moment(archive.created).format('MMMM Do YYYY, h:mm:ss a');
          var archiveLink = '/archive/id/' + name;
          var expires = Moment(archive.expires).fromNow();
          var status = archive.status;
          var expired = (archive.status === 'Removed');

          if(status === 'Importing') {
            expired = true;
          } else {
            var now = Moment();
            if(now.diff(Moment(archive.expires)) < 0) {
              status += ' - expires ' + expires;
            } else {
              status += ' - expired ' + expires;
            }
          }

          return <li className='section-nav-item' key={index}>
            <div>
              <div><Link to="edit-archive" params={{archiveId: name}}>{name}</Link></div>
              <div>{created}</div>
              {expired ?
                <small>({status}.)</small>
                :
                <small>(<a className="archive-link" href={archiveLink} target='_blank'>{status}.</a>)</small>
              }
            </div>
          </li>;
        })}
      </ul>
    );
  }
});
