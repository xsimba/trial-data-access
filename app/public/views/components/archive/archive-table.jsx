'use strict';

var React = require('react');
var _ = require('lodash');
var Moment = require('moment');
var ReactPaginate = require('react-paginate');

module.exports = React.createClass({
  getInitialState: function () {
    if(typeof(window) == 'undefined'){
      global.window = new Object();
    }

    return {
      initialData: this.props.data,
      data: this.props.data.slice(0,this.props.perPage),
      page: 0,
      pages: (this.props.data.length/this.props.perPage),
      perPage: this.props.perPage
    };
  },
  handlePageClick: function(pageNum) {
    var start = (pageNum.selected) * this.state.perPage;
    var end = start + this.state.perPage;
    var data = this.state.initialData.slice(start, end);
    this.setState({
      page: pageNum.selected,
      data: data
    });
  },
  render: function render() {
    return (
      <div>
        <table className="archive-table">
          <thead>
            <tr>
              <th>Field</th>
              <th>Recorded at</th>
              <th>Timestamp</th>
              <th>Sample</th>
            </tr>
          </thead>
          <tbody>
          {
            this.state.data.map(function(d, index){
              return <tr key={'table'+index}>
                <td>{d[0]}</td>
                <td>{Moment(d[2] * 1000).format('MMMM Do YYYY, h:mm:ss:SS a')}</td>
                <td>{d[2]}</td>
                <td>{d[1]}</td>
              </tr>
            })
          }
          </tbody>
        </table>
        <ReactPaginate previousLabel={"«"}
          nextLabel={"»"}
          breakLabel={<li className="break"><a href="">...</a></li>}
          pageNum={this.state.pages}
          clickCallback={this.handlePageClick}
          containerClassName={"pagination"}
          subContainerClassName={"pages pagination"}
          activeClassName={"active"} />
      </div>
    );
  }
});
