'use strict';

var React = require('react');
var rd3 = require('react-d3');
var LineChart = rd3.LineChart;

module.exports = React.createClass({
  getInitialState: function () {
    if(typeof(window) == 'undefined'){
      global.window = new Object();
    }

    return {
      charts: [],
      name: '',
      data: this.props.data,
      xAxisLabel: this.props.xAxisLabel
    };
  },
  componentWillReceiveProps: function(newProps) {
    this.setState({
      data: newProps.data
    });
  },
  render: function render() {
    return (
      <div>
      {this.state.data.length > 0 ?
        <LineChart
          legend={true}
          data={this.state.data}
          width='100%'
          height={400}
          viewBoxObject={{
            x: 0,
            y: 0,
            width: 500,
            height: 400
          }}
          gridHorizontal={true}
          xAxisLabel={this.state.xAxisLabel}
        />
        : ''
      }
      </div>
    );
  }
});
