'use strict';

var React = require('react');
var Moment = require('moment');

module.exports = React.createClass({
  getInitialState: function () {
    if(typeof(window) == 'undefined'){
      global.window = new Object();
    }

    return {};
  },
  render: function render() {
    var expiryTime = Moment(this.props.archive.expires);
    var expiry = expiryTime.format('MMMM Do YYYY, h:mm:ss a');
    var expires = expiryTime.fromNow();
    var now = Moment();
    var expiredTerm = now.diff(expiryTime) < 0 ? 'expires' : 'expired';

    var created = Moment(this.props.archive.created).format('MMMM Do YYYY, h:mm:ss a');

    var parent = null;
    if(this.props.archive.hasOwnProperty('parent')) {
      parent = this.props.archive.parent;
    }

    return (
      <div className="archive-details">
        <h2>Archive <small>{this.props.archiveId}</small></h2>
        <p><strong>Created:</strong> {created}.</p>
        {parent? <p>Duplicated from archive {parent}.</p> : ''}
      </div>
    );
  }
});
