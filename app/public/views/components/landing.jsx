'use strict';

var React = require('react');
var Router = require('react-router');
var Link = Router.Link;

module.exports = React.createClass({
  render: function() {
    var sessionImage = '/images/landing/sami.png'
    , sessionH2 = 'Import SAMI data'
    , sessionP = 'Import data from SAMI or a SAMI Trial Session.';

    return (
      <section className="welcome-top welcome-section">
        <ul>
          <li></li>
          <li>
            {this.props.loggedIn ?
              <Link className="landing-block" to="import">
                <div className="section-image">
                  <img src={sessionImage} />
                </div>
                <h2>{sessionH2}</h2>
                <p>{sessionP}</p>
                <span className="learn-more">More</span>
              </Link>
            : <a className="landing-block" href="/import">
                <div className="section-image">
                  <img src="/images/landing/sami.png" />
                </div>
                <h2>{sessionH2}</h2>
                <p>{sessionP}</p>
                <span className="learn-more">More</span>
              </a>}
          </li>
          <li></li>
          </ul>
      </section>
    );
  }
});
