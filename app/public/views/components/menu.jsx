'use strict';

var React = require('react');
var Router = require('react-router');
var Link = Router.Link;
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;
var Login = require('./login.jsx');

module.exports = React.createClass({
  getInitialState: function () {
    if(typeof(window) == 'undefined'){
      global.window = new Object();
    }

    return { menuOpen: false };
  },
  toggleMenu: function() {
    this.setState({menuOpen: !this.state.menuOpen});
  },
  isTablet: function() {
    var tablet = false;
    if(window.outerWidth){
      tablet = (window.outerWidth > 761 && window.outerWidth < 1025);
    } else if(document) {
      tablet = (document.body.clientWidth > 761 && document.body.clientWidth < 1025);
    }
    return tablet;
  },
  closeMenu: function() {
    if(this.isTablet()) {
      this.setState({menuOpen: false});
    }
  },
  openMenu: function() {
    if(this.isTablet()) {
      this.setState({menuOpen: true});
    }
  },
  render: function render() {
    var navContainerClasses = this.state.menuOpen ? 'nav-container open' : 'nav-container';
    return (
      <header className="main-header">
        <nav className="mobile-nav">
          <h1 className="logo">
            {this.props.loggedIn ? <Link to="home">Trial Data Access</Link> : <a href="/">Trial Data Access</a> }
          </h1>
          <a className="toggle-nav" href="#" onClick={this.props.toggleMobileMenu}>
            <span className="fa fa-bars"></span>
            <span className="fa close"></span>
          </a>
        </nav>
        <nav className="main-nav">
          <div className="beta">
            <span>Beta</span>
          </div>
          <h1 className="logo">
            {this.props.loggedIn ? <Link to="home">Trial Data Access</Link> : <a href="/login">Trial Data Access</a> }
          </h1>
          {this.props.noLinks ? '' :
          <div className={navContainerClasses}>
            <div className="navigation-shroud"></div>
            <div className="current-page" onClick={this.toggleMenu}>
              Menu
            </div>
            <div className="nav-list-container" onMouseOver={this.openMenu} onMouseOut={this.closeMenu}>
              <ul className="nav-list">
                <li className="nav-item">
                  {this.props.loggedIn ? <Link to="import">Import</Link> : <a href="/login">Import</a> }
                </li>
                <li className="nav-item">
                  {this.props.loggedIn ? <a href="/search-archive">Search</a> : <a href="/login">Search</a> }
                </li>
                <li className="nav-item">
                  {this.props.loggedIn ? <Link to="list-archive">Archives</Link> : <a href="/login">Archives</a> }
                </li>
              </ul>
              <ul className="offsite-nav-list login">
                <Login loggedIn={this.props.loggedIn} logout={this.props.logout} />
              </ul>
            </div>
          </div>}
        </nav>
      </header>
    );
  }
});
