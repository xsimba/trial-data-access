'use strict';

var React = require('react');

module.exports = React.createClass({
  getInitialState: function () {
    if(typeof(window) == 'undefined'){
      global.window = new Object();
    }
    return {
      samiTip: false,
      sessionTip: false
    };
  },
  toggleSamiTip: function() {
    this.setState({
      sessionTip: false,
      samiTip: !this.state.samiTip
    });
  },
  toggleSessionTip: function() {
    this.setState({
      samiTip: false,
      sessionTip: !this.state.sessionTip
    });
  },
  render: function render() {
    var sessionJson = '{\n "sessionId":"041550a47c66460f99e9f226b0b538fc",\n "exportId":"8bda3ef4cc0643918cabd4d2434e8921",\n "startDate":1440941185000,\n "endDate":1441114012428,\n "sdids":"2e71bd49506a411aa70fec0efb00b17f",\n "uids":"b379322f43e641bd9ad6ee7787043fad",\n "trialId":"b936c867117a4ae9a40bfb482682c1a8"\n }',
        samiJson = '{\n "startDate":1440941185000,\n "endDate":1441114012428,\n "sdids":"2e71bd49506a411aa70fec0efb00b17f",\n "uids":"b379322f43e641bd9ad6ee7787043fad",\n "trialId":"b936c867117a4ae9a40bfb482682c1a8"\n}';

    return (
      <div>
        <p>Create an archive from <a className='tip' onClick={this.toggleSamiTip}> SAMI data</a> or a <a className='tip' onClick={this.toggleSessionTip}>trial session</a>.</p>
        {this.state.samiTip?<div className='tipMsg'>
          Get the JSON from the <a target="_blank" href="https://portal.samsungsami.io/">SAMI data visualization tool</a>.
          Example JSON:
          <pre><code>{samiJson}</code></pre>
        </div>:''}
        {this.state.sessionTip?<div className='tipMsg'>
          Get the session JSON from the <a target="_blank" href="http://trials.simband.io/">Trials Admin app</a>.
          Example JSON:
          <pre><code>{sessionJson}</code></pre>
        </div>:''}
        {this.props.submitted?
          <p>
            Importing {this.props.importingSami?'SAMI data ...':'session ...'}
          </p>:
          <p>Paste JSON:</p>
        }
      </div>
    );
  }
});
