'use strict';

var React = require('react');
var Qajax = require('qajax');
var Message = require('../components/messages/message.jsx');
var ArchiveList = require('../components/archive/archive-list.jsx');
var Progress = require('../components/progress-bar.jsx');
var ImportArchive = require('../components/sami/import-archive.jsx');
var classNames = require('classnames');
var Router = require('react-router');
var Navigation = Router.Navigation;
var io = require('socket.io-client');
var config = require('../../../../config/react-config.js');
var socket = io.connect(config.websocket.url);
var Tabs = require('react-simpletabs');
var ImportTrials = require('../components/trial/import-trial.jsx');
var Dropzone = require('react-dropzone');
require('es6-promise').polyfill();
require('isomorphic-fetch');

module.exports = React.createClass({
  mixins: [Navigation],
  getInitialState: function () {
    if(typeof(window) == 'undefined'){
      global.window = new Object();
    }

    return {
      csv: '',
      session: '',
      error: '',
      loggedout: '',
      submitted: false,
      success: '',
      archives: [],
      sidebarOpen: false,
      importingSami: false,
      defaultTab: 0,
      allowedCSVMimeTypes: "text/csv,application/vnd.ms-excel,application/csv"
    };
  },
  handleResize: function(e) {
    // close sidebar on mobile screen
    var closeSidebar = false;
    if(window.outerWidth){
      closeSidebar = window.outerWidth < 761;
    } else if(document) {
      closeSidebar = document.body.clientWidth < 761;
    }

    if(closeSidebar) this.setState({sidebarOpen: false});
  },
  componentDidMount: function() {
    var self = this;
    socket.on('archives', function (data) {
      self.getUserArchives();
    });
  },
  componentWillUnmount: function() {
    if(window.removeEventListener) window.removeEventListener('resize', this.handleResize);
  },
  componentWillMount: function() {
    this.getUserArchives();
    if(window.addEventListener) window.addEventListener('resize', this.handleResize);
  },
  getUserArchives: function() {
    var self = this;
    Qajax({ url: "/archive/user/list", headers: { "Content-Type" : "application/json"}, method: "GET" })
      .then(Qajax.filterSuccess)
      .get("responseText")
      .then(function (txt) {
        try {
          self.setState({archives: JSON.parse(txt)});
        } catch(e) {
          console.log(e);
        }
      }, function (err) {
        console.log("xhr failure: ", err);
        if(err && err.status && err.status === 401) {
          self.setState({loggedout: true});
        }
      });
  },
  handleChange: function(event) {
    this.setState({session: event.target.value});
  },
  validateSessionJSON: function() {
    var errors = {};
    var session = this.state.session;
    if(session === '') errors['empty'] = 'Please provide session JSON.';
    if(!errors['empty']) {
      try {
        JSON.parse(session);
      } catch (e) {
        console.log(e);
        errors['invalid'] = 'Invalid JSON.';
      }
    }
    if(Object.keys(errors).length === 0) return null;
    return errors;
  },
  importSession: function(event) {
    event.preventDefault();

    var validSessionJSON = this.validateSessionJSON();
    if(validSessionJSON !== null) return this.setState({error: validSessionJSON});

    var data = JSON.parse(this.state.session);

    this.createArchive(data);
  },
  importTrial: function(event) {
    if(event) event.preventDefault();

    var trialId = this.refs.importTrial.state.trial;
    var selectedParticipants = this.refs.importTrial.state.selectedParticipants;
    var selectedDevices = this.refs.importTrial.state.selectedDevices;
    var startDate = this.refs.importTrial.state.startDate;
    var endDate = this.refs.importTrial.state.endDate;

    var data = {
      sdids: selectedDevices.join(','),
      uids: selectedParticipants.join(','),
      trialId: trialId,
      startDate: startDate.getTime(),
      endDate: endDate.getTime()
    };

    this.createArchive(JSON.stringify(data));
  },
  createArchive: function(data) {
    this.closeSuccess();
    this.closeError();

    var self = this;
    this.setState({submitted: true});

    var url = "/session/import";

    if(!data.hasOwnProperty('sessionId')) {
      url = "/sami/import";
      this.setState({importingSami:true});
    } else {
      this.setState({importingSami:false});
    }

    Qajax({ url: url, headers: { "Content-Type" : "application/json"}, method: "POST", data: data })
      .then(Qajax.filterSuccess)
      .get("responseText")
      .then(function (txt) {
        var success = self.formatSuccess(txt);
        self.closeError();
        self.setState({
          session: '',
          submitted: false
        });
        // set cookie
        if(success.hasOwnProperty('url')) {
          var archiveId = success.url.split('/');
          archiveId = archiveId[archiveId.length-1];
          var completeURL = '/archive/edit/id/' + archiveId;
          document.cookie = 'importing=' + archiveId;
          self.transitionTo('edit-archive',{archiveId:archiveId});
        }
      }, function (err) {
        if(err && err.status && err.status === 401) {
          self.setState({loggedout: true});
        }
        self.setState({submitted: false});
        var errorObj = self.formatErrors(err.responseText);
        self.closeSuccess();
        if(errorObj === null) {
          self.setState({error: err.statusText});
        } else {
          self.setState({error: errorObj});
        }
      });
  },
  formatSuccess: function(successJsonString){
    try {
      var success = JSON.parse(successJsonString);
      return success;
    } catch (e) {
      console.log(e);
      return successJsonString;
    }
  },
  formatErrors: function(errorJsonString) {
    var errorObj = JSON.parse(errorJsonString);
    if(!errorObj.hasOwnProperty('error')) return null;
    try {
      var errorObjParsed = JSON.parse(errorObj.error);
    } catch (e) {
      console.log(e);
      return errorObj.error;
    }
    errorObj = errorObjParsed;

    if(Array.isArray(errorObj)) {
      return errorObj.map(function(error){
        var message = error.field.replace('data.','') + ' ' + error.message;
        return message;
      });
    } else {
      return errorObj.error;
    }
  },
  openSidebar: function() {
    this.setState({sidebarOpen: !this.state.sidebarOpen});
  },
  closeSuccess: function() {
    this.setState({success: ''});
  },
  closeError: function() {
    this.setState({error: ''});
  },
  closeLoggedout: function() {
    this.setState({loggedout: ''});
  },
  handleSelected: function (index, last) {
    console.log('Selected tab: ' + index + ', Last tab: ' + last);
  },
  uploadCSV: function(files) {
    var self = this;
    self.setState({submitted:true});

    var file = files[0];
    var ext = file.name.split('.');
    ext = ext[ext.length-1];

    console.log(ext);

    if(files.length < 1 || ext !== 'csv') {
      self.setState({
        submitted: false,
        error: 'File must be a CSV.'
      });
      return;
    }

    var data = new FormData();
    data.append('file',files[0]);
    fetch('/csv/import', {
      credentials: 'same-origin',
      method: 'post',
      headers: {
        'Accept': 'application/json'
      },
      body: data
    }).then(function(response){
      self.setState({
        'csv': '',
        'submitted':false
      });
      return response.json();
    }).then(function(json) {
      var success = json;

      if(success.hasOwnProperty('url')) {
        var archiveId = success.url.split('/');
        archiveId = archiveId[archiveId.length-1];
        var completeURL = '/archive/edit/id/' + archiveId;
        document.cookie = 'importing=' + archiveId;
        self.transitionTo('edit-archive',{archiveId:archiveId});
      }

    }).catch(function(err){
      console.log(err);
    });
  },
  render: function render() {
    var contentClasses = classNames('content', 'side', {'sidebar-open' : this.state.sidebarOpen}),
        header = 'Import data.',
        self = this;
    return (
      <div className={contentClasses}>
        <a className="toggle-menu" href="#" onClick={this.openSidebar}>
          ↓
        </a>
        <nav className='section-nav'>
          <h1>Recent Archives</h1>
          <ArchiveList archives={this.state.archives} />
        </nav>
        <div className='main content'>
          <div className='main-content-interior'>
            <Message type="imported" close={this.closeSuccess} message={this.state.success} />
            <Message type="loggedout" close={this.closeLoggedout} message={this.state.loggedout} />
            <Message type="error" close={this.closeError} message={this.state.error} />
            <header className="alpha welcome">
              <h1>{header}</h1>
            </header>

            <Tabs>
              <Tabs.Panel title='SAMI'>
                <ImportArchive submitted={this.state.submitted} importingSami={this.state.importingSami}/>
                <div className='import-form'>
                  {this.state.submitted?<Progress />:''}
                  {this.state.submitted?'':<textarea disabled={this.state.submitted} value={this.state.session} onChange={this.handleChange}></textarea>}
                  {this.state.submitted?'':<button className='submit' onClick={this.importSession}>import</button>}
                </div>
              </Tabs.Panel>
              <Tabs.Panel title='Trials'>
                <ImportTrials ref='importTrial' importTrial={this.importTrial} submitted={this.state.submitted} />
              </Tabs.Panel>
              <Tabs.Panel title='CSV'>
                <div className='import-form'>
                  <p>Import CSV by uploading or pasting it below.</p>
                  {this.state.submitted?<Progress />:''}
                  {this.state.submitted?'':
                    <Dropzone onDrop={this.uploadCSV} multiple={false} className="import-dropzone" accept={this.state.allowedCSVMimeTypes} >
                      <div>Try dropping your CSV file here, or click to select file to upload.</div>
                    </Dropzone>}
                </div>
              </Tabs.Panel>
            </Tabs>

          </div>
        </div>
      </div>
    );
  }
});
