'use strict';

var Layout = require('./layout.jsx');
var React = require('react');
var Router = require('react-router');
var Link = Router.Link;
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;
var Menu = require('./components/menu.jsx');
var Landing = require('./components/landing.jsx');
var Home = require('./home.jsx');

module.exports = React.createClass({
  getDefaultProps: function () {
    return { home: true };
  },
  render: function render() {
    if(this.props.url) {
      return (
        <Layout {...this.props}>
          <Router.RouteHandler {...this.props}/>
        </Layout>
      );
    } else {
      return (
        <Layout {...this.props}>
          <Home loggedIn={this.props.loggedIn} logout={this.props.logout} welcome={this.props.welcome} title={this.props.title} />
        </Layout>
      );
    }
  }
});
