'use strict';

var React = require('react');
var Qajax = require('qajax');
var Message = require('./components/messages/message.jsx');
var Progress = require('./components/progress-bar.jsx');
var Router = require('react-router');
var SearchResults = require('./components/search/search-results.jsx');
var SearchFilter = require('./components/search/search-filter.jsx');
var SearchQuery = require('./components/search/search-query.jsx');
var ReactPaginate = require('react-paginate');

module.exports = React.createClass({
  getInitialState: function () {
    if(typeof(window) == 'undefined'){
      global.window = new Object();
    }

    return {
      error: '',
      searching: false,
      success: '',
      results: [],
      page: 0,
      pages: 0,
      per_page: 5,
      searchFilters: [0],
      bool: ['must','must_not'],
      field: ['_all','archive.metadata.key','archive.metadata.value'],
      filter: true
    };
  },
  searchArchivesPage: function(page) {
    this.search(page);
  },
  searchArchives: function(event) {
    if (event) event.preventDefault();
    var page = this.state.page;
    this.search(page);
  },
  search: function(page) {
    var self = this;
    var url = '/search';
    var filters = {must:{},must_not:{}};
    var searchFilters = this.state.searchFilters;
    var valid = true;

    if(this.state.filter) {
      for(var i = 0; i < searchFilters.length; i++) {
        var filter = searchFilters[i];
        var searchFilter = self.refs['filter_'+filter].state;
        if(searchFilter.queryType === 'term' && searchFilter.value === '') {
          valid = false;
          break;
        }

        if(searchFilter.queryType === 'range') {
          if(searchFilter.from === '') searchFilter.from = 0;
          if(searchFilter.to === '') searchFilter.to = 0;

          filters[searchFilter.bool][searchFilter.field] = {
            from: searchFilter.from,
            to: searchFilter.to
          };

        } else {
          if(filters[searchFilter.bool].hasOwnProperty(searchFilter.field)) {
            filters[searchFilter.bool][searchFilter.field] += ' OR ' + searchFilter.value;
          } else {
            filters[searchFilter.bool][searchFilter.field] = searchFilter.value;
          }
        }
      }
    } else {
      var query = self.refs.search.state.value;
      url = '/search/find';
      if(query === '') valid = false;
    }

    if(!valid) return self.setState({error: 'Please provide a value.'});

    self.setState({searching: true});

    if(page === 0) page = 1;

    var q = this.state.filter ? JSON.stringify(filters) : query;

    var params = {
      q: q,
      per_page: self.state.per_page,
      page: page
    };

    Qajax({ url: url, headers: { "Content-Type" : "application/json"}, method: "GET", params: params })
      .then(Qajax.filterSuccess)
      .get("responseText")
      .then(function (txt) {
        var response = JSON.parse(txt);
        var results = response.results || [];
        if(results.length < 1) results = null;
        self.closeError();
        var page = parseInt(response.page);
        if(response.pages === 0) page = 0;
        self.setState({
          searching: false,
          results: results,
          page: page,
          pages: response.pages
        });
      }, function (err) {
        if(err && err.status && err.status === 401) {
          self.setState({error: 'You have been logged out.'});
        }
        self.setState({
          searching: false,
          results: [],
          page: 0
        });
        var errorObj = self.formatErrors(err.responseText);
        if(errorObj === null) {
          self.setState({error: err.statusText});
        } else {
          self.setState({error: errorObj});
        }
      });
  },
  formatErrors: function(errorJsonString) {
    var errorObj = JSON.parse(errorJsonString);
    if(!errorObj.hasOwnProperty('error')) return null;
    try {
      var errorObjParsed = JSON.parse(errorObj.error);
    } catch (e) {
      console.log(e);
      return errorObj.error;
    }
    errorObj = errorObjParsed;

    if(Array.isArray(errorObj)) {
      return errorObj.map(function(error){
        var message = error.field.replace('data.','') + ' ' + error.message;
        return message;
      });
    } else {
      return errorObj.error;
    }
  },
  closeSuccess: function() {
    this.setState({success: ''});
  },
  closeError: function() {
    this.setState({error: ''});
  },
  addSearchFilter: function(e) {
    e.preventDefault();
    var searchFilters = this.state.searchFilters;
    searchFilters.push(searchFilters.length);
    this.setState({searchFilters:searchFilters});
  },
  removeSearchFilter: function(searchFilter,e) {
    e.preventDefault();
    if(this.state.searchFilters.length < 2) return false;
    var searchFilters = this.state.searchFilters;
    var index = searchFilters.indexOf(searchFilter);
    searchFilters.splice(index, 1);
    this.setState({searchFilters:searchFilters});
  },
  isLastFilter: function(searchFilter) {
    var searchFilters = this.state.searchFilters;
    return searchFilter === searchFilters[searchFilters.length-1];
  },
  getMetadaKeySuggestions: function(query, callback) {
    var url = '/search/metadata/key';
    var params = {q: query};

    Qajax({ url: url, headers: { "Content-Type" : "application/json"}, method: "GET", params: params })
      .then(Qajax.filterSuccess)
      .get("responseText")
      .then(function (txt) {
        var results = JSON.parse(txt);
        callback(null,results);
      }, function (err) {
        callback(err.responseText,null);
      });
  },
  handlePageClick: function(data) {
    this.searchArchivesPage((data.selected + 1));
  },
  handleSearchTypeClick: function() {
    this.setState({filter: !this.state.filter});
  },
  render: function render() {
    return (
      <div>
        <div className='main content'>
          <div className='main-content-interior'>
            <Message type="error" close={this.closeError} message={this.state.error} />
            <header className="alpha welcome">
              <h1>Search</h1>
              <p>Find archives by searching metadata.</p>
            </header>
            <div className='search-form'>
              <form className='search-form'>
                {this.state.filter
                  ? <h2 className='search-heading'>Filters <small><a onClick={this.handleSearchTypeClick}>(use text search instead)</a></small></h2>
                  : <h2 className='search-heading'>Search <small><a onClick={this.handleSearchTypeClick}>(use filter search instead)</a></small></h2>
                }

                {this.state.filter ?
                  <div>
                    {this.state.searchFilters.map(function(searchFilter){
                      return <SearchFilter ref={'filter_'+searchFilter} key={searchFilter} searchFilter={searchFilter} totalFilters={this.state.searchFilters.length} submited={this.state.submitted} bool={this.state.bool} field={this.state.field} removeSearchFilter={this.removeSearchFilter} addSearchFilter={this.addSearchFilter} isLastFilter={this.isLastFilter(searchFilter)} getMetadaKeySuggestions={this.getMetadaKeySuggestions} />
                    },this)}
                  </div>
                  :
                  <div>
                    <SearchQuery ref="search" submited={this.state.submitted} />
                  </div>
                }

                {this.state.searching?<Progress />:<button className='submit' onClick={this.searchArchives}>search</button>}
              </form>
              <SearchResults heading={'Search results'} results={this.state.results} />
              {this.state.results !== null && this.state.results.length > 0 ?
              <ReactPaginate previousLabel={"«"}
                nextLabel={"»"}
                breakLabel={<li className="break"><a href="">...</a></li>}
                pageNum={this.state.pages}
                clickCallback={this.handlePageClick}
                containerClassName={"pagination"}
                subContainerClassName={"pages pagination"}
                activeClassName={"active"} />
              : ''}
            </div>
          </div>
        </div>
      </div>
    );
  }
});
