'use strict';

var React = require('react');
var Router = require('react-router');
var Link = Router.Link;
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;
var Landing = require('./components/landing.jsx');

module.exports = React.createClass({
    displayName: 'home',
    getDefaultProps: function () {
      return { home: true };
    },
    render: function render() {
      return (
        <div className='content side'>
          <div className='main content'>
            <div className='main-content-interior'>
              <header className="alpha welcome">
                <h1>{this.props.welcome} {this.props.title}.</h1>
                <p>Query, retrieve, update and share Simband data after it has been recorded.</p>
                {!this.props.loggedIn ? <a href='/login' className='button login-btn' >login »</a> : ''}
              </header>
              <Landing loggedIn={this.props.loggedIn} />
            </div>
          </div>
        </div>
      );
    }
});
