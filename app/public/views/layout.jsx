'use strict';

var React = require('react');
var Router = require('react-router');
var Menu = require('./components/menu.jsx');
var classNames = require('classnames');

module.exports = React.createClass({
  mixins: [ Router.State ],
  getInitialState: function () {
    if(typeof(window) == 'undefined'){
      global.window = new Object();
    }

    return {menuExpanded : false};
  },
  handleResize: function(e) {
    // close mobile menu if we are using a bigger screen width e.g. non-mobile screen.
    var closeMenu = false;
    if(window && window.outerWidth){
      closeMenu = window.outerWidth > 760;
    } else if(document) {
      closeMenu = document.body.clientWidth > 760;
    }

    if(closeMenu) this.setState({menuExpanded: false});
  },
  componentDidMount: function() {
    if(window.addEventListener) window.addEventListener('resize', this.handleResize);
  },
  componentWillUnmount: function() {
    if(window.removeEventListener) window.removeEventListener('resize', this.handleResize);
  },
  toggleMenu: function() {
    this.setState({menuExpanded: !this.state.menuExpanded});
  },
  render: function render() {
    try {
      var bodyClass = this.props.hasOwnProperty('home')
      || this.getPath() === '/' ? 'index' : '';
      if(this.getPath() === '/import') bodyClass = 'sami';
      if(this.getPath() === '/search-archive') bodyClass = 'community';
      if(this.getPath() === '/archives') bodyClass = 'simbase';
      // simbase
    } catch(e) {
      var bodyClass = 'index';
    }

    var classes = classNames(bodyClass, {'menu-expanded' : this.state.menuExpanded});

    return (
      <html>
        <head>
          <meta charSet='utf-8' />
          <title>
            {this.props.title}
          </title>
          <link href="/css/samsung/application.css" rel="stylesheet" type="text/css" />
          <link href="/css/main.css" rel="stylesheet" type="text/css" />
          <link href="/css/react-simpletabs/react-simpletabs.min.css" rel="stylesheet" type="text/css" />
          <link href="/css/react-select/react-select.css" rel="stylesheet" type="text/css" />
          <link href="/css/react-widgets/css/react-widgets.css" rel="stylesheet" type="text/css" />
          <link href="/css/samsung/print.css" rel="stylesheet" type="text/css" media="print" />
        </head>
        <body className={classes}>
          <div className="container">
            {this.props.hasOwnProperty('loggedIn') ? <Menu loggedIn={this.props.loggedIn} logout={this.props.logout} toggleMobileMenu={this.toggleMenu}/> : <Menu noLinks={true} /> }
            {this.props.children}
          </div>
        </body>
        <script src='/bundle.js'></script>
      </html>
    );
  }
});
