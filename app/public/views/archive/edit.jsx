'use strict';

var React = require('react');
var Qajax = require('qajax');
var Message = require('../components/messages/message.jsx');
var Router = require('react-router');
var ArchiveData = require('../components/archive/archive-data.jsx');
var ArchiveTable = require('../components/archive/archive-table.jsx');
var MetadataTable = require('../components/metadata/metadata-table.jsx');
var ImportArchiveFieldTable = require('../components/import/archive-fields-table.jsx');
var ImportArchiveTable = require('../components/import/archive-table.jsx');
var ImportMetadataTable = require('../components/import/metadata-table.jsx');
var ArchiveDetails = require('../components/archive/archive-details.jsx');
var ReactTabs = require('react-tabs');
var Tab = ReactTabs.Tab;
var Tabs = ReactTabs.Tabs;
var TabList = ReactTabs.TabList;
var TabPanel = ReactTabs.TabPanel;
var SimpleTabs = require('react-simpletabs');
var _ = require('lodash');
var Progress = require('../components/progress-bar.jsx');
var classNames = require('classnames');
var ArchiveManage = require('../components/archive/archive-manage.jsx');
var Navigation = Router.Navigation;
require('es6-promise').polyfill();
require('isomorphic-fetch');

module.exports = React.createClass({
  mixins: [Navigation],
  getInitialState: function () {
    if(typeof(window) == 'undefined'){
      global.window = new Object();
    }

    if(typeof(document) == 'undefined'){
      global.document = {
        cookie: '',
        location: null
      }
    }

    return {
      data: [],
      defaultTab: 0,
      error: '',
      keys: [],
      success: '',
      info: '',
      archive: {},
      metadata: [],
      tableData: [],
      hasArchiveData: true,
      archiveDataLoaded: false,
      sidebarOpen: false,
      updatingArchive: false,
      recreate: false,
      duplicating: false
    };
  },
  componentWillMount: function() {
    var cookie = this.getCookie('importing');
    var success = '';
    if(cookie && cookie === this.props.params.archiveId) {
      document.cookie = "importing=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
      success = 'Please complete your archive import by selecting data to import.';
    }
    this.setState({
      success: success
    });
    this.getFilters();
  },
  getCookie: function(cookieName) {
    var name = cookieName + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0)==' ') c = c.substring(1);
      if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
  },
  openSidebar: function() {
    this.setState({sidebarOpen: !this.state.sidebarOpen});
  },
  getArchive: function() {
    var self = this;
    var filters = this.state.filters;
    Qajax({ url: "/archive/"+this.props.params.archiveId, headers: { "Content-Type" : "application/json"}, method: "GET" })
      .then(Qajax.filterSuccess)
      .get("responseText")
      .then(function (txt) {
        try {
          var archive = JSON.parse(txt);
          var metadata = [];
          if(archive.hasOwnProperty('metadata')) metadata = archive.metadata;

          metadata = metadata.map(function(m){
            m.import = true;
            var filter = _.find(filters.metadata, function(f){
              return f.key === m.key && f.value === m.value;
            });
            if(filter && filter.hidden) m.import = false;
            return m;
          });

          var data = [];
          var keys = [];
          var archiveDataLoaded = true;
          if(archive.hasOwnProperty('data')) {
            archive.data = JSON.parse(archive.data);
            try {
              data = archive.data;
              if(data) {
                keys = data.map(function(d){
                  return {
                    key: Object.keys(d)[0],
                    import: true
                  };
                });
              }
              var count = 0;
              var tableData = data.map(function(k, index){
                var key = Object.keys(k)[0];
                if(k[key].hasOwnProperty('t')) {
                  return k[key].t.map(function(t, i){
                    var importData = true;
                    var el = [key, k[key].s[i], t, importData, count];
                    var filter = _.find(filters.data, function(d){
                      var details = d;
                      if(d.hasOwnProperty('data')) details = JSON.parse(d.data);

                      return el[0] === details.field && el[1] === details.timestamp && el[2] === details.sample;
                    });
                    if(filter) el[3] = false;
                    count++;
                    return el
                  });
                } else {
                  var d = k.data;
                  var keys = Object.keys(d);
                  return keys.map(function(v, i){
                    var value = d[v];
                    if(_.isObject(value)) value = JSON.stringify(value);
                    var el =  [v,value,k.ts, true, count];
                    count++;
                    return el;
                  });
                }
              });
              tableData = _.flatten(tableData);
              console.log(tableData[0]);
            } catch(e) {
              console.log(e);
            }
          } else {
            archiveDataLoaded = false;
            setTimeout(function(){
              self.getArchive();
            }, 5000);
          }

          self.setState({
            archive: archive,
            metadata: metadata,
            data: data,
            keys: keys,
            tableData: tableData,
            archiveDataLoaded: archiveDataLoaded
          });
        } catch(e) {
          console.log(e);
        }
      }, function (err) {
        console.log("xhr failure: ", err);
        var err = JSON.parse(err.responseText);
        var error = err.hasOwnProperty('error') ? err.error : err;
        if(typeof err !== "string") err = JSON.stringify(err);
        self.setState({error: error});
      });
  },
  getFilters: function() {
    var self = this;
    Qajax({ url: "/archive/filter/"+this.props.params.archiveId, headers: { "Content-Type" : "application/json"}, method: "GET" })
      .then(Qajax.filterSuccess)
      .get("responseText")
      .then(function (txt) {
        try {
          var filters = JSON.parse(txt);
          self.setState({filters: filters});
          self.getArchive();
        } catch(e) {
          console.log(e);
        }
      }, function (err) {
        console.log("xhr failure: ", err);
        var err = JSON.parse(err.responseText);
        var error = err.hasOwnProperty('error') ? err.error : err;
        if(typeof err !== "string") err = JSON.stringify(err);
        self.setState({error: error});
      });
  },
  addMetadata: function(key, value) {
    this.setState({
      error: '',
      success: '',
      updatingArchive: true
    });
    var self = this;
    var url = '/archive/'+this.props.params.archiveId+'/metadata/add';
    var data = {
      key: key,
      value: value
    };
    Qajax({ url: url, headers: { "Content-Type" : "application/json"}, method: "POST", data: data })
      .then(Qajax.filterSuccess)
      .get("responseText")
      .then(function (txt) {
        try {
          var m = JSON.parse(txt);
          var metadata = self.state.metadata;
          metadata.push(m);
          self.setState({
            defaultTab: 1,
            error: '',
            metadata: metadata,
            success: 'Metadata added.',
            updatingArchive: false
          });
          console.log(self.state.metadata);
          return true;
        } catch(e) {
          self.setState({error: e});
          return false;
        }
      }, function (err) {
        console.log("xhr failure: ", err);
        var err = JSON.parse(err.responseText);
        var error = err.hasOwnProperty('error') ? err.error : err;
        self.setState({
          error: error,
          success: '',
          updatingArchive: false
        });
        return false;
      });
  },
  closeSuccess: function() {
    this.setState({success: ''});
  },
  closeInfo: function() {
    this.setState({info: ''});
  },
  closeError: function() {
    this.setState({error: ''});
  },
  recreateArchive: function(e) {
    e.preventDefault();

    var archive = this.state.archive;
    archive.status = 'Importing';
    this.setState({
      archive: archive,
      recreate: true
    });
  },
  setFilters: function() {
    this.complete();
  },
  createArchive: function(e) {
    e.preventDefault();
    console.log('create archive...');
    var self = this;
    var canEdit = this.state.archive.hasOwnProperty('canEdit');
    self.setState({
      error: '',
      success: '',
      updatingArchive: true,
      recreate: false
    });

    Qajax({ url: '/archive/'+this.props.params.archiveId+'/create', headers: { "Content-Type" : "application/json"}, method: "POST", data: {} })
      .then(Qajax.filterSuccess)
      .get("responseText")
      .then(function (txt) {
        try {
          var archive = JSON.parse(txt);
          if(canEdit) archive.canEdit = true;
          self.setState({
            archive: archive,
            error: '',
            success: 'Archive created.',
            updatingArchive: false
          });
          return true;
        } catch(e) {
          self.setState({
            error: e,
            success: '',
            updatingArchive: false
          });
          return false;
        }
      }, function (err) {
        console.log("xhr failure: ", err);
        var err = JSON.parse(err.responseText);
        var error = err.hasOwnProperty('error') ? err.error : err;
        self.setState({error: error});
        return false;
      });
  },
  handleSelected: function (index, last) {
    console.log('Selected tab: ' + index + ', Last tab: ' + last);
  },
  selectData: function(e) {
    var index = parseInt(e.target.value);
    var checked = e.target.checked;
    var tableData = this.state.tableData;
    var data = _.find(tableData, function(d){
      return d[4] === index;
    });
    data[3] = checked;
    this.setState({tableData: tableData});
  },
  selectMetadata: function(e) {
    var index = e.target.value;
    var checked = e.target.checked;
    var metadata = this.state.metadata;
    var m = metadata[index];
    m.import = checked;
    this.setState({metadata: metadata});
  },
  selectField: function(e) {
    var field = e.target.value; // field name
    var checked = e.target.checked;
    var tableData = this.state.tableData;
    tableData = tableData.map(function(data){
      if(data[0] === field) {
        data[3] = checked;
      }
      return data;
    });
    var keys = this.state.keys;
    keys = keys.map(function(key){
      if(key.key === field) {
        key.import = checked;
      }
      return key;
    });

    this.setState({
      keys: keys,
      tableData: tableData
    });
  },
  complete: function() {
    var data = _.filter(this.state.tableData, function(d){
      return d[3] === false;
    });
    var metadata = _.filter(this.state.metadata, function(m){
      return m.import === false;
    });

    var self = this;

    self.setState({
      archiveDataLoaded: false,
      success: '',
      error: ''
    });

    var url = '/archive/filter/' + this.props.params.archiveId;
    var payload = {
      data: data,
      metadata: metadata
    };

    Qajax({ url: url, headers: { "Content-Type" : "application/json"}, method: "POST", data: payload })
      .then(Qajax.filterSuccess)
      .get("responseText")
      .then(function (txt) {
        console.log(txt);
        var archive = JSON.parse(txt);
        self.setState({
          archive: archive,
          archiveDataLoaded: true,
          info: {'url': archive.url, 'expires': archive.expires}
        });
      }, function (err) {
        console.log("xhr failure: ", err);
        self.setState({
          error: err,
          success: '',
          updatingArchive: false
        });
      });

  },
  duplicate: function(e) {
    if(e) e.preventDefault();
    var self = this;
    var archiveId = self.props.params.archiveId;
    self.setState({duplicating: true});
    fetch('/archive/' + archiveId + '/duplicate', {
      credentials: 'same-origin',
      method: 'post',
      headers: {
        'Accept': 'application/json'
      },
      body: {}
    }).then(function (response) {
      self.setState({duplicating: false});
      return response.json();
    }).then(function (json) {
      var success = json;

      if (success.hasOwnProperty('filename')) {
        var archiveId = success.filename.split('.');
        archiveId = archiveId[0];
        var completeURL = '/archive/edit/id/' + archiveId;
        document.location = completeURL;
      }

    }).catch(function (err) {
      console.log(err);
    });
  },
  render: function render() {
    var contentClasses = classNames('content', 'side', {'sidebar-open' : this.state.sidebarOpen});
    return (
      <div className={contentClasses}>
        <a className="toggle-menu" href="#" onClick={this.openSidebar}>
          ↓
        </a>
        <nav className='section-nav manage-archive'>
          <h1>Manage</h1>
          {this.state.updatingArchive ?
            <Progress />
            :<ArchiveManage
              archiveId={this.props.params.archiveId}
              archive={this.state.archive}
              addMetadata={this.addMetadata}
              createArchive={this.recreateArchive}
              duplicate={this.duplicate}
              duplicating={this.state.duplicating} />
          }
        </nav>
        <div className='main content'>
          <div className='main-content-interior archive-detail-content'>
            <Message type="info" close={this.closeInfo} message={this.state.info} />
            <Message type="success" close={this.closeSuccess} message={this.state.success} />
            <Message type="error" close={this.closeError} message={this.state.error} />
            <header className="alpha welcome">
              <ArchiveDetails archiveId={this.props.params.archiveId} archive={this.state.archive} />
            </header>

            {this.state.hasArchiveData && this.state.archive.status === 'Importing'?
              <div>
              {this.state.archiveDataLoaded && this.state.hasArchiveData ?
                <div>
                  <p>Unselect data or metadata you do not want to import into the archive.</p>

                  {this.state.recreate ?
                    <button className='submit complete-archive-import' onClick={this.setFilters}>Confirm data</button>
                    :
                    <button className='submit complete-archive-import' onClick={this.complete}>Complete</button>
                  }

                  <SimpleTabs>
                    <SimpleTabs.Panel title='Fields'>
                      <div className="archive-display">
                        <ImportArchiveFieldTable selectField={this.selectField} data={this.state.keys} />
                      </div>
                    </SimpleTabs.Panel>
                    <SimpleTabs.Panel title='Data'>
                      <div className="archive-display">
                        <ImportArchiveTable selectData={this.selectData} data={this.state.tableData} perPage={5} />
                      </div>
                    </SimpleTabs.Panel>
                    <SimpleTabs.Panel title='Metadata'>
                      <div className="archive-display">
                        <ImportMetadataTable selectMetadata={this.selectMetadata} metadata={this.state.metadata} />
                      </div>
                    </SimpleTabs.Panel>
                  </SimpleTabs>
                </div>
                : <Progress />}
              </div>
              :
              <div>
              {this.state.archiveDataLoaded && this.state.hasArchiveData ?
                <Tabs onSelect={this.handleSelected} selectedIndex={this.state.defaultTab} >

                  <TabList>
                    <Tab>Charts</Tab>
                    <Tab>Data</Tab>
                    <Tab>Metadata</Tab>
                  </TabList>

                  <TabPanel>
                    <ArchiveData keys={this.state.keys} data={this.state.data} />
                  </TabPanel>
                  <TabPanel>
                    <ArchiveTable data={this.state.tableData} perPage={5} />
                  </TabPanel>
                  <TabPanel>
                    <MetadataTable metadata={this.state.metadata} />
                  </TabPanel>

                </Tabs>
                : <Progress />}
              </div>
            }


            {this.state.archiveDataLoaded && !this.state.hasArchiveData ?
              <p>No data available.</p>
            : '' }

          </div>
        </div>
      </div>
    );
  }
});
