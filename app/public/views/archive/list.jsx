'use strict';

var React = require('react');
var Qajax = require('qajax');
var Message = require('../components/messages/message.jsx');
var Router = require('react-router');
var ArchiveData = require('../components/archive/archive-data.jsx');
var SearchResults = require('../components/search/search-results.jsx');
var ReactPaginate = require('react-paginate');

module.exports = React.createClass({
  getInitialState: function () {
    if(typeof(window) == 'undefined'){
      global.window = new Object();
    }

    return {
      error: '',
      searching: false,
      success: '',
      results: [],
      page: 0,
      pages: 0,
      per_page: 5
    };
  },
  componentDidMount: function() {
    this.search(this.state.page);
  },
  closeSuccess: function() {
    this.setState({success: ''});
  },
  closeError: function() {
    this.setState({error: ''});
  },
  formatErrors: function(errorJsonString) {
    var errorObj = JSON.parse(errorJsonString);
    if(!errorObj.hasOwnProperty('error')) return null;
    try {
      var errorObjParsed = JSON.parse(errorObj.error);
    } catch (e) {
      console.log(e);
      return errorObj.error;
    }
    errorObj = errorObjParsed;

    if(Array.isArray(errorObj)) {
      return errorObj.map(function(error){
        var message = error.field.replace('data.','') + ' ' + error.message;
        return message;
      });
    } else {
      return errorObj.error;
    }
  },
  search: function(page) {
    var self = this;
    var url = '/search/user/archives';

    self.setState({searching: true});

    if(page === 0) page = 1;

    var params = {
      per_page: self.state.per_page,
      page: page
    };

    Qajax({ url: url, headers: { "Content-Type" : "application/json"}, method: "GET", params: params })
      .then(Qajax.filterSuccess)
      .get("responseText")
      .then(function (txt) {
        var response = JSON.parse(txt);
        var results = response.results || [];
        if(results.length < 1) results = null;
        self.closeError();
        var page = parseInt(response.page);
        if(response.pages === 0) page = 0;
        self.setState({
          searching: false,
          results: results,
          page: page,
          pages: response.pages
        });
      }, function (err) {
        if(err && err.status && err.status === 401) {
          self.setState({error: 'You have been logged out.'});
        }
        self.setState({
          searching: false,
          results: [],
          page: 0
        });
        var errorObj = self.formatErrors(err.responseText);
        if(errorObj === null) {
          self.setState({error: err.statusText});
        } else {
          self.setState({error: errorObj});
        }

      });
  },
  handlePageClick: function(data) {
    this.search((data.selected + 1));
  },
  render: function render() {
    return (
      <div>
        <div className='main content archive-list'>
          <div className='main-content-interior'>
            <Message type="error" close={this.closeError} message={this.state.error} />
            <header className="alpha welcome">
              <h1>Your Archives</h1>
            </header>
          </div>
          <SearchResults results={this.state.results} />
          {this.state.results !== null && this.state.results.length > 0 ?
          <ReactPaginate previousLabel={"«"}
            nextLabel={"»"}
            breakLabel={<li className="break"><a href="">...</a></li>}
            pageNum={this.state.pages}
            clickCallback={this.handlePageClick}
            containerClassName={"pagination"}
            subContainerClassName={"pages pagination"}
            activeClassName={"active"} />
           : ''}
        </div>
      </div>
    );
  }
});
