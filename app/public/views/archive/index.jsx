'use strict';

var React = require('react');
var ArchiveData = require('../components/archive/archive-data.jsx');
var ArchiveTable = require('../components/archive/archive-table.jsx');
var MetadataTable = require('../components/metadata/metadata-table.jsx');
var Tabs = require('react-simpletabs');

module.exports = React.createClass({
  getInitialState: function () {
    if(typeof(window) == 'undefined'){
      global.window = new Object();
    }

    return {};
  },
  componentDidMount: function() {
    if(this.props.error && (this.props.error === 'New' || this.props.error === 'Uploading')) {
      setTimeout(function(){
        window.location.reload(1);
      }, 5000);
    }
  },
  render: function render() {
    return (
      <div className='main content'>
        {this.props.hasOwnProperty('error')
          ?<div>
              <h1>Archive</h1>
              <p><strong>Status:</strong> {this.props.error}.</p>
            </div>
          :
          <Tabs>
            <Tabs.Panel title='Data Table'>
              <div className="archive-display">
                <ArchiveTable data={this.props.tableData} perPage={5} />
              </div>
            </Tabs.Panel>
            <Tabs.Panel title='Data Charts'>
              <div className="archive-display">
                <ArchiveData keys={this.props.keys} data={this.props.data} />
              </div>
            </Tabs.Panel>
            <Tabs.Panel title='Metadata'>
              <div className="archive-display">
                <MetadataTable metadata={this.props.metadata} />
              </div>
            </Tabs.Panel>
          </Tabs>
        }

      </div>
    );
  }
});
