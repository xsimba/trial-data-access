'use strict';

var Routes = require('./routes.jsx');
var Client = require('react-engine/lib/client');

require('./views/**/*.jsx', {glob: true});

var options = {
  routes: Routes,

  viewResolver: function(viewName) {
    return require('./views/' + viewName);
  }
};

document.addEventListener('DOMContentLoaded', function onLoad() {
  Client.boot(options);
});
