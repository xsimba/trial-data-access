'use strict';

var React = require('react');
var Router = require('react-router');

var App = require('./views/app.jsx');
var Home = require('./views/home.jsx');
var Import = require('./views/sami/import.jsx');
var Search = require('./views/search.jsx');
var Archive = require('./views/archive/index.jsx');
var Edit = require('./views/archive/edit.jsx');
var List = require('./views/archive/list.jsx');

var routes = module.exports = (
  <Router.Route path='/' handler={App}>
    <Router.DefaultRoute name='home' handler={Home} />
    <Router.Route path='/import' name='import' handler={Import} />
    <Router.Route path='/search-archive' name='search-archive' handler={Search} />
    <Router.Route path='/archive/id/:archiveId' name='archive' handler={Archive} />
    <Router.Route path='/archive/edit/id/:archiveId' name='edit-archive' handler={Edit} />
    <Router.Route path='/archives' name='list-archive' handler={List} />
  </Router.Route>
);
