var express = require('express')
  , router = express.Router()
  , logger = require('../../lib/logger')
  , archive = require('../../lib/elastic/archive')
  , archive_aws = require('../../lib/services/archive/aws')
  , archive_session = require('../../lib/services/archive/session')
  , archive_user = require('../../lib/services/archive/user')
  , Metadata = require('../../lib/services/archive/metadata')
  , request = require('superagent')
  , passport = require('passport')
  , config = require('../../config/config')
  , path = require('path')
  , dynamodb = require('../../lib/services/aws/dynamodb')
  , _ = require('lodash')
  , transform = require('../../lib/services/transform/transform')
  , filtered = require('../../lib/services/archive/filtered')
  , duplicate = require('../../lib/services/archive/duplicate');

module.exports = function (app) {
  app.use('/archive', router);
};

/**
 * Get archive page for editing.
 */
router.get('/edit/id/:archiveId', function (req, res, next) {
  logger.info('Get archive page for editing.');

  if(!req.isAuthenticated()) {
    return res.redirect('/');
  }

  res.render('/archive'+req.url,{
    url: '/archive'+req.url,
    name: 'edit-archive',
    title: req.i18n.__('title'),
    loggedIn: req.user ? true : false,
    logout: config.links.logout + encodeURIComponent(config.app.url) + config.links.logoutRedirect,
    archiveId: req.params.archiveId
  });

});

/**
 * Get archive by archiveId.
 */
router.get('/id/:archiveId', function(req, res, next) {
  logger.info('Get archive.' + req.params.archiveId);

  archive_session.GetByUUID(req, true, function (err, archive) {
    if (err) return res.status(500).json(getError(err, req));

    var type = 'json';

    if (archive.status === "Uploaded") {
      if(archive.type === '.csv') {
        return res.redirect(archive.url);
      }
      request
        .get(archive.url)
        .end(function (err, response) {
          if (err) return res.status(500)[type](err);

          if(req.useragent.browser === "unknown") {
            return res.status(201)[type](response.body);
          } else {
            var keys = [], data, metadata;

            var archive = response.body;

            metadata = archive.metadata;
            metadata = flattenObject(metadata);

            var m = [];
            for (var key in metadata) {
              if (metadata.hasOwnProperty(key)) {
                m.push({
                  key: key,
                  value: metadata[key]
                })
              }
            }

            metadata = m;

            try {
              data = archive.data;
              if(typeof data !== "object") data = JSON.parse(data);

              finalData = [];
              if (data) {
                data.forEach(function(d){
                  var key = Object.keys(d)[0];
                  if(d[key].hasOwnProperty('s') && d[key].s.length > 0) {
                    keys.push(key);
                    finalData.push(d);
                  }
                });
              }

              if(keys.length>0) data = finalData;

              transform.Table.Archive(data, function(err, tableData){
                if(err) return res.status(500)[type](err);

                res.render('/archive'+req.url,{
                  url: '/archive'+req.url,
                  name: 'edit-archive',
                  title: req.i18n.__('title'),
                  archiveId: req.params.archiveId,
                  archive: archive,
                  keys: keys,
                  data: data,
                  tableData: tableData,
                  metadata: metadata
                });
              });
            } catch(e) {
              logger.error(e);
              return res.status(500)[type](e);
            }

          }
        });
    } else {
      if (archive.status === 'Removed') archive.status = 'Expired';
      var result = {};
      result[req.i18n.__('archive_status')] = req.i18n.__(archive.status);
      if(req.useragent.browser === "unknown") {
        res.status(201)[type](result);
      } else {
        res.render('/archive'+req.url,{
          url: '/archive'+req.url,
          name: 'edit-archive',
          title: req.i18n.__('title'),
          error: archive.status
        });
      }
    }

  });
});

/**
 * Get list of user's archives.
 */
router.get('/user/list', function(req, res, next) {
  logger.info('Get archives that belong to user.');

  if(!req.user) return unAuthorizedRequest(req, res);

  archive_user.List(req, function (err, archives) {
    if (err) return res.status(500).json(getError(err, req));

    res.status(201).json(archives);

  });
});

router.post('/:archiveId/metadata/add', function (req, res, next) {
  if(!req.isAuthenticated()) {
    next();
  } else {
    addArchiveMetadata(req, res);
  }
});

router.post('/:archiveId/metadata/add',passport.authenticate('bearer', { session: false }),function(req, res, next){
  addArchiveMetadata(req, res);
});

/**
 * Add metadata to archive.
 *
 * @param req
 * @param res
 */
function addArchiveMetadata(req, res) {
  logger.info('Post add metadata to archive.' + req.body + ' ' + req.params.archiveId);

  archive_user.IsOwner(req, req.params.archiveId, function(err, isOwner){
    if(err) return res.status(500).json(err);
    if(!isOwner) return res.status(401).json('User does not have permission to edit archive.');
    Metadata.Service.Add(req.params.archiveId, req.body, function(err, results) {
      if (err) {
        logger.error(err);
        return res.status(500).json(getError(err, req));
      }
      if (results && results.error) return res.status(400).json(results.error);

      res.status(201).json(results);
    });
  });

}

router.get('/:archiveId/metadata', function (req, res, next) {
  if(!req.isAuthenticated()) {
    next();
  } else {
    getArchiveMetadata(req, res);
  }
});

router.get('/:archiveId/metadata',passport.authenticate('bearer', { session: false }),function(req, res, next){
  getArchiveMetadata(req, res);
});

/**
 * Get archive metadata.
 *
 * @param req
 * @param res
 */
function getArchiveMetadata(req, res) {
  logger.info('Get archive metadata. ' + req.params.archiveId);

  Metadata.Service.Get(req.params.archiveId, req.body, function(err, results) {
    if (err) return res.status(500).json(getError(err, req));
    if (results && results.error) return res.status(400).json(results.error);

    res.status(201).json(results);
  });
}

/**
 * First see if the user is authenticated by checking the request e.g. session.
 */
router.get('/:archiveId', function (req, res, next) {
  if(!req.isAuthenticated()) {
    next();
  } else {
    getArchive(req, res);
  }
});

/**
 * Finally check if a bearer token has been supplied.
 */
router.get('/:archiveId',passport.authenticate('bearer', { session: false }),function(req, res, next){
  getArchive(req, res);
});

/**
 * Get archive metadata.
 *
 * @param req
 * @param res
 */
function getArchive(req, res) {
  logger.info('Get archive. ' + req.params.archiveId);

  archive_session.GetByUUID(req, false, function (err, a) {
    if (err) return res.status(500).json(getError(err, req));
    if (a && a.error) return res.status(400).json(a.error);

    archive.get_archive(req.params.archiveId, function(err, result){
      if(err) logger.error(err);

      if(!err) {
        a.url = config.archive.proxy + path.basename(a.filename, path.extname(a.filename));
        a.data = result._source.data;
        a.metadata = result._source.metadata;
      }

      return res.status(200).json(a);
    });

  });
}

/**
 * First see if the user is authenticated by checking the request e.g. session.
 */
router.post('/:archiveId/create', function (req, res, next) {
  if(!req.isAuthenticated()) {
    next();
  } else {
    createArchiveAWS(req, res);
  }
});

/**
 * Finally check if a bearer token has been supplied.
 */
router.post('/:archiveId/create',passport.authenticate('bearer', { session: false }),function(req, res, next){
  createArchiveAWS(req, res);
});

/**
 * Get archive metadata.
 *
 * @param req
 * @param res
 */
function createArchiveAWS(req, res) {
  logger.info('Create archive on AWS. ' + req.params.archiveId);

  archive_user.IsOwner(req, req.params.archiveId, function(err, isOwner) {
    if (err) return res.status(500).json(err);
    if (!isOwner) return res.status(401).json('User does not have permission to edit archive.');
    archive_aws.CreateFromDatabase(req, function (err, archive) {
      if (err) return res.status(500).json(getError(err, req));
      if (archive && archive.error) return res.status(400).json(archive.error);

      res.status(201).json(archive);
    });
  });
}

/**
 * First see if the user is authenticated by checking the request e.g. session.
 */
router.get('/data/:archiveId', function (req, res, next) {
  if(!req.isAuthenticated()) {
    next();
  } else {
    getArchiveData(req, res);
  }
});

/**
 * Finally check if a bearer token has been supplied.
 */
router.get('/data/:archiveId',passport.authenticate('bearer', { session: false }),function(req, res, next){
  getArchiveData(req, res);
});

/**
 * Get archive data.
 *
 * @param req
 * @param res
 */
function getArchiveData(req, res) {
  logger.info('Get archive data. ' + req.params.archiveId);

  archive.get_archive(req.params.archiveId, function(err, archive){
    if (err) return res.status(500).json(getError(err, req));
    return res.status(200).json(archive._source.data);
  });
}

router.post('/filter/:archiveId', function (req, res, next) {
  if(!req.isAuthenticated()) {
    next();
  } else {
    filterArchive(req, res);
  }
});

router.post('/filter/:archiveId',passport.authenticate('bearer', { session: false }),function(req, res, next){
  filterArchive(req, res);
});

function filterArchive(req, res) {
  logger.info('Post filter archive.');

  archive_user.IsOwner(req, req.params.archiveId, function(err, isOwner) {
    if (err) return res.status(500).json(err);
    if (!isOwner) return res.status(401).json('User does not have permission to edit archive.');
    filtered.Reset(req, function(err, reset){
      if(err) return res.status(500).json(getError(err, req));
      filtered.Import(req, function(err, result){
        if(err) return res.status(500).json(getError(err, req));
        archive_aws.CreateFromDatabase(req, function (err, archive) {
          if (err) return res.status(500).json(getError(err, req));
          if (archive && archive.error) return res.status(400).json(archive.error);

          res.status(201).json(archive);
        });
      });
    })
  });

}

router.get('/filter/:archiveId', function (req, res, next) {
  if(!req.isAuthenticated()) {
    next();
  } else {
    getFilterArchive(req, res);
  }
});

router.get('/filter/:archiveId',passport.authenticate('bearer', { session: false }),function(req, res, next){
  getFilterArchive(req, res);
});

function getFilterArchive(req, res) {
  logger.info('Get filter archive.');

  filtered.Get(req.params.archiveId, function(err, filters){
    if(err) return res.status(500).json(getError(err, req));
    res.status(200).json(filters);
  });
}

router.post('/:archiveId/duplicate', function (req, res, next) {
  if(!req.isAuthenticated()) {
    next();
  } else {
    duplicateArchive(req, res);
  }
});

router.post('/:archiveId/duplicate',passport.authenticate('bearer', { session: false }),function(req, res, next){
  duplicateArchive(req, res);
});

function duplicateArchive(req, res) {
  var archiveId = req.params.archiveId;
  logger.info('Create duplicate archive from ' + archiveId);

  duplicate.Create(req, archiveId, function(err, archive){
    if(err) return res.status(500).json(getError(err, req));
    res.status(200).json(archive);
  });
}

/**
 * Return unauthorized request response.
 *
 * @param req
 * @param res
 * @returns {*}
 */
function unAuthorizedRequest(req, res) {
  var error = {};
  logger.error("Unauthorized request " + req.body.sessionId);
  error[req.i18n.__('error')] = "You are not authorized."; // TODO i18n...
  return res.status(401).json(error);
}

function getError(err, req) {
  var error = {};
  error[req.i18n.__('error')] = err;
  return error;
}

var flattenObject = function(ob) {
  var toReturn = {};

  for (var i in ob) {
    if (!ob.hasOwnProperty(i)) continue;

    if ((typeof ob[i]) == 'object') {
      var flatObject = flattenObject(ob[i]);
      for (var x in flatObject) {
        if (!flatObject.hasOwnProperty(x)) continue;

        toReturn[i + '/' + x] = flatObject[x];
      }
    } else {
      toReturn[i] = ob[i];
    }
  }
  return toReturn;
};
