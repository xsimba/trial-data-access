var express = require('express')
  , router = express.Router()
  , logger = require('../../lib/logger')
  , archive = require('../../lib/elastic/archive')
  , Metadata = require('../../lib/services/archive/metadata')
  , request = require('superagent')
  , passport = require('passport')
  , config = require('../../config/config')
  , path = require('path')
  , dynamodb = require('../../lib/services/aws/dynamodb')
  , _ = require('lodash')
  , csvexport = require('../../lib/export/csv')
  , csv = require('express-csv');

module.exports = function (app) {
  app.use('/export', router);
};

/**
 * First see if the user is authenticated by checking the request e.g. session.
 */
router.get('/csv/:archiveId', function (req, res, next) {
  if(!req.isAuthenticated()) {
    next();
  } else {
    exportArchiveAndMetadataToCSV(req, res);
  }
});

/**
 * Finally check if a bearer token has been supplied.
 */
router.get('/csv/:archiveId',passport.authenticate('bearer', { session: false }),function(req, res, next){
  exportArchiveAndMetadataToCSV(req, res);
});

/**
 * Get archive data.
 *
 * @param req
 * @param res
 */
function exportArchiveAndMetadataToCSV(req, res) {
  logger.info('Export archive to CSV. ' + req.params.archiveId);

  csvexport.all(req.params.archiveId, function(err, csvexport){
    if (err) return res.status(500).json(getError(err, req));
    var archiveFilename = req.params.archiveId + '.zip';
    res.set('Content-Type', 'application/zip')
    res.set('Content-Disposition', 'attachment; filename=' + archiveFilename);
    res.set('Content-Length', csvexport.length);
    res.end(csvexport, 'binary');
    return;
  });
}

/**
 * First see if the user is authenticated by checking the request e.g. session.
 */
router.get('/csv/archive/:archiveId', function (req, res, next) {
  if(!req.isAuthenticated()) {
    next();
  } else {
    exportArchiveToCSV(req, res);
  }
});

/**
 * Finally check if a bearer token has been supplied.
 */
router.get('/csv/archive/:archiveId',passport.authenticate('bearer', { session: false }),function(req, res, next){
  exportArchiveToCSV(req, res);
});

/**
 * Get archive data.
 *
 * @param req
 * @param res
 */
function exportArchiveToCSV(req, res) {
  logger.info('Export archive to CSV. ' + req.params.archiveId);

  csvexport.archive(req.params.archiveId, function(err, csvexport){
    if (err) return res.status(500).json(getError(err, req));
    return res.status(200).csv(csvexport);
  });
}

/**
 * First see if the user is authenticated by checking the request e.g. session.
 */
router.get('/csv/metadata/:archiveId', function (req, res, next) {
  if(!req.isAuthenticated()) {
    next();
  } else {
    exportMetadataToCSV(req, res);
  }
});

/**
 * Finally check if a bearer token has been supplied.
 */
router.get('/csv/metadata/:archiveId',passport.authenticate('bearer', { session: false }),function(req, res, next){
  exportMetadataToCSV(req, res);
});

/**
 * Get archive data.
 *
 * @param req
 * @param res
 */
function exportMetadataToCSV(req, res) {
  logger.info('Export metadata to CSV. ' + req.params.archiveId);

  csvexport.metadata(req.params.archiveId, function(err, csvexport){
    if (err) return res.status(500).json(getError(err, req));
    return res.status(200).csv(csvexport);
  });
}

function getError(err, req) {
  var error = {};
  error[req.i18n.__('error')] = err;
  return error;
}
