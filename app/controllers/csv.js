var express = require('express')
  , router = express.Router()
  , logger = require('../../lib/logger')
  , archive_csv = require('../../lib/services/archive/csv')
  , passport = require('passport')
  , config = require('../../config/config')
  , _ = require('lodash')
  , multipart = require('connect-multiparty')
  , multipartMiddleware = multipart()
  , fs = require('fs');

module.exports = function (app) {
  app.use('/csv', router);
};

router.post('/import', multipartMiddleware, function (req, res, next) {
  if(!req.isAuthenticated()) {
    next();
  } else {
    importCSV(req, res);
  }
});

router.post('/import',passport.authenticate('bearer', { session: false }),function(req, res, next){
  importCSV(req, res);
});

/**
 * Handle the import of SAMI data and return Archive details.
 *
 * @param req
 * @param res
 */
function importCSV(req, res) {
  logger.info('Post import CSV.' + req.body);

  if(req.files && req.files.hasOwnProperty('file')) {
    var filepath = req.files.file.path;

    archive_csv.DataImport(req, filepath, function(err, archive) {
      if (err) {
        var error = {};
        logger.error(err);
        if(!_.isString(err)) err = 'Unable to import CSV data.';
        error[req.i18n.__('error')] = err;
        return res.status(500).json(error);
      }
      if (archive && archive.error) return res.status(400).json(archive.error);

      res.status(201).json(archive);
    });
  }
}
