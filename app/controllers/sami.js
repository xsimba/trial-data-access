var express = require('express')
  , router = express.Router()
  , logger = require('../../lib/logger')
  , archive_sami = require('../../lib/services/archive/sami')
  , passport = require('passport')
  , config = require('../../config/config')
  , _ = require('lodash');

module.exports = function (app) {
  app.use('/sami', router);
};

router.post('/import', function (req, res, next) {
  if(!req.isAuthenticated()) {
    next();
  } else {
    importSami(req, res);
  }
});

router.post('/import',passport.authenticate('bearer', { session: false }),function(req, res, next){
  importSami(req, res);
});

/**
 * Handle the import of SAMI data and return Archive details.
 *
 * @param req
 * @param res
 */
function importSami(req, res) {
  logger.info('Post sami data.' + req.body.trialId);

  archive_sami.DataImport(req, function(err, archive) {
    if (err) {
      var error = {};
      logger.error(err);
      if(!_.isString(err)) err = 'Unable to import SAMI data.';
      error[req.i18n.__('error')] = err;
      return res.status(500).json(error);
    }
    if (archive && archive.error) return res.status(400).json(archive.error);

    res.status(201).json(archive);
  });
}
