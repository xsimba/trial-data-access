var express = require('express')
  , router = express.Router()
  , logger = require('../../lib/logger')
  , passport = require('passport')
  , config = require('../../config/config')
  , archive = require('../../lib/elastic/archive')
  , dynamodb = require('../../lib/services/aws/dynamodb')
  , sami = require('../../lib/services/api/sami')
  , archiveUtility = require('../../lib/services/archive/utility');

module.exports = function (app) {
  app.use('/search', router);
};

router.get('/', function (req, res, next) {
  if(!req.isAuthenticated()) {
    next();
  } else {
    searchElasticArchive(req, res);
  }
});

router.get('/',passport.authenticate('bearer', { session: false }),function(req, res, next){
  searchElasticArchive(req, res);
});

/**
 * Handle the archive search with filters request and response.
 *
 * @param req
 * @param res
 */
function searchElasticArchive(req, res) {
  logger.info('Get elasticsearch index archive search results.');

  archive.search(req, function(err, results){
    if (err) return res.status(500).json(formatError(err, req));

    if (results && results.error) return res.status(400).json(results.error);

    res.status(201).json(results);
  });
}

router.get('/find', function (req, res, next) {
  if(!req.isAuthenticated()) {
    next();
  } else {
    fullSearchElasticArchive(req, res);
  }
});

router.get('/find',passport.authenticate('bearer', { session: false }),function(req, res, next){
  fullSearchElasticArchive(req, res);
});

/**
 * Handle the search with free text request and response.
 *
 * @param req
 * @param res
 */
function fullSearchElasticArchive(req, res) {
  logger.info('Get elasticsearch index archive search results full text search.');

  archive.text_search(req, function(err, results){
    if (err) return res.status(500).json(formatError(err, req));

    if (results && results.error) return res.status(400).json(results.error);

    res.status(201).json(results);
  });
}

router.get('/user/archives', function (req, res, next) {
  if(!req.isAuthenticated()) {
    next();
  } else {
    userArchives(req, res);
  }
});

router.get('/user/archives',passport.authenticate('bearer', { session: false }),function(req, res, next){
  userArchives(req, res);
});

/**
 * Search for all user's archives.
 *
 * @param req
 * @param res
 */
function userArchives(req, res) {
  logger.info('Get user\'s archives with elasticsearch index archive search.');

  if(req.session.hasOwnProperty('userId')) return getUserArchives(req, res);

  var token  = archiveUtility.getToken(req);

  sami.Users.Profile(token, function(err, user) {
    if (err) return res.status(500).json(formatError(err, req));

    if (!user.body || !user.body.data) return res.status(500).json('No user profile data.');

    req.session.userId = user.body.data.id;

    getUserArchives(req, res);
  });
}

/**
 * Search for all user's archives.
 *
 * @param req
 * @param res
 */
function getUserArchives(req, res) {
  archive.users_archives(req, function(err, results){
    if (err) return res.status(500).json(formatError(err, req));

    if (results && results.error) return res.status(400).json(results.error);

    res.status(201).json(results);
  });
}

router.get('/metadata/key', function (req, res, next) {
  if(!req.isAuthenticated()) {
    next();
  } else {
    searchElasticMetadataKey(req, res);
  }
});

router.get('/metadata/key',passport.authenticate('bearer', { session: false }),function(req, res, next){
  searchElasticMetadataKey(req, res);
});

/**
 * Handle the metadata search request and response.
 *
 * @param req
 * @param res
 */
function searchElasticMetadataKey(req, res) {
  logger.info('Get metadata keys.');

  dynamodb.ArchiveMetadataKeySearch(req.query.q, function(err, results){
    if (err) return res.status(500).json(formatError(err, req));

    if (results && results.error) return res.status(400).json(results.error);

    res.status(201).json(results);
  });
}

function formatError(err, req) {
  var error = {};
  error[req.i18n.__('error')] = err;
  return error;
}
