var express = require('express')
  , router = express.Router()
  , logger = require('../../lib/logger')
  , archive = require('../../lib/elastic/archive')
  , archive_aws = require('../../lib/services/archive/aws')
  , archive_session = require('../../lib/services/archive/session')
  , archive_user = require('../../lib/services/archive/user')
  , Metadata = require('../../lib/services/archive/metadata')
  , request = require('superagent')
  , passport = require('passport')
  , config = require('../../config/config')
  , path = require('path')
  , dynamodb = require('../../lib/services/aws/dynamodb')
  , _ = require('lodash')
  , transform = require('../../lib/services/transform/transform')
  , sami = require('../../lib/services/api/sami')
  , archiveUtility = require('../../lib/services/archive/utility');

module.exports = function (app) {
  app.use('/trial', router);
};

/**
 * First see if the user is authenticated by checking the request e.g. session.
 */
router.get('/list', function (req, res, next) {
  if(!req.isAuthenticated()) {
    next();
  } else {
    trialsList(req, res);
  }
});

/**
 * Finally check if a bearer token has been supplied.
 */
router.get('/list',passport.authenticate('bearer', { session: false }),function(req, res, next){
  trialsList(req, res);
});

/**
 * Get list of trials user has access to.
 *
 * @param req
 * @param res
 */
function trialsList(req, res) {
  logger.info('Get user\'s trials list.');
  var token  = archiveUtility.getToken(req);
  sami.Users.Profile(token, function(err, profile){
    if (err) return res.status(500).json(getError(err, req));
    if(!profile.hasOwnProperty('body') || !profile.body.hasOwnProperty('data') || !profile.body.data.hasOwnProperty('id')) {
      return res.status(500).json(getError('No user ID.', req));
    }
    var userId = profile.body.data.id;
    sami.Users.Trials(userId, token, function(err, trials){
      if (err) return res.status(500).json(getError(err, req));
      return res.status(200).json(trials.body.data.trials);
    });
  });
}

router.get('/:trialId/participants', function (req, res, next) {
  if(!req.isAuthenticated()) {
    next();
  } else {
    trialParticipantsList(req, res);
  }
});

router.get('/:trialId/participants',passport.authenticate('bearer', { session: false }),function(req, res, next){
  trialParticipantsList(req, res);
});

/**
 * Get list of trial participants.
 *
 * @param req
 * @param res
 */
function trialParticipantsList(req, res) {
  logger.info('Get trial\'s participants list: ' + req.params.trialId);
  var token  = archiveUtility.getToken(req);
  sami.Trials.Participants(req.params.trialId, token, function(err, participants){
    if (err) return res.status(500).json(getError(err, req));
    return res.status(200).json(participants.body.data.participants);
  });
}

router.get('/:trialId/participants', function (req, res, next) {
  if(!req.isAuthenticated()) {
    next();
  } else {
    trialParticipantsList(req, res);
  }
});

router.get('/:trialId/participants',passport.authenticate('bearer', { session: false }),function(req, res, next){
  trialParticipantsList(req, res);
});

/**
 * Get list of trial participants.
 *
 * @param req
 * @param res
 */
function trialParticipantsList(req, res) {
  logger.info('Get trial\'s participants list: ' + req.params.trialId);
  var token  = archiveUtility.getToken(req);
  sami.Trials.Participants(req.params.trialId, token, function(err, participants){
    if (err) return res.status(500).json(getError(err, req));
    return res.status(200).json(participants.body.data.participants);
  });
}

router.get('/:trialId/participants/:participantId/devices', function (req, res, next) {
  if(!req.isAuthenticated()) {
    next();
  } else {
    trialParticipantDeviceList(req, res);
  }
});

router.get('/:trialId/participants/:participantId/devices',passport.authenticate('bearer', { session: false }),function(req, res, next){
  trialParticipantDeviceList(req, res);
});

/**
 * Get list of trial participants.
 *
 * @param req
 * @param res
 */
function trialParticipantDeviceList(req, res) {
  logger.info('Get trial\'s participant\'s device list: ' + req.params.trialId);
  var token  = archiveUtility.getToken(req);
  sami.Trials.UserDevices(req.params.trialId, req.params.participantId, token, function(err, participants){
    if (err) return res.status(500).json(getError(err, req));
    return res.status(200).json(participants.body.data.devices);
  });
}

function getError(err, req) {
  var error = {};
  error[req.i18n.__('error')] = err;
  return error;
}
