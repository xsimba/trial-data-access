var express = require('express')
  , router = express.Router()
  , logger = require('../../lib/logger')
  , archive_session = require('../../lib/services/archive/session')
  , passport = require('passport')
  , config = require('../../config/config');

module.exports = function (app) {
  app.use('/session', router);
};

router.post('/import', function (req, res, next) {
  if(!req.isAuthenticated()) {
    next();
  } else {
    importSession(req, res);
  }

});

router.post('/import',passport.authenticate('bearer', { session: false }),function(req, res, next){
  importSession(req, res);
});

/**
 * Get import session page
 */
router.get('/import', function (req, res, next) {
  logger.info('Get import.');


  if(!req.isAuthenticated()) {
    return res.redirect('/');
  }

  res.render('/import',{
    url: '/import',
    name: 'import',
    title: req.i18n.__('title'),
    loggedIn: req.user ? true : false,
    logout: config.links.logout + encodeURIComponent(config.app.url) + config.links.logoutRedirect
  });

});

/**
 * Handle the import session request and response.
 *
 * @param req
 * @param res
 */
function importSession(req, res) {
  logger.info('Post session.' + req.body.sessionId);

  archive_session.DataImport(req, function(err, archive) {
    if (err) {
      logger.error(err);
      return res.status(500).json(getError(err, req));
    }
    if (archive && archive.error) return res.status(400).json(archive.error);

    res.status(201).json(archive);
  });
}

function getError(err, req) {
  var error = {};
  error[req.i18n.__('error')] = err;
  return error;
}
