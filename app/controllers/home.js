var express = require('express')
  , passport = require('passport')
  , router = express.Router()
  , logger = require('../../lib/logger')
  , config = require('../../config/config')
  , dynamodb = require('../../lib/services/aws/dynamodb');

module.exports = function (app) {
  app.use('/', router);
};

/**
 * Home page.
 */
router.get('/', function (req, res, next) {
  var response = {
    welcome: req.i18n.__('welcome'),
    title: req.i18n.__('title'),
    loggedIn: req.user ? true : false,
    logout: config.links.logout + encodeURIComponent(config.app.url) + config.links.logoutRedirect
  };

  if(req.query && req.query.status && req.query.status === 'logout') {
    req.session.destroy(function(err){
      if(err) logger.error(err);
      res.redirect('/');
    })
  } else if(req.user) {
      response.url = '/';
      response.name= 'index';
      response.access_token = req.user.name;
      res.render('/', response); // should use react-routing now...
  } else {
    res.render('index', response);
  }

});

/**
 * Login.
 */
router.get('/login', passport.authenticate('sami'));

/**
 * Authorize with SAMI.
 */
router.get('/authorize',
  passport.authenticate('sami', { successRedirect: '/',
    failureRedirect: '/' }));

/**
 * Get import sami page
 */
router.get('/import', function (req, res, next) {
  logger.info('Get import.');

  if(!req.isAuthenticated()) {
    return res.redirect('/');
  }

  res.render('/import',{
    url: '/import',
    name: 'import',
    title: req.i18n.__('title'),
    loggedIn: req.user ? true : false,
    logout: config.links.logout + encodeURIComponent(config.app.url) + config.links.logoutRedirect
  });

});

/**
 * Get search archive page
 */
router.get('/search-archive', function (req, res, next) {
  logger.info('Get search archive.');

  dynamodb.MetadataKeys(function(err, keys){
    if(err) return res.status(500).json(error);

    if(!req.isAuthenticated()) {
      return res.redirect('/');
    }

    res.render('/search-archive',{
      url: '/search-archive',
      name: 'search-archive',
      title: req.i18n.__('title'),
      loggedIn: req.user ? true : false,
      logout: config.links.logout + encodeURIComponent(config.app.url) + config.links.logoutRedirect,
      keys: keys,
      userId: req.user? req.user.name : null
    });
  });
});


/**
 * Get archive list
 */
router.get('/archives', function (req, res, next) {
  logger.info('Get archives.');

  if(!req.isAuthenticated()) {
    return res.redirect('/');
  }

  res.render('/archives',{
    url: '/archives',
    name: 'archives',
    title: req.i18n.__('title'),
    loggedIn: req.user ? true : false,
    logout: config.links.logout + encodeURIComponent(config.app.url) + config.links.logoutRedirect
  });

});

